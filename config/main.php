<?php

return [
    'base_url'           => 'http://flexcat.cc',
    'upload_folder'      => 'upload',
    'default_img_upload' => 'img',
    'pages_img_folder'   => 'page',
    'upload_sites_img'   => 'sites',
    'dynamic_pagefolder' => 'Dynamic'
];