<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Обновление системы FlexCat CMS</title>
    <link rel="stylesheet" href="https://flexcat.ru/cmsUpdate/jquery-ui.css">
    <link rel="stylesheet" href="https://flexcat.ru/cmsUpdate/pageUpdateStyle.min.css">
    <link rel="shortcut icon" href="https://flexcat.ru/cmsUpdate/flexcat_favicon.ico">
    <link rel="stylesheet" href="https://flexcat.ru/cmsUpdate/icofont.min.css">
    <script src="https://flexcat.ru/cmsUpdate/jquery-3.3.1.min.js"></script>
    <script src="https://flexcat.ru/cmsUpdate/jquery-ui.min.js"></script>
</head>
<body>
<nav class="container-main-menu topMains">
    <div id="top-menu">
        <a href="/flexcat/admin">
            <img src="https://flexcat.ru/cmsUpdate/flexcat_white.svg" alt="FlexCat" height="24">FlexCat
        </a>
    </div>
</nav>

<div class="page-title-block topTitle">
    <a href="#">
        <i class="icofont-cloud-refresh"></i>
        Установка обновления FlexCat CMS version <?php
        $content = file_get_contents('http://flexcat.ru/cmsUpdate/flexcat.xml');
        $ver = new SimpleXMLElement($content);
        echo $ver->version;
        ?>
    </a>
</div>
<div class="center-info">
<?php $version = file_get_contents('version.txt'); ?>



