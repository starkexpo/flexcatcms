$(function () {


//Main menu list
$('.titleFlexList').on('click', function (e) {
    e.preventDefault();
    var $this = $(this);
    if($this.is(':animated') == false) {
        var $thisBlock = $this.siblings('.extremum-slide');
        var view = $thisBlock.attr('data-view');
        $('[data-view="show"]').hide();
        $('[data-view="show"]').attr('data-view', 'hide');
        $('.titleFlexList').removeClass('nowMain');
        // console.log(view);

        if(view == "hide"){
            $thisBlock.show('puff');
            $thisBlock.attr('data-view', 'show');
            $this.addClass('nowMain');
        }
        else if(view == "show"){
            $thisBlock.show();
            $thisBlock.hide('drop', {direction: "up"}, 500);
            $thisBlock.attr('data-view', 'hide');
            $this.removeClass('nowMain');
            // $thisBlock.css('display', 'none');
        }

        // console.log($thisBlock);
    }
});




    $('.off-canvas-launcher').on('click', function (e) {
        e.preventDefault();
        var $topMain = $('.topMain');
        var view = $topMain.attr('data-view');
        switch (view){
            case "0":
                $topMain.attr('data-view', '1');
                $topMain.addClass('topMain-flexbox').animate({
                    marginLeft: 0
                }, 340);
                $(this).addClass('button-close');
                break;

            case "1":
                $topMain.attr('data-view', '0');
                $topMain.animate({
                    marginLeft: "-110%"
                }, 340, function () {
                    $topMain.removeClass('topMain-flexbox');
                    $topMain.css('margin-left', '100%');
                });
                $(this).removeClass('button-close');
                break;
        }
    });



    $(document).on('mouseup', function (e) {
        var $notification =  $('.titleFlexList');
        var $condition = $('.extremum-slide');
        if (!$notification.is(e.target) && $notification.has(e.target).length === 0 && !$condition.is(e.target) && $condition.has(e.target).length === 0) {
            var $nowBlock = $('[data-view="show"]');
            $nowBlock.attr('data-view', 'hide');
            $nowBlock.hide('drop', {direction: "up"}, 500);
            $('.titleFlexList').removeClass('nowMain');
            // $('.extremum-slide').css('display', 'none');

            // console.info('click');
        }

    });










});