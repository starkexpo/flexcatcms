<?php

namespace FlexCat\Service\Router;


use FlexCat\Service\AbstractProvider;
use FlexCat\Http\Router\Router;

class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public $serviceName = 'router';

    /**
     * @return mixed
     */
    public function init()
    {

        $router = new Router('http://'.$_SERVER['HTTP_HOST']);

        $this->di->set($this->serviceName, $router);
    }
}

