<?php

namespace FlexCat\Service;


abstract class AbstractProvider
{

    /**
     * @var \FlexCat\DI\DI;
     */
    protected $di;

    /**
     * AbstractProvider constructor.
     * @param \FlexCat\DI\DI $di
     */
    public function __construct(\FlexCat\DI\DI $di)
    {
        $this->di = $di;
    }

    /**
     * @return mixed
     */
    abstract function init();
}