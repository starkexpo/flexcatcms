<?php


namespace FlexCat\Service\Config;

use FlexCat\Service\AbstractProvider;
use FlexCat\Config\Config;


class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public $serviceName = 'config';

    /**
     * @return mixed
     */
    public function init()
    {
        $config['main'] = Config::file('main');
        $config['database'] = Config::file('ConfigDatabase');

        $this->di->set($this->serviceName, $config);
    }
}