<?php

namespace FlexCat\Service\Load;


use FlexCat\Service\AbstractProvider;
use FlexCat\Template\Load;

class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public $serviceName = 'load';

    /**
     * @return mixed
     */
    public function init()
    {

        $load = new Load($this->di);

        $this->di->set($this->serviceName, $load);
    }
}

