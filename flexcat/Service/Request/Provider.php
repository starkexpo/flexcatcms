<?php

namespace FlexCat\Service\Request;


use FlexCat\Service\AbstractProvider;
use FlexCat\Http\Request\Request;

class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public $serviceName = 'request';

    /**
     * @return mixed
     */
    public function init()
    {

        $request = new Request();

        $this->di->set($this->serviceName, $request);
    }
}

