<?php

namespace FlexCat\Service\View;


use FlexCat\Service\AbstractProvider;
use FlexCat\Template\View;

class Provider extends AbstractProvider
{
    /**
     * @var string
     */
    public $serviceName = 'view';

    /**
     * @return mixed
     */
    public function init()
    {
        $view = new View();

        $this->di->set($this->serviceName, $view);
    }
}


