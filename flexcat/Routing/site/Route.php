<?php

/**
 * List Routes
 */

$queryBuilder = new \FlexCat\Database\QueryBuilder();
$db = new FlexCat\Database\Connection();

$sql = $queryBuilder
    ->select()
    ->from('routing')
    ->where("env", "site")
    ->where("published", "enable")
    ->sql();
$routes = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

foreach ($routes as $route) {
    $routeLoad = '';

    $routeLoad = '$this->router->add("'
        . $route['module'] . '", "'
        . $route['url'] . '", "'
        . $route['controller'] . ':'
        . $route['method'] . '", "'
        . $route['request'] . '"); ';

    eval($routeLoad);
}

/*
$this->router->add('home', '/', 'HomeController:index');
$this->router->add('news', '/news/', 'HomeController:news');
$this->router->add('news_single', '/news/(id:any)', 'HomeController:news');*/
