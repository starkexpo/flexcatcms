<?php

/**
 * List Routes
 */

$queryBuilder = new \FlexCat\Database\QueryBuilder();
$db = new FlexCat\Database\Connection();

$sql = $queryBuilder
    ->select()
    ->from('routing')
    ->where("env", "admin")
    ->where("published", "enable")
    ->sql();
$routes = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

foreach ($routes as $route) {
    $routeLoad = '';

    $routeLoad = '$this->router->add("'
        . $route['module'] . '", "'
        . $route['url'] . '", "'
        . $route['controller'] . ':'
        . $route['method'] . '", "'
        . $route['request'] . '"); ';

    eval($routeLoad);
}


/*
//admin
$this->router->add('login', '/admin/login/', 'LoginController:form');
$this->router->add('auth-admin', '/admin/auth/', 'LoginController:authAdmin', 'POST');
$this->router->add('logout', '/admin/logout/', 'AdminController:logout');
$this->router->add('dashboard', '/admin/', 'DashboardController:index');

//pages
////GET
$this->router->add('page', '/admin/pages/', 'PageController:listing');
$this->router->add('page-create', '/admin/pages/create/', 'PageController:create');
$this->router->add('page-edit', '/admin/pages/edit/(id:int)', 'PageController:edit');

////POST
$this->router->add('page-add', '/admin/page/add/', 'PageController:add', 'POST');
$this->router->add('page-update', '/admin/page/update/', 'PageController:update', 'POST');
$this->router->add('page-alias', '/admin/page/alias/', 'PageController:aliasAjax', 'POST');

//sites
$this->router->add('sites', '/admin/sites/', 'SitesController:listing');
$this->router->add('sites-defaults', '/admin/sites/defaults/(id:int)', 'SitesController:defaults');
$this->router->add('sites-create', '/admin/sites/create/', 'SitesController:create');
$this->router->add('sites-edit', '/admin/sites/edit/(id:int)', 'SitesController:edit');

$this->router->add('sites-add', '/admin/sites/add/', 'SitesController:add', 'POST');
$this->router->add('sites-update', '/admin/sites/update/', 'SitesController:update', 'POST');
$this->router->add('sites-favicon', '/admin/sites/favicon/', 'SitesController:favicon', 'POST');
$this->router->add('sites-favdelete', '/admin/sites/favdelete/(id:int)', 'SitesController:faviconDelete');

//Structure
//items
$this->router->add('structure', '/admin/structure/', 'StructureController:listing');
$this->router->add('structure-item-create', '/admin/structure/create/', 'StructureController:createItem');

//menu
$this->router->add('structure-menu', '/admin/structure/menu/', 'StructureController:listingMenu');
$this->router->add('structure-menu-create', '/admin/structure/menu/create/', 'StructureController:createMenu');
$this->router->add('structure-menu-edit', '/admin/structure/menu/edit/(id:int)', 'StructureController:editMenu');

$this->router->add('structure-menu-add', '/admin/structure/menu/add/', 'StructureController:addMenu', 'POST');
$this->router->add('structure-menu-update', '/admin/structure/menu/update/', 'StructureController:updateMenu', 'POST');*/