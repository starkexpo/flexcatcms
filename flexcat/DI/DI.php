<?php

namespace FlexCat\DI;


class DI
{
    /**
     * @var array
     */
    private $container = [];

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value)
    {
        $this->container[$key] = $value;

        return $this;
    }

    public function get($key)
    {
//        return $this->container[$key];
        return $this->has($key);
    }

    public function has($key)
    {
        return isset($this->container[$key]) ? $this->container[$key] : null;
    }

    public function push($key, $element = [])
    {
        if ($this->has($key) !== null) {
            $this->set($key, []);
        }

        if (!empty($element)) {
            $this->container[$key][$element['key']] =  $element['value'];
        }
    }
}