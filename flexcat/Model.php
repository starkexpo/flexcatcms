<?php

namespace FlexCat;

use FlexCat\Database\QueryBuilder;
use FlexCat\DI\DI;

abstract class Model
{
    /**
     * Controller constructor.
     * @param $di
     * @var \FlexCat\DI\DI
     */
    protected $di;
    protected $db;
    protected $config;

    public $queryBuilder;

    /**
     * Controller constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;
        $this->db = $this->di->get('db');
        $this->config = $this->di->get('config');

        $this->queryBuilder = new QueryBuilder();
    }

}