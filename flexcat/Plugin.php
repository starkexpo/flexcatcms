<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 26/12/2018
 * Time: 17:18
 */

namespace FlexCat;

use FlexCat\DI\DI;

abstract class Plugin
{
//    use ActiveRecord;


    /*protected $table = 'plugin';

    public $id;
    public $directory;
    public $is_active;*/

    protected $di;
    protected $db;
    protected $router;


    public function __construct(DI $di)
    {
        $this->di = $di;
    }

    abstract public function details();

    public function getDI()
    {
        return $this->di;
    }
}