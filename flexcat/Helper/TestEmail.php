<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 02/01/2019
 * Time: 23:22
 */

namespace FlexCat\Helper;


class TestEmail
{
    public static function verificationEmail($email)
    {
        $email=strtolower($email);
        $email=str_replace("|","I",$email);
        if(!preg_match("/^[a-z0-9\.\-_]+@[a-z0-9\-_]+\.([a-z0-9\-_]+\.)*?[a-z]+$/is", $email) or $email=="" or strlen($email)>35) {

            return false;
        }

        return true;
    }
}