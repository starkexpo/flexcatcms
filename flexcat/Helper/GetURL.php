<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 24/02/2019
 * Time: 22:39
 */

namespace FlexCat\Helper;


class GetURL
{
    public function request_url()
    {
        $result = ''; // Пока результат пуст
        $default_port = 80; // Порт по-умолчанию

        // А не в защищенном-ли мы соединении?
        if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {

            $result .= 'https://';

            $default_port = 443;
        } else {

            $result .= 'http://';
        }

        $result .= $_SERVER['SERVER_NAME'];


        if ($_SERVER['SERVER_PORT'] != $default_port) {

            $result .= ':'.$_SERVER['SERVER_PORT'];
        }

        $result .= $_SERVER['REQUEST_URI'];

        return $result;
    }
}