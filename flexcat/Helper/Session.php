<?php

namespace flexcat\Helper;


class Session
{
    public static function set($key, $value)
    {
        @session_start();
        $_SESSION[$key] = $value;
    }

    public static function get($key)
    {
        @session_start();
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return null;
    }

    public static function delete($key)
    {
        @session_start();
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }
}