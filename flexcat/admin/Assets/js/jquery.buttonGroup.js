(function($) {
  $.fn.buttonGroup = function() {
    var $this = $(this);

    $this.on('click',  function(e) {
      e.preventDefault();
      var $dromMenu = $(this);

      $dromMenu.toggleClass('btn-group-active');
      $dromMenu.next('.btn-group-drop-menu').toggleClass('hide');
    });

  };
})(jQuery);