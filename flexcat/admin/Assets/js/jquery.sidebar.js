(function($) {
  $.fn.sideBar = function() {
    var $this = $(this),
        $menu = $('.sidebar'),
        $miniMenu = $('.sidebar-minimize');

    $this.on('click', '.sidebar-item > a', function(e) {
      var $item = $(this),
          $itemLine = $item.parents('.sidebar-item');

      if ($itemLine.find('.submenu').length > 0 &&
          $itemLine.find('.submenu').is(':animated') === false) {
        e.preventDefault();

        if ($item.hasClass('sidebar-active-item') === false) {
          $item.addClass('sidebar-active-item');
          $item.find('.arrow-sub').addClass('arrow-rotate');
          $itemLine.find('.submenu').removeClass('hide').hide().slideDown();
        } else {
          $item.removeClass('sidebar-active-item');
          $item.find('.arrow-sub').removeClass('arrow-rotate');
          $itemLine.find('.submenu').slideUp();
        }
      }
    });

    $this.on('mousemove', '.submenu', function() {
      var $parent = $(this).parents('.sidebar-item');

      if ($parent.parent().hasClass('sidebar-minimize')) {
        $parent.addClass('sidebar-active-item');
      }
    });

    $this.on('mouseout', '.submenu', function() {
      var $parent = $(this).parents('.sidebar-item');

      if ($parent.parent().hasClass('sidebar-minimize')) {
        $parent.removeClass('sidebar-active-item');
      }
    });

  };
})(jQuery);