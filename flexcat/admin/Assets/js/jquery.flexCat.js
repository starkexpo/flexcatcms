(function($) {
  $('.user-block').myProfile();

  $('.main-menu-container').mainMenu();

  $('.dateTimes').timeMenu();

  $('#sidebarMenu').sideBar();

  $('.container-control-window').controlWindow();

  $('.btn-group').buttonGroup();


  if ($('.flexcat-tabs').length > 0) {
    $('.flexcat-tabs').tabsFlexCat();
  }

  if ($('.check-switch-ios').length > 0) {
    $('.check-switch-ios').switchios();
  }



  $('.container-central').on('click', '.btn-show-option', function(e) {
    e.preventDefault();
    var $this = $(this);

    $this.next('.btn-mini-group').toggleClass('btn-mini-group-active');

  });


  // $('.select').selectmenu();
  $('.select').selectmenu().selectmenu('menuWidget').css('height', '250px');

  //Tooltip for window panel
  $('.tooltip').tooltip({show: 'fade', hide: 'drop', track: false});



  $('.hello-delete').dialog({
    resizable: false,
    modal: true,
    autoOpen: false,
    urlDelete: '',
    item: '',
    title: 'Удаление записи',
    show: {
      effect: 'drop',
      duration: 300,
      direction: 'up',
      easing: 'easeInQuad',
    },
    hide: {effect: 'explode', delay: 100, duration: 600, easing: 'easeInQuad'},
    buttons: {
      'Отменить': function() {
        $(this).dialog('close');
      },
      'Удалить': function() {
        var url = $(this).dialog('option', 'urlDelete');
        var $item = $(this).dialog('option', 'item');

        // console.log(url);
        // console.warn($item);

        var str = url.replace(/\d+/, '');
        var itemId = parseInt(url.replace(str, ''));
        // console.info("itemId: " + itemId);

        setTimeout(function() {
          $item.css({
            position: 'absolute',
            width: $item.width() + 'px',
            display: 'block',
          });
          $item.hide('drop', {direction: 'left'}, 800);
        }, 800);

        $.ajax({
          type: 'GET',
          url: url,
          data: itemId,
          timeout: 5000,
          cache: false,
          processData: false,
          beforeSend: function() {
            /*before load*/
          },
          complete: function() {
            /*load off data */
          },
          success: function(data) {
            // data = $.parseJSON(data);
            // console.log(data);
          },
          fail: function() {
            alert('Please try again soon.');
          },
        });

        $(this).dialog('close');
        $(this).addClass('hide');
      },
    },
    open: function() {
      $('.ui-dialog-buttonpane').
          find('button:contains("Удалить")').
          addClass('btn-red-dialog');

      $(this).removeClass('hide');
    },
  });


  /*delete item*/
  $('.container-content').on('click', '.delete-but', function(e) {
    e.preventDefault();

    var $this = $(this),
        url = $this.attr('href'),
        $item = $this.closest('tr');

    $('.hello-delete').dialog('option', 'urlDelete', url);
    $('.hello-delete').dialog('option', 'item', $item);
    $('.hello-delete').dialog('open');
  });


})(jQuery);