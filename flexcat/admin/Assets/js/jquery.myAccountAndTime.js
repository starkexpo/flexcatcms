(function($) {
  $.fn.myProfile = function() {
    var $this = $(this),
        $blockUser = $this.find('.user-describe');

    $this.on('click', function() {
      var view = $blockUser.attr('data-view');

      if ($blockUser.is(':animated') == true) {
        return false;
      }

      switch (view) {
        case '0':
          $blockUser.attr('data-view', '1');
          $this.addClass('user-active');
          $blockUser.removeClass('hide').
              hide().
              show('slide', {direction: 'up'}, 200);
          break;

        case '1':
          $blockUser.attr('data-view', '0');
          $this.removeClass('user-active');
          $blockUser.hide('drop', {direction: 'left'}, 300, function() {
            $blockUser.addClass('hide');
          });
          break;
      }
    });
  };

  $.fn.timeMenu = function() {
    var $this = $(this),
        $days = $this.find('.dateTimes-day'),
        $minutes = $this.find('.minutes-main-time');

    function setDayTopMenu(day) {
      var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
      var dayWeek = days[day];
      return dayWeek;
    }

    function displayTime() {
      var now = new Date();
      var hours = now.getHours();
      var minutes = now.getMinutes();
      // var seconds = now.getSeconds();
      var day = now.getDay();

      var dayOfWeek = setDayTopMenu(day);
      if (minutes < 10) {
        minutes = '0' + minutes;
      }

      // console.log(dayOfWeek);
      // $('#dateMain').text(dayOfWeek + ' ' + hours + ':' + minutes);   // + ":"+ seconds
      $days.text(dayOfWeek + ' ' + hours);
      $minutes.text(minutes);
    }

    displayTime();
    setInterval(displayTime, 1000);
  };

})(jQuery);