(function($) {

  // scroll
  var scrollWindow = function() {
    $(window).scroll(function() {
      var $w = $(this),
          st = $w.scrollTop(),
          navbar = $('.main-menu-container'),
          $buttonMenu = $('.main-menu-hover'),
          $dateTime = $('.dateTimes'),
          $logo = $('.main-menu-logo'),
          $imgLogo = $('.main-menu-logo-img'),
          $control = $('.main-menu-control-button'),
          $userBlock = $('.user-describe');
      // var sd = $('.js-scroll-wrap');

      if (st > 150) {
        if (!navbar.hasClass('scrolled')) {
          navbar.addClass('scrolled');
          $logo.addClass('dark-menu-main');
          $buttonMenu.addClass('dark-menu-main');
          $dateTime.addClass('dark-menu-main');
          $imgLogo.addClass('dark-menu-main');
          $control.addClass('dark-menu-main');
          $userBlock.addClass('scrolled');
        }
      }
      if (st < 150) {
        if (navbar.hasClass('scrolled')) {
          navbar.removeClass('scrolled sleep');
          $logo.removeClass('dark-menu-main');
          $buttonMenu.removeClass('dark-menu-main');
          $dateTime.removeClass('dark-menu-main');
          $imgLogo.removeClass('dark-menu-main');
          $control.removeClass('dark-menu-main');
          $userBlock.removeClass('scrolled sleep');
        }
      }

      if (st > 350) {
        if (!navbar.hasClass('awake')) {
          navbar.addClass('awake');
          $userBlock.addClass('awake');
        }

        /*if(sd.length > 0) {
          sd.addClass('sleep');
        }*/
      }
      if (st < 350) {
        if (navbar.hasClass('awake')) {
          navbar.removeClass('awake');
          navbar.addClass('sleep');
          $userBlock.removeClass('awake');
          $userBlock.addClass('sleep');
        }
        /*if(sd.length > 0) {
          sd.removeClass('sleep');
        }*/
      }
    });
  };
  scrollWindow();





})(jQuery);