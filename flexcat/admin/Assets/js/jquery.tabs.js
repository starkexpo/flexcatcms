(function($) {
  $.fn.tabsFlexCat = function() {

    $(this).each(function() {
      var $this = $(this);
      var $tab = $this.find('li.active-tabs');
      var $link = $tab.find('a');
      var $panel = $($link.attr('href'));

      $this.on('click', '.tab-control', function(e) {
        e.preventDefault();

        var $link = $(this),
            id = $(this).attr('href');


        if (id && !$link.is('.active-tabs')) {
          $panel.removeClass('active-tab');
          $tab.removeClass('active-tabs');

          $panel = $(id).addClass('active-tab');
          $tab = $link.parent().addClass('active-tabs');
        }

      });

    });
  };

})(jQuery);





