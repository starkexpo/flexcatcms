(function($) {

  function sendForm($button) {
    var nameButton = $button.attr('name');
    var $form = $('#sendForm');
    var formData = $form.serialize();
    var method = $form.attr('method').toUpperCase();
    var url = $form.attr('action');
    var $container = $('.container-content');
    var containerWidth = $container.width() - 16;


    $.ajax({
      type: method,
      url: url,
      data: formData,
      timeout: 5000,
      cache: false,
      processData: false,
      beforeSend: function() {
        $button.animate({fontSize: 0}, 300).append(
            '<img src="/flexcat/admin/Assets/img/loading/oval.svg" ' +
            ' class="loadSend">');
      },
      complete: function() {
        $button.animate({fontSize: '14px'}, 300).find('img').remove();
        $button.prepend('<i class="icofont-check"></i>').
            find('i').
            hide().
            show('drop');
        setTimeout(function() {
          $button.find('i').hide('explode', function() {
            $button.find('i').remove();
          });
        }, 1500);

        $container.before('<div class="alert-block" style="width: ' +
            containerWidth
            + 'px; ">Успешно сохранено! <i class="icofont-close"></i></div>');
      }
      ,
      success: function(data) {
        // data = $.parseJSON(data);
        // console.log(data);

        var redirect = $('#cancelButton').attr('href');

        if (nameButton == 'save') {
          window.location = redirect;
        }

      },
      fail: function() {
        alert('Please try again soon.');
      },
    });
  }

  $(':submit').on('click', function(e) {
    e.preventDefault();
    sendForm($(this));
  });

  $('.container-central').on('click', '.alert-block > i', function() {
    var $this = $(this);

    $this.parent().fadeOut(function() {
      $this.parent().remove();
    });

  });

})(jQuery);
