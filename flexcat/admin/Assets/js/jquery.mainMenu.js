(function($) {
  $.fn.mainMenu = function() {
    var $this = $(this),
        $controlPanel = $this.find('.main-menu-block-panel');

    $this.on('click', '.main-menu-panel-show-button', function() {
      $controlPanel.toggleClass('panel-sm-size');
      // $(this).toggleClass('main-menu-active-button');
    });

    $this.on('click', '.main-menu-show-button', function(e) {
      e.preventDefault();
      var $button = $(this),
          menuSize = $button.attr('data-size'),
          $sideBar = $('#sidebarMenu'),
          $leftBlock = $('.container-left');

      switch (menuSize) {
        case 'max':
          $button.addClass('main-menu-show-mini');
          $button.attr('data-size', 'min');
          $sideBar.removeClass('sidebar');
          $sideBar.addClass('sidebar-minimize');
          $leftBlock.addClass('miniBlockLeft');
          $sideBar.find('.submenu').each(function() {
            $(this).removeAttr('style').addClass('hide');
          });
          $sideBar.find('.sidebar-item > a').each(function() {
            $(this).removeClass('sidebar-active-item');
            $(this).find('.arrow-sub').removeClass('arrow-rotate');
          });

          localStorage.setItem('menuSize', 'min');
          break;

        case 'min':
          $button.removeClass('main-menu-show-mini');
          $button.attr('data-size', 'max');
          $sideBar.removeClass('sidebar-minimize');
          $sideBar.addClass('sidebar');
          $leftBlock.removeClass('miniBlockLeft');

          localStorage.setItem('menuSize', 'max');
          break;
      }

    });

    $this.on('click', '.main-menu-show-menu-tablet-button', function(e) {
      e.preventDefault();
      var $sidebar = $('#sidebarMenu'),
          $block = $('.container-left');

      $block.toggleClass('container-left-active');


      if ($sidebar.hasClass('sidebar-minimize')) {
        $sidebar.removeClass('sidebar-minimize');
        $sidebar.addClass('sidebar');
        $('.main-menu-show-button').removeClass('main-menu-show-mini');
      }
      if ($block.hasClass('miniBlockLeft')) {
        $block.removeClass('miniBlockLeft');
      }

    });

  };
})(jQuery);