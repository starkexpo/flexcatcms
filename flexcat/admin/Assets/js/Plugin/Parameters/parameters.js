$(function() {
  
  $('#typeVariable').selectmenu({
    change: function(e) {
      var types = $(this).val();

      if (types == "sql") {
        $('#field_sql').toggleClass('hide');
      }
      else {
        $('#field_sql').addClass('hide');
      }
    }
  });

});