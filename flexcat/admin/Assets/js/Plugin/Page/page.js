jQuery.noConflict();
jQuery(function($) {
  $('#content').redactor({
    imageUpload: '/flexcat/admin/Assets/js/Plugin/ckeditor/image_upload.php',

  });



  $('input[name="title"]').on('input', function() {
    var title = $(this).val();
    var $form = $('#sendForm');
    var formData = $form.serialize();

    $.ajax({
      type: "POST",
      url: '/flexcat/admin/page/alias/',
      data: formData,
      timeout: 5000,
      cache: false,
      processData: false,

      success: function(data) {
        data = $.parseJSON(data);
        // console.log(data);

        $('input[name="alias"]').val(data);
      },
    });
  });



});
