$('#timeZ').selectmenu().selectmenu('menuWidget').css('height', '250px');
$('#codingSite').selectmenu().selectmenu('menuWidget').css('height', '250px');

$('.tooltip-sites').on('mousemove', function() {
  $(this).next('.img-sites-logo').slideDown();
});

$('.tooltip-sites').on('mouseout', function() {
  $(this).next('.img-sites-logo').slideUp();
});

$('#tab-2').buttonset();

$('#Page404').selectmenu().selectmenu('menuWidget').css({'height': '250px'});
$('#Page403').selectmenu().selectmenu('menuWidget').css('height', '250px');
$('#PageOff').selectmenu().selectmenu('menuWidget').css('height', '250px');

//FileList {0: File, length: 1, item: function}

$('input[type=file]').on('change', function() {

  var files = this.files;

  // if( typeof files == 'undefined' ) return;

  var data = new FormData();

  $.each(files, function(key, value) {
    data.append(key, value);
  });

  data.append('my_file_upload', 1);

  // AJAX запрос
  $.ajax({
    url: '/flexcat/admin/sites/favicon/',
    type: 'POST', // важно!
    data: data,
    cache: false,
    // dataType    : 'json',
    // отключаем обработку передаваемых данных, пусть передаются как есть
    processData: false,
    // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
    contentType: false,
    // функция успешного ответа сервера
    success: function(data) {
      var newname = $.parseJSON(data);
      $('input[type=file]').slideUp();
      // $('.favicon-load').attr('src', '/upload/img/sites/' + newname);

      $('.ajax-reply').
          prepend('<img src="/upload/img/sites/' + newname +
              '" class="favicon-load">');

      $('.ajax-reply').removeClass('hide').hide().slideDown();

      $('#favicons').val(newname);
    },
    // функция ошибки ответа сервера
    error: function(jqXHR, status, errorThrown) {
      console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
    },

  });

});