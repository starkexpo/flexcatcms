(function($) {

  $('.container-central').on('click', '.published-button', function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    // console.info(url);

    // var pageId = parseInt(url.replace('/flexcat/admin/structure/setpublic/', ''));

    var str = url.replace(/\d+/, '');

    var pageId = parseInt(url.replace(str, ''));
    // console.log("pageID: " + pageId);

    $(this).toggleClass('public-off');

    $.ajax({
      type: 'GET',
      url: url,
      data: pageId,
      timeout: 5000,
      cache: false,
      processData: false,
      beforeSend: function() {
        /*before load*/
      },
      complete: function() {
        /*load off data */
      },
      success: function(data) {
        // data = $.parseJSON(data);
        // console.log(data);
      },
      fail: function() {
        alert('Please try again soon.');
      },
    });
  });

})(jQuery);