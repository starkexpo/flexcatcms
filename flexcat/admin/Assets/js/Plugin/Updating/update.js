$(function() {

  $('#testUpdate').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var url = $this.attr('href');
    var dataLink = $this.attr('data-update-link');
    var nowVersion = $('.title-version').attr('data-version');
    var description = '';

    $.ajax({
      type: "GET",
      url: url,
      timeout: 9000,
      cache: false,
      processData: false,
      beforeSend: function() {
        $this.hide();
        $('.loadUpdate').toggle();
      },
      complete: function() {
       /* $button.animate({fontSize: '14px'}, 300).find('img').remove();
        $button.prepend('<i class="icofont-check"></i>').
            find('i').
            hide().
            show('puff');
        setTimeout(function() {
          $button.find('i').hide('explode', function() {
            $button.find('i').remove();
          });
        }, 1500);

        $container.before('<div class="alert-block" style="width: ' +
            containerWidth
            + 'px; ">Успешно сохранено! <i class="icofont-close"></i></div>');*/
      }
      ,
      success: function(data) {
        data = $.parseJSON(data);
        // console.log(data);

        // console.info(data['version'][0]);
        // console.info(data['date'][0]);

        $.each(data['descriptions'], function(index, value) {
          // console.log(value[0]);
          description += "<li>" + value[0]  + "</li>";
        });



        /*var redirect = $('#cancelButton').attr('href');

        if (nameButton == 'save') {
          window.location = redirect;
        }*/

        $('.loadUpdate').toggle();

        if (nowVersion !== data['version'][0]) {
          $('.title-update-acces').toggle();
          $('.blockUpdate').toggleClass('hide');
          $('#versionUpdatesInfo').text(data['version'][0]);
          $('.list-update').prepend(description);

        } else {
          $('.notfin-update').toggleClass('hide');
        }
        // $this.attr('href', dataLink).text('Начать обновление').show();

      },
      fail: function() {
        alert('Please try again soon.');
      },
    });
  });

  $('.start_update').on('click', function(e) {
    // e.preventDefault();

    var $this = $(this);
    var link = $this.attr('href');
    // console.info(link);

    $this.find('span').hide();
    $('.loadCMS').removeClass('hide');
    // $this.attr('href', '#');
  });

});