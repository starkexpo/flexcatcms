$(function() {

  $('#templatePage').selectmenu({
    change: function() {
      // console.log($(this).val());
      var dynamicPageID = $(this).val();
      var $formLoad = $('#optionLoad');

      $.ajax({
        type: 'GET',
        url: '/flexcat/admin/layouts/maket/widget/options/' + dynamicPageID,
        data: dynamicPageID,
        timeout: 5000,
        cache: false,
        processData: false,
        beforeSend: function() {
          /*$button.animate({fontSize: 0}, 300).append(
              '<img src="/flexcat/admin/Assets/img/loading/oval.svg" ' +
              ' class="loadSend">');*/
        },
        complete: function() {
          /* $button.animate({fontSize: '14px'}, 300).find('img').remove();
           $button.prepend('<i class="icofont-check"></i>').
               find('i').
               hide().
               show('puff');
           setTimeout(function() {
             $button.find('i').hide('explode', function() {
               $button.find('i').remove();
             });
           }, 1500);

           $container.before('<div class="alert-block" style="width: ' +
               containerWidth
               + 'px; ">Успешно сохранено! <i class="icofont-close"></i></div>');*/
        }
        ,
        success: function(data) {
          // data = $.parseJSON(data);
          console.log(data);
          $formLoad.html('');
          $formLoad.prepend(data);
          $formLoad.find('.sql_select').selectmenu();

        },
        fail: function() {
          alert('Please try again soon.');
        },
      });

    },
  });

});