$(function() {
  var editor = CodeMirror.fromTextArea(document.getElementById('layoutCode'), {
    'mode': 'application/x-httpd-php',
    'lineNumbers': 'true',
    'styleActiveLine': 'true',
    'lineWrapping': 'true',
    'autoCloseTags': 'true',
    'tabSize': '2',
    'indentWithTabs': 'true',
    'smartIndent': 'false',
  });
  editor.setSize(null, '450px');

  /*var editorCSS = CodeMirror.fromTextArea(
      document.getElementById('cssLassCode'), {
        'mode': 'css',
        'lineNumbers': 'true',
        'styleActiveLine': 'true',
        'lineWrapping': 'true',
        'autoCloseTags': 'true',
        'tabSize': '2',
        'indentWithTabs': 'true',
        'smartIndent': 'false',
      });
  editorCSS.setSize(null, '450px');

  var editorJavaScript = CodeMirror.fromTextArea(
      document.getElementById('javascriptCode'), {
        'mode': 'javascript',
        'lineNumbers': 'true',
        'styleActiveLine': 'true',
        'lineWrapping': 'true',
        'autoCloseTags': 'true',
        'tabSize': '2',
        'indentWithTabs': 'true',
        'smartIndent': 'false',
      });
  editorJavaScript.setSize(null, '450px');*/

});