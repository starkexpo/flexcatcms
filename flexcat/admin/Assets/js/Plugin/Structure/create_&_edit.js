jQuery(function($) {

  $('.structure-radio').on('click', function() {
    var $button = $(this).find('input[type="radio"]');
    $('.structure-radio span').each(function() {
      $(this).removeClass('activeButton');
    });

    var value = $button.val();

    if ($button.prop('checked')) {
      // console.info($button.val());
      $button.next('span').addClass('activeButton');
      $('.tab-type').addClass('hide');

      $('#radio'+ value).toggleClass('hide');
    }

  });


  $('#contentPage').redactor({
    imageUpload: '/flexcat/admin/Assets/js/Plugin/ckeditor/image_upload.php',

  });


  $('input[name="title"]').on('input', function() {
    var title = $(this).val();
    var $form = $('#sendForm');
    var formData = $form.serialize();

    $.ajax({
      type: "POST",
      url: '/flexcat/admin/page/alias/',
      data: formData,
      timeout: 5000,
      cache: false,
      processData: false,

      success: function(data) {
        data = $.parseJSON(data);
        // console.log(data);

        $('input[name="path"]').val(data);
      },
    });
  });


  $('#loadPages').selectmenu({
    change: function(e) {
      var pageId = $(this).val();
      var $textPage = $('textarea[name="contentPage"]');

      console.info(pageId);

      $.ajax({
        type: 'GET',
        url: '/flexcat/admin/structure/pageid/' + pageId,
        data: pageId,
        timeout: 5000,
        cache: false,
        processData: false,
        beforeSend: function() {
          /*before load*/
        },
        complete: function() {
          /*load off data */
        }
        ,
        success: function(data) {
          data = $.parseJSON(data);
          // console.log(data);
          $textPage.val(data);
          var $parent = $textPage.parent();
          var text = $parent.find('.redactor_editor').html(data);


        },
        fail: function() {
          alert('Please try again soon.');
        },
      });

    }
  });



});