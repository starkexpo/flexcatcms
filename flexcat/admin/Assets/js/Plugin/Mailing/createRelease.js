$(function() {

  /*var editor = CodeMirror.fromTextArea(document.getElementById('layoutCode'), {
    'mode': 'application/x-httpd-php',
    'lineNumbers': 'true',
    'styleActiveLine': 'true',
    'lineWrapping': 'true',
    'autoCloseTags': 'true',
    'tabSize': '2',
    'indentWithTabs': 'true',
    'smartIndent': 'false',
  });
  editor.setSize(null, '450px');*/

  // $('.CodeMirror-scroll').css('border', '1px solid #CCC');

  $('#newRelease').dialog({
    resizable: false,
    height: 600,
    width: '90%',
    minWidth: 800,
    modal: true,
    autoOpen: false,
    title: 'Отправка рассылки',
    show: {
      effect: 'drop',
      duration: 300,
      direction: 'up',
      easing: 'easeInQuad',
    },
    hide: {effect: 'drop', delay: 100, duration: 600, easing: 'easeInQuad'},

  });

  $('#formRead').dialog({
    resizable: false,
    height: 600,
    width: '90%',
    minWidth: 800,
    modal: true,
    autoOpen: false,
    title: 'Просмотр рассылки',
    show: {
      effect: 'drop',
      duration: 300,
      direction: 'up',
      easing: 'easeInQuad',
    },
    hide: {effect: 'drop', delay: 100, duration: 600, easing: 'easeInQuad'},

  });

  $('.readRelease').on('click', function(e) {
    e.preventDefault();
    $('#formRead').dialog('open');
    var url = $(this).attr('href');
    // console.log(url);
    $.ajax({
      url: url,
      method: "GET",
      timeout: 5000,
      cache: false,
      success: function(data) {
        data = $.parseJSON(data);
        // console.log(data);

        var $formRead = $('#formRead');
        $formRead.find('input[name="authorRead"]').val(data[0]['emailFrom']);
        $formRead.find('input[name="themeRead"]').val(data[0]['theme']);
        $formRead.find('textarea[name="templateRead"]').val(data[0]['template']);
        $formRead.find('.dateRead > span').text(data[0]['date']);

      }
    });
  });


  function progressBar(step) {
    var progressBarMain = $('#progressBarMain');

    if (step > 0) {
      progressBarMain.progressbar('value', step);
    }  else {
      progressBarMain.progressbar();
    }
    progressBarMain.progressbar({
      complete: function() {
        $('.loadPost').hide();
        $('.blockLoad').find('h1').text('Отправка писем успешно завершена!');
      }
    });
  }

  $('#newMailing').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    $('#newRelease').dialog('open');
    progressBar(0);
  });


  function sendMail() {
    var $form = $('#formMail');
    var formData = $form.serialize();
    var url = $form.attr('action');
    var method = $form.attr('method');

    $.ajax({
      url: url,
      method: method,
      data: formData,
      timeout: 5000,
      cache: false,
      // contentType: "multipart/form-data",
      // contentType: false,
      // processData: false,
      beforeSend: function() {

      },
      complete: function() {
        /*load off data */
      },
      success: function(data) {
        // data = $.parseJSON(data);
        // console.log(data);
      },
      fail: function() {
        alert('Please try again soon.');
      },
    });
  }

  $('#sendMail').on('click', function(e) {
    e.preventDefault();
    $('#formMail').hide();

    var allStep = $('#allStep').val();
    var stepOne = $('#stepOne').val();
    var $stepNowForm = $('#stepNow');
    var stepNow = $stepNowForm.val();

    stepOne = parseInt(stepOne);

    // console.log("all Step: " + allStep);
    // console.log("step One: " + stepOne);
    // console.log("step Now: " + stepNow);

    $('.blockLoad').removeClass('hide');

    var i = 1;
      setInterval(function() {
        if (i > parseInt(allStep)) {
          $('#newRelease').dialog('close');

          setTimeout(function() {
            location.reload();
          }, 1000);
          return;
        }
        // console.info('i = ' + i);


        $stepNowForm.val(i);
        sendMail();
        progressBar(stepOne * i);
        $('#progressBarMain').progressbar( "widget" );

        i++;
      }, 2000);




  });

});