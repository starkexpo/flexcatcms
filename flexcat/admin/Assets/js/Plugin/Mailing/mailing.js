$(function() {
  var editor = CodeMirror.fromTextArea(document.getElementById('layoutCode'), {
    'mode': 'application/x-httpd-php',
    'lineNumbers': 'true',
    'styleActiveLine': 'true',
    'lineWrapping': 'true',
    'autoCloseTags': 'true',
    'tabSize': '2',
    'indentWithTabs': 'true',
    'smartIndent': 'false',
  });
  editor.setSize(null, '450px');
  

});