<?php

$pathUpload = require_once $_SERVER['DOCUMENT_ROOT'] . '/config/main.php';

$path = $_SERVER['DOCUMENT_ROOT']
    . "/"
    . $pathUpload['upload_folder']
    . "/"
    . $pathUpload['default_img_upload']
    . "/";


require $_SERVER['DOCUMENT_ROOT'] . '/flexcat/Functions/UploadFile.php';

$upload = new \FlexCat\Functions\UploadFile();


$newName = $upload->uploadImage($_FILES['file'], $pathUpload['pages_img_folder'], $path);

$array = array(
    'filelink' =>
        "/" . $pathUpload['upload_folder'] .
        "/" . $pathUpload['default_img_upload'] .
        "/" . $pathUpload['pages_img_folder'] .
        "/" . $newName
);


if ($newName == true) {
    echo stripslashes(json_encode($array));
}

?>