jQuery.noConflict();

jQuery(function($) {
  $('#content').redactor({
    imageUpload: '/flexcat/admin/Assets/js/Plugin/ckeditor/image_upload.php',

  });
});




(function($) {

$('input[name="date_public"]').datepicker({
  dateFormat: 'd.m.Y',
  monthNames: [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'],
  dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
  numberOfMonths: 1,
});

$('input[name="date_delpublic"]').datepicker({
  dateFormat: 'd.m.Y',
  monthNames: [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'],
  dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
  numberOfMonths: 1,
});

$('input[type=file]').on('change', function() {

  var files = this.files;

  // if( typeof files == 'undefined' ) return;

  var data = new FormData();

  $.each(files, function(key, value) {
    data.append(key, value);
  });

  data.append('my_file_upload', 1);

  // AJAX запрос
  $.ajax({
    url: '/flexcat/admin/infosys/info/imgload/',
    type: 'POST', // важно!
    data: data,
    cache: false,
    // dataType    : 'json',
    // отключаем обработку передаваемых данных, пусть передаются как есть
    processData: false,
    // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
    contentType: false,
    // функция успешного ответа сервера
    success: function(data) {
      var newname = $.parseJSON(data);
      $('input[type=file]').slideUp();
      // $('.favicon-load').attr('src', '/upload/img/sites/' + newname);

      $('.ajax-reply').
          prepend('<img src="/upload/img/infosys/' + newname +
              '" class="favicon-load">');

      $('.ajax-reply').removeClass('hide').hide().slideDown();

      $('#bigIMG').val(newname);
    },
    // функция ошибки ответа сервера
    error: function(jqXHR, status, errorThrown) {
      console.log('ОШИБКА AJAX запроса: ' + status, jqXHR);
    },

  });

});

$('input[name="title"]').on('input', function() {
  var title = $(this).val();
  var $form = $('#sendForm');
  var formData = $form.serialize();

  $.ajax({
    type: "POST",
    url: '/flexcat/admin/page/alias/',
    data: formData,
    timeout: 5000,
    cache: false,
    processData: false,

    success: function(data) {
      data = $.parseJSON(data);
      // console.log(data);

      $('input[name="alias"]').val(data);
    },
  });
});

})(jQuery);