$(function() {
  $('.statusCall').on('click', function(e) {
    e.preventDefault();
    var $this = $(this);
    var url = $this.attr('href');


    $this.parent().parent().prev().prev().find('.statusCall').toggleClass('call-green');

    $.ajax({
      type: "GET",
      url: url,
      data: url,
      timeout: 5000,
      cache: false,
      // dataType: 'json',
      processData: false,
      beforeSend: function() {

      },
      complete: function() {
        /*load off data */


      },
      success: function(data) {
        console.log(data);
      },
      fail: function() {
        alert('Please try again soon.');
      },
    });


  });

});