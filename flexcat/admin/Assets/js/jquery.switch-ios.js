(function($) {

  $.fn.switchios = function() {

    $(this).each(function() {
      var $this = $(this);
      var props = $this.is(':checked');
      var onClas = '';

      if ($this.is(':checked')) {
        onClas = 'switchOn';
      } else {
        onClas = '';
      }

      $this.addClass('hide').after('<div class="switch ' + onClas + '"></div>');

    });

    $('body').on('click', '.switch', function() {
      var $this = $(this);
      $this.toggleClass('switchOn');

      var chek = $this.prev(':checkbox').attr('name');
      var $checkbox = $this.prev(':checkbox');

      if ($checkbox.is(':checked')) {
        $checkbox.prop('checked', '');
      } else {
        $checkbox.prop('checked', 'checked');
      }
    });

  };
})(jQuery);