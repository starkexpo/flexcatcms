(function($) {
  $.fn.controlWindow = function() {
    var $this = $(this);

    $this.on('click', '#resizeWindow',function(e) {
      e.preventDefault();
      var element = document.documentElement;

        if (element.requestFullscreen) {
          element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
          element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
          element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
          element.msRequestFullscreen();
        }
        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        }
    });

    $this.on('click', '#menuHide', function(e) {
      e.preventDefault();
      $('.container-left').toggle();
      $(this).toggleClass('activeControlButton');
    });

  };
})(jQuery);