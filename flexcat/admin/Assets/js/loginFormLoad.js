$(function() {

  $('.container-authorization').removeClass('hide').hide();

  setTimeout(function() {
    $('.container-authorization').
        removeClass('hide').
        hide().
        show('drop', {direction: 'up'}, 700);
  }, 700);

  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function background() {
    // var bgBody = 'bg' + Math.floor((Math.random() * 18) + 1);
    var bgBody = 'bg' + getRandomInt(1, 18);
    localStorage.setItem('background', bgBody);

    $('body').
        css('background-image',
            'url("../Assets/img/Background/'
            + bgBody +
            '.jpg")',
        );
  }

  background();

  function sendData(button) {
    var $forms = $('#formLogin');
    var url = $forms.attr('action');
    var dataForm = $forms.serialize();
    var method = $forms.attr('method');

    if (method.toUpperCase() === 'POST') {
      method = 'POST';
    } else {
      method = 'GET';
    }

    $.ajax({
      type: method,
      url: url,
      timeout: 5000,
      data: dataForm,
      beforeSend: function() {
        $(button + ' span').hide();
        $('.loadingImage').show();
      },
      complete: function() {
        $('.loadingImage').hide();
        // console.log('complete');
      },
      success: function(data) {
        if (data) {
          data = $.parseJSON(data);

          // console.info(data);

          switch (data) {
            case 'authorize':
              $('.autorizet-input').removeClass('errors');
              location.reload();
              break;

            case 'errors':
              $('.autorizet-input').addClass('errors');
              $('#formLogin i').css('color', '#FFF');
              $(button + ' span').show();
              $('.loadingImage').hide();
              break;
          }
        }
      },
      error: function() {
        $('.message-error').slideUp(300);
      },
    });
  }

  $('#sendUs').on('click', function(e) {
    e.preventDefault();
    sendData('#sendUs');
  });
});