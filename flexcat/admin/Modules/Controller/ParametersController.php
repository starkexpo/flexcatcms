<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 14/01/2019
 * Time: 04:58
 */

namespace FlexCat\Admin\Modules\Controller;


class ParametersController extends AdminController
{
    public function listing($id)
    {
//        var_dump($id);
        $this->load->model('Parameters');
        $this->data['dynamic'] = $this->model->parameters->getDynamic($id);
        $this->data['options'] = $this->model->parameters->getList($id);

        $this->view->render('parameters/list', $this->data); //$this->data
    }

    public function createOption($id)
    {
        $this->load->model('Parameters');
        $this->data['dynamic'] = $this->model->parameters->getDynamic($id);
//        echo $id;

        $this->view->render('parameters/create', $this->data);
    }

    public function editOption($id)
    {
        $this->load->model('Parameters');
        $this->data['options'] = $this->model->parameters->getViewOption($id);
        $this->data['dynamic'] = $this->model->parameters->getDynamic($this->data['options'][0]['dynamic_id']);


        $this->view->render('parameters/edit', $this->data);
    }

    public function addOption()
    {
        sleep(1);
        $this->load->model('Parameters');
        $params = $this->request->post;

        if (isset($params['title']) &&
            strlen($params['title']) > 1 &&
            strlen($params['variable']) > 0 &&
            $params['dynamic_id'] > 0) {
            $parametersID = $this->model->parameters->createOption($params);

            echo $parametersID;
        }
    }

    public function updateOption()
    {
        sleep(1);
        $this->load->model('Parameters');
        $params = $this->request->post;

        if (isset($params['title']) &&
            strlen($params['title']) > 1 &&
            strlen($params['variable']) > 0 &&
            $params['dynamic_id'] > 0) {
            $parametersID = $this->model->parameters->editOption($params);

            echo $parametersID;
        }
    }

    public function deleteOption($id)
    {
        $this->load->model('Parameters');
        $this->model->parameters->deleteOption($id);
    }
}