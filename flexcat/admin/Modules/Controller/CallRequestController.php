<?php

namespace FlexCat\Admin\Modules\Controller;


class CallRequestController extends AdminController
{
    public function listing()
    {
        $this->load->model('CallRequest');
        $this->data['calls'] = $this->model->callRequest->getListCall();

        $this->view->render('callrequest/list', $this->data);
    }


    public function setStatus($id)
    {
        $this->load->model('CallRequest');
        $this->data['calls'] = $this->model->callRequest->setStatus($id);
    }

    public function delete($id)
    {
        $this->load->model('CallRequest');

        $this->model->callRequest->deleteCall($id);
    }
}