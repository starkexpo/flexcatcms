<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 12/12/2018
 * Time: 04:29
 */

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Template\Controller;
use FlexCat\DI\DI;
use FlexCat\Auth\Auth;
use FlexCat\Database\QueryBuilder;

class LoginController extends Controller
{
    /**
     * @var Auth
     */
    protected $auth;

    /**
     * LoginController constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        parent::__construct($di);

        $this->auth = new Auth();

        if ($this->auth->hashUser() !== null) {
            //redirect
            header('Location: /flexcat/admin/');
            exit();
        }
    }

    public function form()
    {
        $this->view->render('login');
    }

    public function authAdmin()
    {
        $params = $this->request->post;
        $queryBuilder = new QueryBuilder();

        $params['login'] = trim($params['login']);
        $params['login'] = stripcslashes($params['login']);
        $params['login'] = htmlspecialchars($params['login']);

        $params['password'] = trim($params['password']);
        $params['password'] = stripcslashes($params['password']);
        $params['password'] = htmlspecialchars($params['password']);

        $sql = $queryBuilder
            ->select()
            ->from('users')
            ->where('login', $params['login'])
            ->where('password', md5($params['password']))
            ->where('role', 'admin')
            ->limit(1)
            ->sql();

        $query = $this->db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        if (!empty($query)) {
            $user = $query[0];



            if ($user['role'] == 'admin') {



                $hash = md5($user['id'] . $user['login'] . $user['password'] . $this->auth->salt());


                $sql = $queryBuilder
                    ->update('user')
                    ->set(['hash' =>  $hash])
                    ->where('id', $user['id'])->sql();
                $this->db->execute($sql, $queryBuilder->values);


                $this->auth->authorize($hash, $user['login'], $user['id']);



                exit(json_encode('authorize'));
//                header('Location: /flexcat/admin/');
//                exit();
            }
        }

        exit(json_encode('errors'));
//        echo "Incorect email or password";
    }
}