<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 08/01/2019
 * Time: 18:57
 */

namespace FlexCat\Admin\Modules\Controller;


class SectionsController extends AdminController
{
    public function listingSections($id)
    {
        $this->load->model('Sections');
        $this->data['makets'] = $this->model->sections->getListSections($id);

        $this->data['widgetList'] = $this->model->sections->getListWidgets();

        $this->load->model('Layouts');
        $this->data['now_id'] = $this->model->layouts->getView($id);

        $this->view->render('sections/list', $this->data); //$this->data
    }

    public function createSections($id = '')
    {
        $this->load->model('Layouts');
        $this->data['now_id'] = $this->model->layouts->getView($id);

        $this->load->model('Layouts');
        $this->data['sites'] = $this->model->layouts->getActiveSite();

        $this->view->render('sections/create', $this->data); //$this->data
    }

    public function editSections($id)
    {
        $this->load->model('Layouts');
        $this->data['sites'] = $this->model->layouts->getActiveSite();

        $this->load->model('Sections');
        $this->data['nowEdit'] = $this->model->sections->getViewSection($id);


        $this->view->render('sections/edit', $this->data); //$this->data
    }

    public function addSections()
    {
//        sleep(1);
        $this->load->model('Sections');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->sections->createSection($params);

            echo $sectionID;
        }
    }

    public function updateSections()
    {
//        sleep(1);
        $this->load->model('Sections');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->sections->editSection($params);

            echo $sectionID;
        }
    }

    public function deleteSections($id, $foldersTemplate = '')
    {
        $this->load->model('Widget');
        $widgets = $this->model->widget->getListWidgets($id);
        foreach ($widgets as $widget) {
            $this->model->widget->deleteWidget($widget['id']);
        }

        $this->load->model('Sections');
        $section = $this->model->sections->getViewSection($id);


        if (strlen($foldersTemplate) <= 0) {
            $this->load->model('Layouts');
            $layouts = $this->model->layouts->getLayoutsData($section[0]['layouts_id']);

            $this->load->model('Sites');
            $sites = $this->model->sites->getSiteData($layouts->site);

            $this->load->model('Sections');
            $folderSite = $this->model->sections->getSite($sites->template);
        }

        $fileName = $section[0]['alias'] . '.php';
        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $folderSite[0]['folder'] . "/";

        if (@chdir($path) === true) {
            @unlink($path . $fileName);
        }

        $this->load->model('Sections');
        $this->model->sections->deleteSection($id);
    }
}