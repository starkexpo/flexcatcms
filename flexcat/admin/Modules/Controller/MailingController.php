<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 27/01/2019
 * Time: 22:34
 */

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Helper\TestEmail;

class MailingController extends AdminController
{
    public $step = 2;

    public function listing()
    {
        $this->load->model('Mailing');
        $this->data['mailing'] = $this->model->mailing->getListMailing();

        $this->view->render('mailing/list', $this->data); // $this->data
    }

    public function listingRelease($id)
    {
        $this->load->model('Mailing');
        $this->data['release'] = $this->model->mailing->getListRelease($id);
        $this->data['nowMailing'] = $this->model->mailing->getListMailing($id);
        $this->data['users'] = $this->model->mailing->getListUsers($id);
        $this->data['limitStep'] = $this->step;


        $this->load->model('User');
        $this->data['admines'] = $this->model->user->getUsers();


        $this->view->render('mailing/listRelease', $this->data);
    }

    public function listingUsers($id)
    {
        $this->load->model('Mailing');
        $this->data['users'] = $this->model->mailing->getListUsers($id);
        $this->data['nowMailing'] = $this->model->mailing->getListMailing($id);


        $this->view->render('mailing/listUsers', $this->data);
    }

    public function createMailing()
    {
        $this->view->render('mailing/createMailing');
    }

    public function addMailing()
    {
        sleep(1);
        $this->load->model('Mailing');
        $params = $this->request->post;

        $subscribesListID = $this->model->mailing->createMailing($params);
    }

    public function createUser($id)
    {
        $this->load->model('Mailing');
        $this->data['mailing'] = $this->model->mailing->getListMailing();
        $this->data['nowID'] = $id;

        $this->view->render('mailing/createUser', $this->data);
    }

    public function editUsers($id)
    {
        $this->load->model('Mailing');
        $this->data['users'] = $this->model->mailing->getUser($id);
        $this->data['mailing'] = $this->model->mailing->getListMailing();

        $this->view->render('mailing/editUser', $this->data);
    }

    public function addUser()
    {
        sleep(1);
        $this->load->model('Mailing');
        $params = $this->request->post;

        $validating = TestEmail::verificationEmail($params['email']);

        if (strlen($params['name']) > 0 && $params['mailing'] > 0 && $validating) {

            $subscribesID = $this->model->mailing->createUser($params);
        }
    }

    public function updateUsers()
    {
        sleep(1);
        $this->load->model('Mailing');
        $params = $this->request->post;

        $validating = TestEmail::verificationEmail($params['email']);

        if (strlen($params['name']) > 0 && $params['mailing'] > 0 && $validating) {

            $subscribesID = $this->model->mailing->updateUser($params);
        }
    }

    public function deleteUsers($id)
    {
        $this->load->model('Mailing');

        $this->model->mailing->deleteUser($id);
    }

    public function mailingEdit($id)
    {
        $this->load->model('Mailing');
        $this->data['mailing'] = $this->model->mailing->getListMailing($id);

        $this->view->render('mailing/editMailing', $this->data);
    }

    public function mailingUpdate()
    {
        sleep(1);
        $this->load->model('Mailing');
        $params = $this->request->post;

        $subscribesListID = $this->model->mailing->editMailing($params);
    }

    public function sendMail()
    {
        $this->load->model('Mailing');
        $params = $this->request->post;

//        print_r($params);

        if (!empty($_FILES['mail_file']['tmp_name'])) {
            // Загружаем файл
            $path = $_FILES['mail_file']['name'];
            if (copy($_FILES['mail_file']['tmp_name'], $path)) {
                $picture = $path;
            }

//            print_r($path);
        }

        $usersList = $this->model->mailing->getListUsers($params['mailing_id']);


        $stepLimit = intval($this->step);                   // = 1 (кол-во писем за раз)
        $endNum = $stepLimit * intval($params['stepNow']);  // 1 * на шаг (1, 2, 3, ...)

        if ($params['stepNow'] == 1) {
            $emailFrom = $params['author'];
            $themeFrom = $params['theme'];
            $templateFrom = $params['template'];
            $parent = $params['mailing_id'];
            $admin_id = $_SESSION['id'];

            $this->db->query("INSERT INTO mailing (emailFrom, theme, template, parent, admin_id) VALUES('$emailFrom', '$themeFrom', '$templateFrom', '$parent', '$admin_id')");
        }

        $startNum = $endNum - $stepLimit;
        $startNum = $startNum + 1;

        print_r($_SESSION);

        echo "<br>";

        echo "start: " . $startNum . " end: " . $endNum;
        $j = 1;
        $emailList = array();

        foreach ($usersList as $item) {
            $emailItem = $item['email'];
            $nameItem = $item['name'];
            $code = $item['code'];

            if ($j >= $startNum && $j <= $endNum) {
                $emailList[] = ['email' => $emailItem, 'name' => $nameItem, 'code' => $code];
            }


            $j++;
        }

        echo "<br>";
        print_r($emailList);




        $this->htmlimgmail($emailList, $params['theme'], $params['template'], $path, $params['author']);

    }


    public function htmlimgmail($mail_to, $thema, $html, $path, $from)
    {
        $EOL = "\n";
        $boundary = "--" . md5(uniqid(time()));
        $headers = "MIME-Version: 1.0;$EOL";
        $headers .= "From: $from $EOL";
        $headers .= "Content-Type: multipart/related; " .
            "boundary=\"$boundary\"$EOL";

        $multipart = "--{$boundary}$EOL";
        $multipart .= "Content-Type: text/html; charset=utf-8$EOL";
        $multipart .= "Content-Transfer-Encoding: 8bit$EOL";
        $multipart .= $EOL;
        if ($EOL == "\n") {
            $multipart .= str_replace("\r\n", $EOL, $html);
        }
        $multipart .= $EOL;



        if (strlen($path) <= 1) {
            for ($i = 0; $i < count($path); $i++) {
                $file = file_get_contents($path[$i]);
                $name = basename($path[$i]);
                $multipart .= "$EOL--$boundary$EOL";
                $multipart .= "Content-Type: image/jpeg; name=\"$name\"$EOL";
                $multipart .= "Content-Transfer-Encoding: base64$EOL";
                $multipart .= "Content-ID: <" . md5($name) . ">$EOL";
                $multipart .= $EOL;
                $multipart .= chunk_split(base64_encode($file), 76, $EOL);
            }
        } else {
            $fp = fopen($path, "r");
            if (!$fp) {
                print "Файл $path не может быть прочитан";
                exit();
            }
            $file = fread($fp, filesize($path));
            fclose($fp);

            $boundary = "--" . md5(uniqid(time())); // генерируем разделитель
            $headers .= "MIME-Version: 1.0\n";
            $headers .= "Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
            $multipart .= "--$boundary\n";
            $kod = 'utf-8'; // или $kod = 'windows-1251';
            $multipart .= "Content-Type: text/html; charset=$kod\n";
            $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
            $multipart .= "$html\n\n";

            $message_part = "--$boundary\n";
            $message_part .= "Content-Type: application/octet-stream\n";
            $message_part .= "Content-Transfer-Encoding: base64\n";
            $message_part .= "Content-Disposition: attachment; filename = \"" . $path . "\"\n\n";
            $message_part .= chunk_split(base64_encode($file)) . "\n";
            $multipart .= $message_part . "--$boundary--\n";

        }

        $multipart .= "$EOL--$boundary--$EOL";
        if (!is_array($mail_to)) {
            // Письмо отправляется одному адресату
            if (!mail($mail_to, $thema, $multipart, $headers)) {
                return false;
            } else {
                return true;
            }
            exit;
        } else {
            // Письмо отправляется на несколько адресов
            foreach ($mail_to as $mail) {

                if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS']=='on')) {
                    $protocol = 'https://';
                } else {
                    $protocol = 'http://';
                }
                $unlinkSubscribe = $protocol . $_SERVER['HTTP_HOST'] . "/unsubscribe/" . $mail['code'];

                /*$multipart = strtr($multipart, [
                    "{LINK}" => $unlinkSubscribe,
                ]);*/

                $multipart = str_replace("{LINK}", $unlinkSubscribe, $multipart);

                mail($mail['email'], $thema, $multipart, $headers);
            }
        }
    }

    public function readRelease($id)
    {
        $this->load->model('Mailing');
        $release = $this->model->mailing->getViewRelease($id);

        echo json_encode($release);
    }

    public function deleteRelease($id)
    {
        $this->load->model('Mailing');
        $this->model->mailing->deleteRelease($id);
    }
}