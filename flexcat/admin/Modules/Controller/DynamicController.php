<?php

namespace FlexCat\Admin\Modules\Controller;

class DynamicController extends AdminController
{
    public function listingDPage($id = '')
    {
        $this->load->model('Dynamic');
        $this->data['dynamics'] = $this->model->dynamic->getList($id);
        $this->data['section_id'] = $id;

        $this->view->render('dynamic/list', $this->data); //$this->data
    }

    public function listView($id = '')
    {
        $this->load->model('Dynamic');
        $this->data['dynamics'] = $this->model->dynamic->getList($id);
//        $this->data['section_id'] = $id;

        return $this->data;
    }

    public function createSection($id = '')
    {
        $this->load->model('Dynamic');

        $this->data['sections'] = $this->model->dynamic->getListSection();
        $this->data['sections_now'] = $id;

        $this->view->render('dynamic/createSections', $this->data);
    }

    public function editSection($id)
    {
        $this->load->model('Dynamic');

        $this->data['sections'] = $this->model->dynamic->getListSection();
        $this->data['sectionsEdit'] = $this->model->dynamic->getSectionData($id);

        $this->view->render('dynamic/editSections', $this->data);
    }

    public function addSection()
    {
        sleep(1);
        $this->load->model('Dynamic');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->dynamic->createSection($params);

            echo $sectionID;
        }
    }

    public function updateSection()
    {
        sleep(1);
        $this->load->model('Dynamic');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->dynamic->updateSection($params);

            echo $sectionID;
        }
    }

    public function deleteSection($id)
    {
        $templates = $this->listView($id);

        foreach ($templates as $key => $template) {

            foreach ($template as $item) {
                if ($item['type'] == 'template') {
                    $this->deleteTemplate($item['id']);
                } elseif ($item['type'] == 'section') {
                    $this->deleteSection($item['id']);
                }
            }
        }

        $this->load->model('Dynamic');
        $this->model->dynamic->deleteTemplate($id);
    }

    public function createTemplate($id = '')
    {
        $this->load->model('Dynamic');

        $this->data['sections'] = $this->model->dynamic->getListSection();
        $this->data['sections_now'] = $id;

        $this->view->render('dynamic/createTemplate', $this->data);
    }

    public function editTemplate($id)
    {
        $this->load->model('Dynamic');

        $this->data['sections'] = $this->model->dynamic->getListSection();
        $this->data['sections_now'] = $this->model->dynamic->getSectionData($id);

        $this->view->render('dynamic/editTemplate', $this->data);
    }

    public function addTemplate()
    {
//        sleep(1);
        $this->load->model('Dynamic');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1 && strlen($params['code']) > 1) {
            $templatenID = $this->model->dynamic->createTemplate($params);

            echo $templatenID;
        }
    }

    public function updateTemplate()
    {
//        sleep(1);
        $this->load->model('Dynamic');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1 && strlen($params['code']) > 1) {
            $templatenID = $this->model->dynamic->editTemplate($params);

            echo $templatenID;
        }
    }

    public function deleteTemplate($id)
    {
        $this->load->model('Parameters');
        $options = $this->model->parameters->getList($id);

        foreach ($options as $option) {
            $this->model->parameters->deleteOption($option['id']);
        }

        $folder = "template" . $id;
        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Dynamic/";


        if (@chdir($path . $folder) === true) {

            @unlink($path . $folder . "/" . "template.php");
            @unlink($path . $folder . "/" . "style.css");
            @unlink($path . $folder . "/" . "javascript.js");

            @chdir($path);
            @rmdir($folder);
        }

        $this->load->model('Dynamic');
        $this->model->dynamic->deleteTemplate($id);
    }
}