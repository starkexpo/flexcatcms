<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 09/01/2019
 * Time: 02:58
 */

namespace FlexCat\Admin\Modules\Controller;


class TpltempController extends AdminController
{
    public function getList($id = '')
    {
        $this->load->model('Sites');
        $this->data['sites'] = $this->model->sites->getSites();
//
        $this->load->model('Tpltemp');
        $this->data['tpl_temp'] = $this->model->tpltemp->getList($id);

        if (isset($id) && $id > 0) {
            $this->data['section_id'] = $id;
        }

        $this->view->render('Tpltemp/list', $this->data); //$this->data
    }

    public function createSection($id = '')
    {
        $this->load->model('Layouts');
        $this->data['sites'] = $this->model->layouts->getActiveSite();

        $this->load->model('Tpltemp');
        $this->data['sections'] = $this->model->tpltemp->getSections();

        if (isset($id) && $id > 0) {
            $this->data['section_id'] = $id;
        }

        $this->view->render('Tpltemp/createSection', $this->data); //$this->data
    }

    public function createTpl($id = '')
    {
        $this->load->model('Layouts');
        $this->data['sites'] = $this->model->layouts->getActiveSite();

        $this->load->model('Tpltemp');
        $this->data['sections'] = $this->model->tpltemp->getSections();

        if (isset($id) && $id > 0) {
            $this->data['section_id'] = $id;
        }

        $this->view->render('Tpltemp/createTpl', $this->data);
    }

    public function editSection($id)
    {
        $this->load->model('Layouts');
        $this->data['sites'] = $this->model->layouts->getActiveSite();

        $this->load->model('Tpltemp');
        $this->data['sections'] = $this->model->tpltemp->getSections();
        $this->data['sectionsEdit'] = $this->model->tpltemp->getSectionData($id);

        if (isset($id) && $id > 0) {
            $this->data['section_id'] = $id;
        }

        $this->view->render('Tpltemp/editSection', $this->data); //$this->data
    }

    public function addSection()
    {
//        sleep(1);
        $this->load->model('Tpltemp');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $TpltempID = $this->model->tpltemp->createSection($params);

            echo $TpltempID;
        }
    }

    public function addTpl()
    {
//        sleep(1);
        $this->load->model('Tpltemp');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $TpltempID = $this->model->tpltemp->createTpl($params);

            echo $TpltempID;
        }
    }

    public function updateSection()
    {
//        sleep(1);
        $this->load->model('Tpltemp');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $TpltempID = $this->model->tpltemp->editSection($params);

            echo $TpltempID;
        }
    }
}