<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 06/01/2019
 * Time: 19:14
 */

namespace FlexCat\Admin\Modules\Controller;


class LayoutsController extends AdminController
{
    public function listingLayouts($id = '')
    {
        $this->load->model('Layouts');
        $this->data['layouts'] = $this->model->layouts->getListIndex($id);

        $this->load->model('Layouts');
        $this->data['now_id'] = $this->model->layouts->getView($id);

        $this->data['sectionsList'] = $this->model->layouts->getListSections();


        $this->load->model('Sites');
        $this->data['sites'] = $this->model->sites->getSites();


        $this->view->render('layouts/list', $this->data); //$this->data
    }

    public function createLayouts($id = '')
    {
        $this->load->model('Layouts');

        $this->data['sections'] = $this->model->layouts->getListSection();
        $this->data['layouts'] = $this->model->layouts->getListLayouts();
        $this->data['now_id'] = $this->model->layouts->getView($id);


        $this->view->render('layouts/createLayouts', $this->data);
    }

    public function createSection($id = '')
    {
        $this->load->model('Layouts');

        $this->data['sites'] = $this->model->layouts->getActiveSite();
        $this->data['sections'] = $this->model->layouts->getListSection();
        $this->data['sections_now'] = $id;

        $this->view->render('layouts/createSection', $this->data);
    }

    public function editSection($id)
    {
        $this->load->model('Layouts');

        $this->data['sites'] = $this->model->layouts->getActiveSite();
        $this->data['sections'] = $this->model->layouts->getListSection();


        $this->data['sectionsEdit'] = $this->model->layouts->getSectionData($id);

        $this->view->render('layouts/editSection', $this->data);
    }

    public function editLayouts($id)
    {
        $this->load->model('Layouts');

        $this->data['sections'] = $this->model->layouts->getListSection();
        $this->data['layouts'] = $this->model->layouts->getListLayouts();

        $this->data['layoutsEdit'] = $this->model->layouts->getLayoutsData($id);

        $this->view->render('layouts/editLayouts', $this->data);
    }

    public function addSection()
    {
        sleep(1);
        $this->load->model('Layouts');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->layouts->createSection($params);

            echo $sectionID;
        }
    }


    public function addLayouts()
    {
//        sleep(1);
        $this->load->model('Layouts');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->layouts->createLayouts($params);

            echo $sectionID;
        }
    }

    public function updateSection()
    {
        sleep(1);
        $this->load->model('Layouts');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1 && $params['sections_id'] > 0) {
            $sectionID = $this->model->layouts->editSection($params);

            echo $sectionID;
        }
    }

    public function updateLayouts()
    {
        sleep(1);
        $this->load->model('Layouts');
        $params = $this->request->post;

//        var_dump($params);


        if (isset($params['title']) && strlen($params['title']) > 1) {
            $sectionID = $this->model->layouts->editLayouts($params);


            echo $sectionID;
        }
    }

    public function deleteLayouts($id)
    {
        $this->load->model('Layouts');
        $layouts = $this->model->layouts->getView($id);
        $list = $this->model->layouts->getListIndex($layouts[0]['id']);

//        var_dump($list);

        $this->load->model('Sites');
        $sites = $this->model->sites->getSiteData($layouts[0]['site']);

        $this->load->model('Layouts');
        $siteTemplate = $this->model->layouts->getSiteTemplate($sites->template);

//        var_dump($siteTemplate[0]['folder']);

        foreach ($list as $item) {
//            var_dump($item);
            if ($item['parent_layouts'] > 0) {


//                $this->deleteLayouts($item['id']);


                //delete Section and Widget
                $this->load->model('Sections');
                $sections = $this->model->sections->getListSections($item['id']);
                foreach ($sections as $section) {


                    $this->load->model('Widget');
                    $widgets = $this->model->widget->getListWidgets($section['id']);
                    foreach ($widgets as $widget) {
                        $this->model->widget->deleteWidget($widget['id']);
                    }

                    $fileName = $section['alias'] . '.php';
                    $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $siteTemplate[0]['folder'] . "/";

                    if (@chdir($path) === true) {
                        @unlink($path . $fileName);
                    }

                    $this->load->model('Sections');
                    $this->model->sections->deleteSection($section['id']);
                }

                //delete layouts
                $this->load->model('Layouts');
                $this->model->layouts->deleteLayouts($item['id']);

                if (@chdir($path) === true) {

                    if ($item['parent_layouts'] == 0) {
                        $fileName = "index.php";
                    } else {
                        $fileName = "layouts" . $item['id'] . ".php";
                    }

                    $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $siteTemplate[0]['folder'] . "/";
                    @unlink($path . $fileName);
                }



                $this->deleteLayouts($item['id']);
//                echo "<br><h1>Delete</h1>";
            }
        }

        $this->load->model('Layouts');
        $this->model->layouts->deleteLayouts($id);
    }

    public function deleteSections($id)
    {
        $this->load->model('Layouts');
        $this->model->layouts->deleteLayouts($id);
    }
}