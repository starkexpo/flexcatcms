<?php

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Admin\Modules\Model\Sidebar\SidebarRepository;

class SidebarController extends AdminController
{

    public static function buildingMenu()
    {
        $sidebar = new SidebarRepository();
        $list = json_decode($sidebar->getList(), true);

        return  $list;
    }

    public static function getUser($id)
    {
        $sidebar = new SidebarRepository();
        $user = json_decode($sidebar->getUser($id), true);

        return  $user;
    }
}