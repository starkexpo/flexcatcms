<?php

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Template\Controller;
use FlexCat\Auth\Auth;


class AdminController extends Controller
{

    /**
     * @var Auth
     */
    protected $auth;

    /**
     * @var array
     */
    public $data = [];

    /**
     * AdminController constructor.
     * @param $di
     */
    public function __construct($di)
    {
        parent::__construct($di);

        $this->auth = new  Auth();


        if ($this->auth->hashUser() == null) {
            header('Location: /flexcat/admin/login/');
            exit();
        }


    }


    /**
     * Check Auth
     */
    public function checkAuthorization()
    {

    }


    public function logout()
    {
        $this->auth->unAuthorize();

        //redirect
        header('Location: /flexcat/admin/login/');
        exit();
    }
}