<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 08/01/2019
 * Time: 18:57
 */

namespace FlexCat\Admin\Modules\Controller;


use FlexCat\Admin\Modules\Model\Widget\WidgetRepository;

class WidgetController extends AdminController
{
    public function listLibrary($id)
    {
        $this->load->model('Widget');

        $this->data['makets'] = $this->model->widget->getViewSection($id);
        $this->data['widgets'] = $this->model->widget->getListWidgets($id);
        $this->data['dynamicsTemplate'] = $this->model->widget->getListTemplateDynamic();

        $this->data['now_id'] = $id;

        $this->view->render('widget/list', $this->data); //$this->data
    }


    public function createWidget($id)
    {
        $this->load->model('Widget');
        $this->data['makets'] = $this->model->widget->getViewSection($id);


        $this->load->model('Dynamic');
        $this->data['dynamic_sections'] = $this->model->dynamic->getListSection();

        $this->load->model('Widget');
        $this->data['dynamic_template'] = $this->model->widget->getListTemplate();

        $this->data['now_id'] = $id;

        $this->view->render('widget/create', $this->data); //$this->data
    }

    public function editWidget($id)
    {
        $this->load->model('Widget');
        $this->data['widgets'] = $this->model->widget->getViewWidget($id);

        $sectionID = $this->data['widgets'][0]['sections_id'];
        $this->load->model('Widget');
        $this->data['makets'] = $this->model->widget->getViewSection($sectionID);

        $this->load->model('Dynamic');
        $this->data['dynamic_sections'] = $this->model->dynamic->getListSection();

        $this->load->model('Widget');
        $this->data['dynamic_template'] = $this->model->widget->getListTemplate();

        $this->load->model('Parameters');
        $this->data['options'] = $this->model->parameters->getList($this->data['widgets'][0]['dynamic_template']);

        $this->view->render('widget/edit', $this->data); //$this->data
    }

    public function addWidget()
    {
        sleep(1);
        $this->load->model('Widget');
        $params = $this->request->post;


        if (isset($params['dynamic_template']) && $params['dynamic_template'] > 0) {
            $widgetID = $this->model->widget->createWidget($params);

            echo $widgetID;
        }
    }

    public function updateWidget()
    {
        sleep(1);
        $this->load->model('Widget');
        $params = $this->request->post;

//        var_dump($params);
//        exit();

        if (isset($params['dynamic_template']) && $params['dynamic_template'] > 0) {
            $widgetID = $this->model->widget->editWidget($params);

            echo $widgetID;
        }

        $this->load->model('Parameters');
        $optiontID = $this->model->parameters->editValue($params);
    }

    public function setPublic($id)
    {
        $this->load->model('Widget');
        $status = $this->model->widget->statusWidget($id);

        echo $status;
    }

    public function deleteWidget($id)
    {
        $this->load->model('Widget');
        $deleteWidget = $this->model->widget->deleteWidget($id);
    }

    public function optionsWidget($id)
    {
        $this->load->model('Parameters');
        $options = $this->model->parameters->getList($id);

        foreach ($options as $option) {

            if ($option['field_type'] == "input") {
                if (strlen($option['value']) > 0) {
                    $value = $option['value'];
                } elseif (strlen($option['defaults']) > 0) {
                    $value = $option['defaults'];
                } else {
                    $value = '';
                }

                echo '<label for="formTitle">' . $option['title'] . '</label>' .
                    '<input type="text" name="options' . $option['id'] . '" class="input-form" value="' . $value . '"><br>';
            }

            if ($option['field_type'] == "sql") {
                echo '<br><br><label for="formTitle">' . $option['title'] . '</label><br>';
                $i = $option['id'];
                $sql_list[$i] = \FlexCat\Admin\Modules\Model\Widget\WidgetRepository::getSQL($option['field_sql']);

                if (!empty($sql_list)){
                          echo '<select name="options' . $option['id'] . '" class="sql_select">';
                                foreach ($sql_list[$i] as $value) {

                                    if ($option['value'] == $value[$option['field_value']]) {
                                        $select = ' selected="selected"';
                                    } else {
                                        $select = '';
                                    }

                                    echo '<option value="' . $value[$option['field_value']] . '" ' . $select . '>';
                                        echo $value[$option['field_title']];
                                    echo '</option>';
                                }
                          echo '</select>';
                }
            }
        }
    }


    public static function loadWidget($blockID)
    {
        $listWidget = json_decode(\FlexCat\Admin\Modules\Model\Widget\WidgetRepository::getListBlockWidgets($blockID));

        foreach ($listWidget as $item) {

            $optionList = json_decode(\FlexCat\Admin\Modules\Model\Widget\WidgetRepository::getListOption($item->dynamic_template));


            if (!empty($optionList)) {
                foreach ($optionList as $option) {

                    if (strlen($option->value) <= 0) {
                        $value = $option->defaults;
                    } else {
                        $value = $option->value;
                    }

                    $variable = '$' . $option->variable . ' = "' . $value . '";';
                    eval($variable);
                }


            }

            require $_SERVER['DOCUMENT_ROOT'] . '/Template/Dynamic/template' . $item->dynamic_template . '/template.php';
        }

        if (empty($listWidget)) {
            $blockName = json_decode(\FlexCat\Admin\Modules\Model\Sections\SectionsRepository::gelBlockSection($blockID));

            echo $blockName[0]->title;
        }
    }
}