<?php

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Helper\Alias;

class PageController extends AdminController
{

    public function listing($id = '')
    {
        $this->load->model('Page');

        $this->data['pages'] = $this->model->page->getPages($id);
        $this->data['now_id'] = $id;

        $this->view->render('pages/list', $this->data);
    }


    public function create($id = '0')
    {
        $this->load->model('Page');
        $this->data['sections'] = $this->model->page->getListSections();
        $this->data['now_section'] = $id;

        $this->view->render('pages/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Page');

        $this->data['page'] = $this->model->page->getPageData($id);
        $this->data['sections'] = $this->model->page->getListSections();

        $this->view->render('pages/edit', $this->data);
    }

    public function add()
    {
        sleep(1);
        $this->load->model('Page');
        $params = $this->request->post;


        if (!isset($params['published']) or $params['published'] <> 'enable') {
            $params['published'] = 'disabled';
        }

        if (!isset($params['date']) or $params['date'] == '') {
            $params['date'] = date("d.m.Y", time());
        }

        if (isset($params['title'])) {
            $pageID = $this->model->page->createPage($params);

            echo $pageID;
        }
    }

    public function update()
    {
        sleep(1);
        $this->load->model('Page');
        $params = $this->request->post;

        if (!isset($params['published']) or $params['published'] <> 'enable') {
            $params['published'] = 'disabled';
        }

        if (!isset($params['date']) or $params['date'] == '') {
            $params['date'] = date("d.m.Y", time());
        }

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $pageID = $this->model->page->updatePage($params);

            echo json_encode('ok');
        }
        else {
            echo json_encode('error');
        }
    }

    public function aliasAjax()
    {
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            echo json_encode(\FlexCat\Helper\Alias::transliteration($params['title']));
        }
    }

    public function createSection($id = '')
    {
        $this->load->model('Page');
        $this->data['sections'] = $this->model->page->getListSections();
        $this->data['sections_now'] = $id;

        $this->view->render('pages/create_section', $this->data);
    }

    public function editSection($id)
    {
        $this->load->model('Page');
        $this->data['section_id'] = $this->model->page->getPageData($id);
        $this->data['sections'] = $this->model->page->getListSections();

        $this->view->render('pages/edit_section', $this->data);
    }

    public function addSection()
    {
        sleep(1);
        $this->load->model('Page');
        $params = $this->request->post;

        if (isset($params['title'])) {
            $pageID = $this->model->page->createSections($params);

            echo $pageID;
        }
    }

    public function updateSection()
    {
        sleep(1);
        $this->load->model('Page');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $pageID = $this->model->page->updateSection($params);
        }
    }

    public function deletePage($id)
    {
        $this->load->model('Page');
        $page = $this->model->page->getPageData($id);

        if ($page->type == 'section') {
            $section = $this->model->page->getPages($page->id);

            foreach ($section as $item) {

                if ($item['type'] == 'section') {
                    $this->deletePage($item['id']);
                    $this->model->page->deletePage($item['id']);
                } else {
                    $this->model->page->deletePage($item['id']);
                }
            }

        } elseif ($page->type == 'page') {
            $this->model->page->deletePage($id);
            echo "delete page";
        }

        $this->model->page->deletePage($id);
    }
}