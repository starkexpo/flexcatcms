<?php

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Functions\UploadFile;

class UserController extends AdminController
{
    public function listing()
    {
        $this->load->model('User');
        $this->data['users'] = $this->model->user->getUsers();

        $this->view->render('user/list', $this->data);
    }

    public function listingUserSite()
    {
        $this->load->model('User');
        $this->data['users'] = $this->model->user->getUserSite();

        $this->view->render('usersite/list', $this->data);
    }

    public function createUser()
    {
        $this->view->render('user/create_user');
    }

    public function editUser($id)
    {
        $this->load->model('User');
        $this->data['user'] = $this->model->user->getUsers($id);

        $this->view->render('user/edit_user', $this->data);
    }

    public function addUser()
    {
        sleep(1);
        $this->load->model('User');
        $params = $this->request->post;

        if (strlen($params['login']) > 1 && strlen($params['password']) > 8 && ($params['password'] == $params['password2'])) {

            $userID = $this->model->user->createUser($params);
        }
    }

    public function updateUser()
    {
        sleep(1);
        $this->load->model('User');
        $params = $this->request->post;

        if (strlen($params['login']) > 1) {

            $userID = $this->model->user->updateUser($params);
        }
    }

    public function avatar()
    {
        if (isset($_POST['my_file_upload'])) {
            $files = $_FILES[0]; // полученные файлы

            $upload = new UploadFile();
            $filename = $upload->uploadImage($files, "avatar");

            echo json_encode($filename);
        }
    }

    public function avatarDelete($id)
    {
//        var_dump($id);

        $this->load->model('User');

        $user = $this->model->user->avatarUpdate($id);

        header("Location:".$_SERVER['HTTP_REFERER']);
    }

    public function deleteUser($id)
    {
        $this->load->model('User');
        $deleteUser = $this->model->user->deleteUser($id);
    }

}