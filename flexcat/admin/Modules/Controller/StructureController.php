<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 04/01/2019
 * Time: 17:19
 */

namespace FlexCat\Admin\Modules\Controller;


class StructureController extends AdminController
{
    public function listing($id = '')
    {
        $this->load->model('Items');
        $this->data['items'] = $this->model->items->getListItem($id);

        $this->load->model('Menu');
        $this->data['menusList'] = $this->model->menu->getMenu();

        if (isset($id) && $id > 0) {
            $this->data['now_id'] = $id;
        }

        $this->view->render('structure/list', $this->data); //, $this->data
    }

    public function createItem($id = '')
    {
        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getNowActive();
        $idSite = intval($nowSite[0]['id']);

        $this->load->model('Menu');
        $this->data['menusList'] = $this->model->menu->getMenu();

        $this->load->model('Layouts');
        $this->data['layouts'] = $this->model->layouts->getListLayouts($idSite);

        $this->load->model('Page');
        $this->data['pagesList'] = $this->model->page->getListPages();
        $this->data['pagesSection'] = $this->model->page->getListSections();

        $this->load->model('Items');
        $this->data['listItems'] = $this->model->items->getListItem();

        $this->load->model('Dynamic');
        $this->data['dynamic_sections'] = $this->model->dynamic->getListSection();

        $this->load->model('Widget');
        $this->data['dynamic_template'] = $this->model->widget->getListTemplate();

        if (isset($id) && $id > 0) {
            $this->data['now_id'] = $id;
        }

        $this->view->render('structure/create_item', $this->data);
    }


    public function editItem($id)
    {
        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getNowActive();
        $idSite = intval($nowSite[0]['id']);

        $this->load->model('Menu');
        $this->data['menusList'] = $this->model->menu->getMenu();

        $this->load->model('Layouts');
        $this->data['layouts'] = $this->model->layouts->getListLayouts($idSite);

        $this->load->model('Page');
        $this->data['pagesList'] = $this->model->page->getListPages();
        $this->data['pagesSection'] = $this->model->page->getListSections();

        $this->load->model('Items');
        $this->data['listItems'] = $this->model->items->getListItem();
        $this->data['now_id'] = $this->model->items->getItemsData($id);

        $this->load->model('Dynamic');
        $this->data['dynamic_sections'] = $this->model->dynamic->getListSection();

        $this->load->model('Widget');
        $this->data['dynamic_template'] = $this->model->widget->getListTemplate();

        $this->load->model('Parameters');
        $this->data['options'] = $this->model->parameters->getList($this->data['now_id']->dynamic_page);

        $this->view->render('structure/edit_item', $this->data);
    }

    public function addItem()
    {
        sleep(1);
        $params = $this->request->post;

        $this->load->model('Parameters');
        $optionLists = $this->model->parameters->getList($params['dynamic_template']);
        $this->model->parameters->resets($params['dynamic_template']);

        $params['options'] = $optionLists;
        $this->load->model('Items');

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $itemsID = $this->model->items->createItems($params);

            echo $itemsID;
            $params['last_items'] = $itemsID;

            $this->load->model('Routing');
            $routeID = $this->model->routing->createRoute($params);
        }
    }

    public function updateItem()
    {
        sleep(1);
        $params = $this->request->post;

        $this->load->model('Parameters');
        $optionLists = $this->model->parameters->getList($params['dynamic_template']);
        $this->model->parameters->resets($params['dynamic_template']);


        $params['options'] = $optionLists;
        $this->load->model('Items');

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $itemsID = $this->model->items->editItems($params);

            $this->load->model('Routing');
            $routeID = $this->model->routing->editRoute($params);
        }
    }

    /* load page */
    public function loadStructurePage($id)
    {
        $this->load->model('Page');

        $page = $this->model->page->getPageData($id);

//        var_dump($page);

        echo json_encode($page->content);
    }

    public function setPublic($id)
    {
        $this->load->model('Items');
        $status = $this->model->items->statusItems($id);

        $this->load->model('Routing');
        $routeID = $this->model->routing->statusRoute($id, $status);

        echo $status;
    }


    public function deleteItem($id)
    {
        $this->load->model('Items');
        $deleteItem = $this->model->items->deleteItem($id);
    }

    /*menu*/

    public function listingMenu()
    {
        $this->load->model('Menu');

        $this->data['menusList'] = $this->model->menu->getMenu();


        $this->load->model('Sites');

        $this->data['sites'] = $this->model->sites->getSites();

        $this->view->render('structure/list_menu', $this->data);
    }

    public function createMenu()
    {
        $this->load->model('Sites');

        $this->data['sites'] = $this->model->sites->getSites();

        $this->view->render('structure/create_menu', $this->data);
    }

    public function editMenu($id)
    {
        $this->load->model('Menu');

        $this->data['menus'] = $this->model->menu->getMenuData($id);


        $this->load->model('Sites');

        $this->data['sites'] = $this->model->sites->getSites();


        $this->view->render('structure/edit_menu', $this->data);
    }

    public function addMenu()
    {
        sleep(1);
        $this->load->model('Menu');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $menuID = $this->model->menu->createMenu($params);

            echo $menuID;
        }
    }

    public function deleteMenu($id)
    {
        $this->load->model('Items');
        $items = $this->model->items->getListItem();

        foreach ($items as $item) {
            if ($item['menu'] == $id) {
                $this->deleteItem($item['id']);
            }
        }

        $this->load->model('Menu');
        $this->model->menu->deleteMenu($id);
    }

    public function updateMenu()
    {
        sleep(1);
        $this->load->model('Menu');
        $params = $this->request->post;

        if (isset($params['title'])) {
            $menuID = $this->model->menu->updateMenu($params);
        }
    }
}