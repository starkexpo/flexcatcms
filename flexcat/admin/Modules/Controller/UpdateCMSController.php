<?php

namespace FlexCat\Admin\Modules\Controller;


//use function Couchbase\fastlzDecompress;
use FlexCat\Template\Define;
use FlexCat\Functions\ArchivePackFile;

class UpdateCMSController extends AdminController
{
    public function update()
    {
//        echo Define::VERSION;

//        $serverVersion = '';

//        $serverVersion = $this->testUpdate();

        /*if (Define::VERSION != $serverVersion) {
            echo "<br> Доступно обновление: " . $serverVersion . "<br>";

            $this->curl_download('https://flexcat.ru/cmsUpdate/', 'flexi.zip');

            $fileZip = $_SERVER['DOCUMENT_ROOT'] . "/upload/update/";
            $files = "flexi.zip";

            $filePath = $fileZip . $files;

//            $this->archive();
//            $this->unArchive();

        }*/


        /*$folder = $_SERVER['DOCUMENT_ROOT'] . "/flexcat";
        $puthFile = $_SERVER['DOCUMENT_ROOT'] . "/upload/update/flexcat.zip";

        $zip = new ArchivePackFile();
        $zip->ArchiveZip($folder, $puthFile);*/






//        echo "<br>".getcwd();
//        chdir($_SERVER['DOCUMENT_ROOT']);
//        echo "<br>".getcwd();

//        rmdir( 'flexcat');



//        var_dump($serverVersion);




//        $updateLink = md5('cms_update');
//        $hostname = $_SERVER['HTTP_HOST'];
//
//        echo '<a href="/updater.php?set_updating='. $updateLink .'">Запустить обновление</a>';


        $this->data['link'] = '/updater.php?set_updating=' . md5('cms_update') . '&step=1';

        $this->view->render('updatecms/list', $this->data);
    }

    public function send()
    {
        $content =  file_get_contents("http://flexcat.ru/cmsUpdate/flexcat.xml");
        $ver = new \SimpleXMLElement($content);

        $xml = array();
        $description = array();

        foreach ($ver->description->item as $item) {
//            var_dump($item);
            $description[] = $item;
        }

        $xml = ["version" => $ver->version, "date" => $ver->date, "descriptions" => $description];



//        var_dump($xml);
        $xml = json_encode($xml);
//        echo "<h1>SEND</h1>";
//        echo "<hr>";
//        var_dump($xml);

        echo $xml;
    }

   /* public function Zip($source, $destination){
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new \ZipArchive();
        if (!$zip->open($destination, \ZipArchive::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true){
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file){
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);
                $file = str_replace('\\', '/', $file);

                if (is_dir($file) === true){
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }else if (is_file($file) === true){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }else if (is_file($source) === true){
            $zip->addFromString(basename($source), file_get_contents($source));
        }
        return $zip->close();
    }*/

    /*public function unArchive()
    {
        $file = 'flexcat.zip';
        $path = $_SERVER['DOCUMENT_ROOT'] . "/upload/";

        $zip = new \ZipArchive();
        $zip->open($path . $file);
        $zip->extractTo($path);
        $zip->close();


    }*/

    /*public function archive()
    {
        $foledArhive = $_SERVER['DOCUMENT_ROOT'] . "/flexcat/index.php";

        $phar = new \Phar($_SERVER['DOCUMENT_ROOT'] . '/upload/flexcat.phar', 0, 'flexcat.phar');
        $phar->startBuffering();
        $phar['homes.php'] = file_get_contents($_SERVER['DOCUMENT_ROOT'] . "/index.php");
//        $phar['flexcat'] = $_SERVER['DOCUMENT_ROOT'] . "/flexcat/";
//        $phar->compress(\Phar::GZ);
        $phar->convertToData(\Phar::ZIP);
        $phar->stopBuffering();

    }*/





    public function testUpdate()
    {
        $f = fopen('https://flexcat.ru/cmsUpdate/version.txt', "rt") or die("Ошибка");
//        echo "<br>Доступно обновление: " . fgets($f);
        $version = fgets($f);
        fclose($f);

        return $version;
    }

    private function curl_download($url, $file)
    {
        // открываем файл, на сервере, на запись
        $dest_file = @fopen($_SERVER['DOCUMENT_ROOT'] . "/upload/update/" . $file, "w");

        // открываем cURL-сессию
        $resource = curl_init();

        // устанавливаем опцию удаленного файла
        curl_setopt($resource, CURLOPT_URL, $url);

        // устанавливаем место на сервере, куда будет скопирован удаленной файл
        curl_setopt($resource, CURLOPT_FILE, $dest_file);

        // заголовки нам не нужны
        curl_setopt($resource, CURLOPT_HEADER, 0);

        // выполняем операцию
        curl_exec($resource);

        // закрываем cURL-сессию
        curl_close($resource);

        // закрываем файл
        fclose($dest_file);
    }
}