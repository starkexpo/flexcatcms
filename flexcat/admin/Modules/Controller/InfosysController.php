<?php

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Functions\UploadFile;

class InfosysController extends AdminController
{
    public function listing($id = '')
    {
        $this->load->model('Infosys');
        $this->data['infosys'] = $this->model->infosys->getList($id);
        $this->data['infosysList'] = $this->model->infosys->getListReleaseAll();


        $this->view->render('infosys/list', $this->data);
    }

    public function create()
    {
        $this->load->model('Infosys');
        $this->data['sections'] = $this->model->infosys->getListSection();

        $this->view->render('infosys/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Infosys');
        $this->data['info'] = $this->model->infosys->getInfo($id);

        $this->view->render('infosys/edit', $this->data);
    }

    public function addInfo()
    {
        sleep(1);
        $this->load->model('Infosys');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $infoID = $this->model->infosys->createInfo($params);

            echo $infoID;
        }
    }

    public function update()
    {
        sleep(1);
        $this->load->model('Infosys');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $infoID = $this->model->infosys->updateInfo($params);

            echo $infoID;
        }
    }

    public function viewInfo($id)
    {
        $this->load->model('Infosys');
        $this->data['viewinfo'] =  $this->model->infosys->getListRelease($id);
        $this->data['now_id'] =  $this->model->infosys->getInfo($id);

        $this->view->render('infosys/info_list', $this->data);
    }

    public function createRelease($id)
    {
        $this->load->model('Infosys');
        $this->data['now_id'] =  $this->model->infosys->getInfo($id);
        $this->data['sections'] = $this->model->infosys->getList(0);


        $this->view->render('infosys/createRelease', $this->data);
    }

    public function editRelease($id)
    {
        $this->load->model('Infosys');
        $this->data['now_id'] =  $this->model->infosys->getInfo($id);
        $this->data['sections'] = $this->model->infosys->getList(0);

        $this->view->render('infosys/editRelease', $this->data); //, $this->data
    }

    public function updateRelease()
    {
        sleep(1);
        $this->load->model('Infosys');
        $params = $this->request->post;

        if (isset($params['title']) && strlen($params['title']) > 1) {
            $releaseID = $this->model->infosys->updateRelease($params);

            echo $releaseID;
        }
    }


    public function imgload()
    {
        if (isset($_POST['my_file_upload'])) {
            $files = $_FILES[0]; // полученные файлы

            $upload = new UploadFile();
            $filename = $upload->uploadImage($files, "infosys");

            echo json_encode($filename);
        }
    }

    public function imageDelete($id)
    {
        $this->load->model('Infosys');
        $sites = $this->model->infosys->imgUpdate($id);

        header("Location:".$_SERVER['HTTP_REFERER']);
    }

    public function addRelease()
    {
        sleep(1);
        $this->load->model('Infosys');
        $params = $this->request->post;

        if (strlen($params['title']) > 0 && $params['section'] > 0) {
            $infoID = $this->model->infosys->createRelease($params);

            echo $infoID;
        }
    }

    public function setPublic($id)
    {
        $this->load->model('Infosys');
        $status = $this->model->infosys->statusInfo($id);
    }

    public function delete($id)
    {
        $this->load->model('Infosys');
        $this->model->infosys->deleteInfo($id);
    }
}