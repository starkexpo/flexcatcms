<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 01/01/2019
 * Time: 19:03
 */

namespace FlexCat\Admin\Modules\Controller;

use FlexCat\Functions\UploadFile;

class SitesController extends AdminController
{
    public function listing()
    {
        $this->load->model('Sites');

        $this->data['sites'] = $this->model->sites->getSites();

        $this->view->render('sites/list', $this->data);
    }

    public function create()
    {
        $this->load->model('Sites');

        $this->data['templates'] = $this->model->sites->getTemplates();

        $this->view->render('sites/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Sites');

        $this->data['pages'] = $this->model->sites->getPages($id);


        $this->data['templates'] = $this->model->sites->getTemplates();
        $this->data['sites'] = $this->model->sites->getSiteData($id);

        $this->view->render('sites/edit', $this->data);
    }

    public function add()
    {
        sleep(1);
        $this->load->model('Sites');
        $params = $this->request->post;

        $testEmail = \FlexCat\Helper\TestEmail::verificationEmail($params['email']);

        if ($testEmail === false) {

            return false;
        }


        if (!isset($params['active']) or $params['active'] <> 'enable') {
            $params['active'] = 'disabled';
        }

        if (isset($params['title']) && isset($params['email'])
            && strlen($params['title']) > 1) {
            $pageID = $this->model->sites->createSites($params);

            echo $pageID;
        }
    }

    public function update()
    {
        sleep(1);
        $this->load->model('Sites');
        $params = $this->request->post;

        $testEmail = \FlexCat\Helper\TestEmail::verificationEmail($params['email']);

        if ($testEmail === false) {

            return false;
        }

        if (!isset($params['active']) or $params['active'] <> 'enable') {
            $params['active'] = 'disabled';
        }

        if (isset($params['title']) && isset($params['email'])
            && strlen($params['title']) > 1) {
            $pageID = $this->model->sites->updateSites($params);

            echo $pageID;
        }
    }

    public function defaults($id)
    {
        $this->load->model('Sites');
        $sites = $this->model->sites->defaultSite($id);

        @file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/robots.txt", $sites[0]['robots']);

        header("Location:".$_SERVER['HTTP_REFERER']); //временно!
    }

    public function getActiveSite()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites')
            ->where("defaults", 'enable')
            ->limit(1)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function favicon()
    {
        if (isset($_POST['my_file_upload'])) {
            $files = $_FILES[0]; // полученные файлы

            $upload = new UploadFile();
            $filename = $upload->uploadImage($files, "sites");

            echo json_encode($filename);
        }
    }

    public function faviconDelete($id)
    {
        $this->load->model('Sites');

        $sites = $this->model->sites->faviconUpdate($id);

        header("Location:".$_SERVER['HTTP_REFERER']);
    }
}