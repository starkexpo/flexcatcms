<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 06/01/2019
 * Time: 20:02
 */

namespace FlexCat\Admin\Modules\Model\Layouts;

use FlexCat\Database\ActiveRecord;

class Layouts
{
    use ActiveRecord;

    protected $table = 'layouts';

    public $id;

    public $type;

    public $title;

    public $parent;

    public $parent_layouts;

    public $site;

    public $layouts;

    public $layout;

    public $css;

    public $javascript;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return mixed
     */
    public function getLayouts()
    {
        return $this->layouts;
    }

    /**
     * @param mixed $layouts
     */
    public function setLayouts($layouts)
    {
        $this->layouts = $layouts;
    }

    /**
     * @return mixed
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return mixed
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * @param mixed $css
     */
    public function setCss($css)
    {
        $this->css = $css;
    }

    /**
     * @return mixed
     */
    public function getJavascript()
    {
        return $this->javascript;
    }

    /**
     * @param mixed $javascript
     */
    public function setJavascript($javascript)
    {
        $this->javascript = $javascript;
    }

    /**
     * @return mixed
     */
    public function getParentLayouts()
    {
        return $this->parent_layouts;
    }

    /**
     * @param mixed $parent_layouts
     */
    public function setParentLayouts($parent_layouts)
    {
        $this->parent_layouts = $parent_layouts;
    }
}