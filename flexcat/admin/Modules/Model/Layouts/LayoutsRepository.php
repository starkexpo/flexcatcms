<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 06/01/2019
 * Time: 21:59
 */

namespace FlexCat\Admin\Modules\Model\Layouts;

use FlexCat\Model;

class LayoutsRepository extends Model
{
    public function getListSection()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('layouts')
            ->where("type", "section")
            ->orderBy('parent', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListLayouts($site_id = '')
    {
        if (isset($site_id) && $site_id > 0) {
            $sql = $this->queryBuilder
                ->select()
                ->from('layouts')
                ->where("type", "layouts")
                ->where("site", $site_id)
                ->orderBy('parent', 'ASC')
                ->sql();
        } else {
            $sql = $this->queryBuilder
                ->select()
                ->from('layouts')
                ->where("type", "layouts")
                ->orderBy('parent', 'ASC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListSections()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sections')
//            ->where("layouts_id", $id)
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getView($id)
    {
        if (isset($id) && $id > 0) {
            $sql = $this->queryBuilder
                ->select()
                ->from('layouts')
                ->where("id", $id)
                ->orderBy('type', 'ASC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public function getTree($id)
    {

    }

    public function getListIndex($id = '')
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('layouts')
                ->where("id", $id)
                ->sql();

            $nowItem =  $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

//            var_dump($nowItem); //layouts

            if ($nowItem[0]['type'] == "section") {

                $sql = $this->queryBuilder
                    ->select()
                    ->from('layouts')
                    ->where("parent", $id)
                    ->orderBy('type', 'ASC')
                    ->sql();
            } else {
                $sql = $this->queryBuilder
                    ->select()
                    ->from('layouts')
                    ->where("parent_layouts", $id)
                    ->orderBy('type', 'ASC')
                    ->sql();
            }

        } else {
            $sql = $this->queryBuilder
                ->select()
                ->from('layouts')
                ->where("parent", '0')
                ->where("parent_layouts", '0')
                ->orderBy('type', 'ASC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    /**
     * @return mixed
     */
    public function getActiveSite()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites')
            ->where("defaults", 'enable')
            ->limit(1)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getSectionData($id)
    {
        $section = new Layouts($id);

        return $section->findOne();
    }

    public function getLayoutsData($id)
    {
        $section = new Layouts($id);

        return $section->findOne();
    }

    public function createSection($params)
    {
        $section = new Layouts();
        $section->setTitle($params['title']);
        $section->setSite($params['site']);
        $section->setType("section");

        if ($params['parent'] > 0) {
            $section->setParent($params['parent']);
        }

        $sectionID = $section->save();

        return $sectionID;
    }


    public function createLayouts($params)
    {
//        var_dump($params);

        $layouts = new Layouts();
        $layouts->setTitle($params['title']);
        $layouts->setLayout($params['layout']);
        $layouts->setType("layouts");

        if (isset($params['parent']) && $params['parent'] > 0) {
            $layouts->setParent(intval($params['parent']));
        }

        if ($params['layouts'] > 0) {
            $layouts->setParentLayouts(intval($params['layouts']));
        }

        $site = $this->getActiveSite();

        $sql = $this->queryBuilder
            ->select()
            ->from('sites_templates')
            ->where("id", intval($site[0]['template']))
            ->limit(1)
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        $folder = "default";

        $site = $this->getActiveSite();
        $layouts->setSite($site[0]['id']);
//
//        if ($params['parent'] > 0) {
//            $section->setParent($params['parent']);
//        }
//
        $layoutsID = $layouts->save();


        if (!empty($templates)) {
            $folder = $templates[0]['folder'];
        }

        if ($params['layouts'] == 0) {
            $fileName = "index.php";
        } else {
            $fileName = "layouts" . $layoutsID . ".php";
        }

        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $folder . "/" . $fileName;


        //нужно указать путь для какого сайта создается шаблон
        @file_put_contents($path, $params['layout']);

//        return $layoutsID;
        header("Location:" . $_SERVER['HTTP_REFERER']);
    }


    public function editSection($params)
    {
        $section = new Layouts();
        $section->setTitle($params['title']);
        $section->setSite($params['site']);
        $section->setId($params['sections_id']);

        if ($params['parent'] > 0) {
            $section->setParent($params['parent']);
        }

        $section->save();
    }

    public function editLayouts($params)
    {
        //var_dump($params);

        $layouts = new Layouts($params['layouts_id']);
        $layouts->setTitle($params['title']);
        $layouts->setLayout($params['layout']);
        $layouts->setType("layouts");
        $layouts->setId($params['layouts_id']);

        if (isset($params['parent']) && $params['parent'] > 0) {
            $layouts->setParent(intval($params['parent']));
        }

        if ($params['layouts'] > 0) {
            $layouts->setParentLayouts(intval($params['layouts']));
        }

        $site = $this->getActiveSite();

        $sql = $this->queryBuilder
            ->select()
            ->from('sites_templates')
            ->where("id", intval($site[0]['template']))
            ->limit(1)
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        $folder = "default";

        $site = $this->getActiveSite();
        $layouts->setSite($site[0]['id']);
        $layoutsID = $layouts->save();

        if (!empty($templates)) {
            $folder = $templates[0]['folder'];
        }

        if ($params['layouts'] == 0) {
            $fileName = "index.php";
        } else {
            $fileName = "layouts" . $params['layouts_id'] . ".php";
        }

        //var_dump( $layoutsID);

        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $folder . "/" . $fileName;


        @file_put_contents($path, $params['layout']);

        $layoutsID = $layouts->save();

        header("Location:" . $_SERVER['HTTP_REFERER']);
    }

    public function getSiteTemplate($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites_templates')
            ->where("id", $id)
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        return $templates;
    }

    public function deleteLayouts($id)
    {
        $sql = $this->queryBuilder
            ->delete('layouts')
            ->where("id", $id)
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

}