<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 01/01/2019
 * Time: 19:10
 */

namespace FlexCat\Admin\Modules\Model\Sites;

use FlexCat\Database\ActiveRecord;

class Sites
{
    use ActiveRecord;

    protected $table = 'sites';

    public $id;

    public $title;

    public $email;

    public $lang;

    public $encoding;

    public $favicon;

    public $timezone;

    public $active;

    public $template;

    public $img;

    public $page404;

    public $page403;

    public $page_off;

    public $robots;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param mixed $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return mixed
     */
    public function getEncoding()
    {
        return $this->encoding;
    }

    /**
     * @param mixed $encoding
     */
    public function setEncoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * @return mixed
     */
    public function getFavicon()
    {
        return $this->favicon;
    }

    /**
     * @param mixed $favicon
     */
    public function setFavicon($favicon)
    {
        $this->favicon = $favicon;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }


    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @param mixed $img
     */
    public function setImg($img)
    {
        $this->img = $img;
    }

    /**
     * @return mixed
     */
    public function getPage404()
    {
        return $this->page404;
    }

    /**
     * @param mixed $page404
     */
    public function setPage404($page404)
    {
        $this->page404 = $page404;
    }

    /**
     * @return mixed
     */
    public function getPage403()
    {
        return $this->page403;
    }

    /**
     * @param mixed $page403
     */
    public function setPage403($page403)
    {
        $this->page403 = $page403;
    }

    /**
     * @return mixed
     */
    public function getPageOff()
    {
        return $this->page_off;
    }

    /**
     * @param mixed $page_off
     */
    public function setPageOff($page_off)
    {
        $this->page_off = $page_off;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getRobots()
    {
        return $this->robots;
    }

    /**
     * @param mixed $robots
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;
    }

}