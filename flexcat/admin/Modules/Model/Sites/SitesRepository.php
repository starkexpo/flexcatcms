<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 01/01/2019
 * Time: 19:22
 */

namespace FlexCat\Admin\Modules\Model\Sites;

use FlexCat\Model;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class SitesRepository extends Model
{
    public function getSites()
    {
        $sql = $this->queryBuilder->select()
            ->from('sites')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }

    public function getPages($id)
    {
        $pages = array();
        $sql = $this->queryBuilder
            ->select()
            ->from('menu')
            ->where('site_id', $id)
            ->orderBy('title', 'ASC')
            ->sql();

        $menuList = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        $sqlItem = $this->queryBuilder
            ->select()
            ->from('menu_item')
            ->sql();

        $itemList = $this->db->query($sqlItem, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        foreach ($itemList as $item) {
            foreach ($menuList as $menu) {
                if ($item['menu'] == $menu['id']) {
                    $idItem = $item['id'];
                    $titleItem = $item['title'];

                    $pages[] = ["id"=>$idItem, "title"=>$titleItem];
                }
            }
        }

        return $pages;
    }



    public function getTemplates()
    {
        $sql = $this->queryBuilder->select()
            ->from('sites_templates')
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql);
    }

    public function createSites($params)
    {
        $sites = new Sites();

        $sites->setTitle($params['title']);
        $sites->setActive($params['active']);
        $sites->setEncoding($params['encoding']);
        $sites->setLang($params['locale']);
        $sites->setTimezone($params['timezone']);
        $sites->setEmail($params['email']);

        if (isset($params['favicon']) && strlen($params['favicon']) > 2) {
            $sites->setFavicon($params['favicon']);
        }

        if (isset($params['robots']) && strlen($params['robots']) > 1) {
            $sites->setRobots($params['robots']);
        }

        if (isset($params['template']) && intval($params['template']) > 0) {
            $sites->setTemplate($params['template']);

            $sql = $this->queryBuilder
                ->select()
                ->from('sites_templates')
                ->where('id', $params['template'])
                ->limit(1)
                ->sql();

            $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
            $sites->setImg($templates[0]['img']);
        } else {
            $sites->setImg("flexcats.png");
        }

        if ($params['page404'] > 0 && isset($params['page404'])) {
            $sites->setPage404($params['page404']);
        }

        if ($params['page403'] > 0 && isset($params['page403'])) {
            $sites->setPage403($params['page403']);
        }

        if ($params['page_off'] > 0 && isset($params['page_off'])) {
            $sites->setPageOff($params['page_off']);
        }
        $siteID = $sites->save();

        return $siteID;
    }

    public function updateSites($params)
    {
        if (isset($params['site_id'])) {

            $sites = new Sites($params['site_id']);

            $sites->setTitle($params['title']);
            $sites->setActive($params['active']);
            $sites->setEncoding($params['encoding']);
            $sites->setLang($params['locale']);
            $sites->setTimezone($params['timezone']);
            $sites->setEmail($params['email']);

            if (isset($params['favicon']) && strlen($params['favicon']) > 2) {
                $sites->setFavicon($params['favicon']);
            }

            if (isset($params['robots']) && strlen($params['robots']) > 1) {
                $sites->setRobots($params['robots']);
            }

            if (isset($params['template']) && intval($params['template']) > 0) {
                $sites->setTemplate($params['template']);

                $sql = $this->queryBuilder
                    ->select()
                    ->from('sites_templates')
                    ->where('id', $params['template'])
                    ->limit(1)
                    ->sql();

                $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
                $sites->setImg($templates[0]['img']);
            } else {
                $sites->setImg("flexcats.png");
            }

            if ($params['page404'] > 0) {
                $sites->setPage404($params['page404']);
            }

            if ($params['page403'] > 0) {
                $sites->setPage403($params['page403']);
            }

            if ($params['page_off'] > 0) {
                $sites->setPageOff($params['page_off']);
            }

            $siteID = $sites->save();
        }
    }

    public function getSiteData($id)
    {
        $sites = new Sites($id);

        return $sites->findOne();
    }


    public function defaultSite($id)
    {
        if (isset($id) && $id > 0) {
            $data['defaults'] = "disabled";

            $sql = $this->queryBuilder
                ->update('sites')
                ->set($data)
                ->sql();

            $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

            $data['defaults'] = "enable";
            $sql = $this->queryBuilder
                ->update('sites')
                ->set($data)
                ->where("id", $id)
                ->sql();

            $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

            $sql = $this->queryBuilder
                ->select()
                ->from('sites')
                ->where("id", $id)
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public static function getSiteNow($id)
    {
        $sites = new Sites($id);

        return $sites->findOne();
    }

    public function faviconUpdate($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites')
            ->where("id", $id)
            ->sql();

        $sites = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        @unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/img/sites/" . $sites[0]["favicon"]);

        $data['favicon'] = '';
        $sql = $this->queryBuilder
            ->update('sites')
            ->set($data)
            ->where("id", $id)
            ->sql();

        $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }


    public static function getNowActive()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder
            ->select()
            ->from('sites')
            ->where("defaults", 'enable')
            ->limit(1)
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

}