<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 12/01/2019
 * Time: 21:29
 */

namespace FlexCat\Admin\Modules\Model\Items;

use FlexCat\Database\ActiveRecord;

class Items
{
    use ActiveRecord;

    protected $table = 'menu_item';

    public $id;

    public $title;

    public $path;

    public $typeItem;

    public $menu;

    public $site;

    public $parent;

    public $sorting;

    public $layouts;

    public $published;

    public $show_menu;

    public $page_id;

    public $dynamic_page;

    public $link;

    public $indexing;

    public $meta_title;

    public $keywords;

    public $description;

    public $options;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param mixed $menu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * @param mixed $meta_title
     */
    public function setMetaTitle($meta_title)
    {
        $this->meta_title = $meta_title;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param mixed $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }

    /**
     * @return mixed
     */
    public function getLayouts()
    {
        return $this->layouts;
    }

    /**
     * @param mixed $layouts
     */
    public function setLayouts($layouts)
    {
        $this->layouts = $layouts;
    }

    /**
     * @return mixed
     */
    public function getShowMenu()
    {
        return $this->show_menu;
    }

    /**
     * @param mixed $show_menu
     */
    public function setShowMenu($show_menu)
    {
        $this->show_menu = $show_menu;
    }

    /**
     * @return mixed
     */
    public function getPageId()
    {
        return $this->page_id;
    }

    /**
     * @param mixed $page_id
     */
    public function setPageId($page_id)
    {
        $this->page_id = $page_id;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getTypeItem()
    {
        return $this->typeItem;
    }

    /**
     * @param mixed $typeItem
     */
    public function setTypeItem($typeItem)
    {
        $this->typeItem = $typeItem;
    }

    /**
     * @return mixed
     */
    public function getDynamicPage()
    {
        return $this->dynamic_page;
    }

    /**
     * @param mixed $dynamic_page
     */
    public function setDynamicPage($dynamic_page)
    {
        $this->dynamic_page = $dynamic_page;
    }

    /**
     * @return mixed
     */
    public function getIndexing()
    {
        return $this->indexing;
    }

    /**
     * @param mixed $indexing
     */
    public function setIndexing($indexing)
    {
        $this->indexing = $indexing;
    }

    /**
     * @return mixed
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param mixed $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
}