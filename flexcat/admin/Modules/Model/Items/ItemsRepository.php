<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 12/01/2019
 * Time: 21:30
 */

namespace FlexCat\Admin\Modules\Model\Items;

use FlexCat\Admin\Modules\Model\Parameters\ParametersRepository;
use FlexCat\Model;

class ItemsRepository extends Model
{
    public function getListItem($id = '')
    {
        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getNowActive();
        $idSite = intval($nowSite[0]['id']);


        if (isset($id) && $id > 0) {
            $sql = $this->queryBuilder
                ->select()
                ->from('menu_item')
                ->where('parent', $id)
                ->where('site',  $idSite)
                ->orderBy('id', 'DESC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        } else {
            $sql = $this->queryBuilder
                ->select()
                ->from('menu_item')
                ->where('parent', '0')
                ->where('site',  $idSite)
                ->orderBy('id', 'DESC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public function createItems($params)
    {
        $item = new Items();
        $item->setTitle($params['title']);
        if ($params['parent'] > 0) {
            $item->setParent($params['parent']);
        }
        if ($params['sorting'] > 0) {
            $item->setSorting($params['sorting']);
        }
        if (strlen($params['path']) > 0 && $params['path'] != '' &&  $params['path'] != '/') {
            $item->setPath('/' . $params['path']);
        } else {
            $item->setPath('/');
        }

        $item->setLayouts($params['layouts']);
        $item->setMenu($params['menu']);
        if (isset($params['published']) && $params['published'] == "enable") {
            $item->setPublished($params['published']);
        } else {
            $item->setPublished('disabled');
        }

        if (isset($params['show_menu']) && $params['show_menu'] == "enable") {
            $item->setShowMenu($params['show_menu']);
        } else {
            $item->setShowMenu('disabled');
        }

        if (isset($params['indexing']) && $params['indexing'] == "enable") {
            $item->setIndexing($params['indexing']);
        } else {
            $item->setIndexing('disabled');
        }

        if (!empty($params['options'])) {

            foreach ($params['options'] as $param) {

                $item_id = $param['id'];
                $numer = $param['id'];
                $vals = $params['options' . $numer];
                $variable = $param['variable'];
                $field_type = $param['field_type'];

                $options[] = ["id" => $item_id, "value" => $vals, "variable" => $variable, "field_type" => $field_type];
            }

            $item->options = base64_encode(serialize($options));
        }

        $item->setShowMenu($params['show_menu']);


        switch ($params['typeItem']) {
            case "0":
                $item->setPageId($params['page']);
                $item->setTypeItem('page');

                $textPage['content'] =  $params['contentPage'];


                $sql = $this->queryBuilder
                    ->update('pages')
                    ->set($textPage)
                    ->where('id', $params['page'])
                    ->sql();

                $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
                break;

            case "1":
                $item->setDynamicPage($params['dynamic_template']);
                $item->setTypeItem('dynamic');
                break;

            case "2":
                $item->setLink($params['link']);
                $item->setTypeItem('link');
                break;

        }


        if (isset($params['indexing']) && $params['indexing'] == "enable") {
            $item->setIndexing($params['indexing']);
        } else {
            $item->setIndexing('disabled');
        }

        if (strlen($params['meta_title']) > 0 && $params['meta_title'] != '') {
            $item->setMetaTitle(trim($params['meta_title']));
        }

        if (strlen($params['description']) > 0 && $params['description'] != '') {
            $item->setDescription(trim($params['description']));
        }

        if (strlen($params['keywords']) > 0 && $params['keywords'] != '') {
            $item->setKeywords(trim($params['keywords']));
        }

        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getNowActive();
        $idSite = intval($nowSite[0]['id']);

        $item->setSite($idSite);

        $itemID = $item->save();

        return $itemID;
    }

    /**
     * @param $params
     */
    public function editItems($params)
    {
//        var_dump($params);
        $item = new Items($params['item_id']);
        $item->setTitle($params['title']);
        if ($params['parent'] > 0) {
            $item->setParent($params['parent']);
        }
        if ($params['sorting'] > 0) {
            $item->setSorting($params['sorting']);
        }
        if (strlen($params['path']) > 0 && $params['path'] != '' &&  $params['path'] != '/') {
            $item->setPath('/' . $params['path']);
        } else {
            $item->setPath('/');
        }

        $item->setLayouts($params['layouts']);
        $item->setMenu($params['menu']);
        if (isset($params['published']) && $params['published'] == "enable") {
            $item->setPublished($params['published']);
        } else {
            $item->setPublished('disabled');
        }

        if (isset($params['show_menu']) && $params['show_menu'] == "enable") {
            $item->setShowMenu($params['show_menu']);
        } else {
            $item->setShowMenu('disabled');
        }

        if (isset($params['indexing']) && $params['indexing'] == "enable") {
            $item->setIndexing($params['indexing']);
        } else {
            $item->setIndexing('disabled');
        }

        if (!empty($params['options'])) {

            foreach ($params['options'] as $param) {

                $item_id = $param['id'];
                $numer = $param['id'];
                $vals = $params['options' . $numer];
                $variable = $param['variable'];
                $field_type = $param['field_type'];

                $options[] = ["id" => $item_id, "value" => $vals, "variable" => $variable, "field_type" => $field_type];
            }

            $item->options = base64_encode(serialize($options));
        }

        switch ($params['typeItem']) {
            case "0":
                $item->setPageId($params['page']);
                $item->setTypeItem('page');

                $textPage['content'] =  $params['contentPage'];


                $sql = $this->queryBuilder
                    ->update('pages')
                    ->set($textPage)
                    ->where('id', $params['page'])
                    ->sql();

                $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

                $textPageOne['link'] = null;
                $sqlOne = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPageOne)
                    ->where('id', $params['item_id'])
                    ->sql();

                $this->db->query($sqlOne, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

                $textPageTwo['dynamic_page'] = '0';
                $sqlTwo = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPageTwo)
                    ->where('id', $params['item_id'])
                    ->sql();
                $this->db->execute($sqlTwo, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
                break;

            case "1":
                $item->setDynamicPage($params['dynamic_template']);
                $item->setTypeItem('dynamic');

                $textPage['page_id'] = '0';
                $sql = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPage)
                    ->where('id', $params['item_id'])
                    ->sql();
                $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

                $textPageTwo['link'] = null;
                $sqlTwo = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPageTwo)
                    ->where('id', $params['item_id'])
                    ->sql();
                $this->db->query($sqlTwo, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
                break;

            case "2":
                $item->setLink($params['link']);
                $item->setTypeItem('link');

                $textPage['page_id'] = '0';
                $sql = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPage)
                    ->where('id', $params['item_id'])
                    ->sql();
                $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

                $textPageTwo['dynamic_page'] = '0';
                $sqlTwo = $this->queryBuilder
                    ->update('menu_item')
                    ->set($textPageTwo)
                    ->where('id', $params['item_id'])
                    ->sql();
                $this->db->execute($sqlTwo, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
                break;

        }


        if (isset($params['indexing']) && $params['indexing'] == "enable") {
            $item->setIndexing($params['indexing']);
        } else {
            $item->setIndexing('disabled');
        }

        if (strlen($params['meta_title']) > 0 && $params['meta_title'] != '') {
            $item->setMetaTitle($params['meta_title']);
        }

        if (strlen($params['description']) > 0 && $params['description'] != '') {
            $item->setDescription($params['description']);
        }

        if (strlen($params['keywords']) > 0) {
            $item->setKeywords($params['keywords']);
        }


        $itemID = $item->save();

        return $params['item_id'];
    }

    public function statusItems($id)
    {
//        echo $id;
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('menu_item')
                ->where('id', $id)
                ->sql();

            $itemBase = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

//            var_dump($itemBase);

//        echo $itemBase[0]['published'];

            $item = new Items($id);
            if ($itemBase[0]['published'] == "enable") {
                $status = 'disabled';
            } else {
                $status = 'enable';
            }
            $item->setPublished($status);
            $item->save();

            return $status;
        }
    }

    public function getItemsData($id)
    {
        $items = new Items($id);

        return $items->findOne();
    }

    public function deleteItem($id)
    {
        if (isset($id) && $id > 0) {


            $sql = $this->queryBuilder
                ->delete('menu_item')
                ->where('id', $id)
                ->sql();
            $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);


            $sqlRoute = $this->queryBuilder
                ->delete('routing')
                ->where('menu_item', $id)
                ->sql();

            $this->db->execute($sqlRoute, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }
}