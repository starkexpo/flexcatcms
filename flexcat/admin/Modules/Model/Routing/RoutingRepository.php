<?php

namespace FlexCat\Admin\Modules\Model\Routing;

use FlexCat\Model;

class RoutingRepository extends Model
{
    public function createRoute($params)
    {
//        echo "<hr>";
//        var_dump($params);

        $route = new Routing();

        if ($params['path'] == '' or $params['path'] == '/') {
            $route->setModule('home');
            $route->setUrl('/');
        } else {
            $route->setModule($params['path']);

            $url = "/" . $params['path'];
            $route->setUrl($url);

            /*if ($params['typeItem'] == 0) {
                $url = "/" . $params['path'] ;
                $route->setUrl($url);
            }*/
        }


        $route->setController('HomeController');
        $route->setMethod('index');
        $route->setRequest('GET');
        $route->setEnv('site');

        if (isset($params['published']) && $params['published'] == "enable") {
            $route->setPublished($params['published']);
        } else {
            $route->setPublished('disabled');
        }

        $route->setMenuItem($params['last_items']);

        $route->save();

    }

    public function editRoute($params)
    {
//        $parametr['url'] = "/" . $params['path'];
//        $parametr['module'] = $params['path'];

        if ($params['path'] == '' or $params['path'] == '/') {
            $parametr['url'] = '/';
            $parametr['module'] = 'home';
        } else {
            $parametr['url'] = "/" . $params['path'];
            $parametr['module'] = $params['path'];
        }



        $sql = $this->queryBuilder
            ->update('routing')
            ->set($parametr)
            ->where('menu_item', $params['item_id'])
            ->where('env', 'site')
            ->sql();
        $itemBase = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function statusRoute($id, $status)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('routing')
            ->where('menu_item', $id)
            ->sql();

        $itemBase = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);


        $route = new Routing($itemBase[0]['id']);

        $route->setPublished($status);
        $route->save();


    }


}