<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 14/01/2019
 * Time: 05:02
 */

namespace FlexCat\Admin\Modules\Model\Parameters;

use FlexCat\Model;

class ParametersRepository extends Model
{
    public function getDynamic($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_page')
            ->where("id", $id)
            ->orderBy('type', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getList($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_option')
            ->where("dynamic_id", $id)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getViewOption($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_option')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createOption($params)
    {
//        var_dump($params);
        $option = new Parameters();
        $option->setTitle($params['title']);

        if (strlen($params['description']) > 1) {
            $option->setDescription($params['description']);
        }

        $option->setVariable($params['variable']);

        if (strlen($params['defaults']) > 1) {
            $option->setDefaults($params['defaults']);
        }

        $option->setFieldType($params['type']);

        if ($params['type'] == "sql" && strlen($params['field_sql']) > 1) {
            $option->setFieldSql($params['field_sql']);
            $option->setFieldTitle($params['field_title']);
            $option->setFieldValue($params['field_value']);
        }

        $option->setDynamicId($params['dynamic_id']);
        $optionID = $option->save();

        return $optionID;
    }

    public function editOption($params)
    {
        $option = new Parameters($params['option_id']);
        $option->setTitle($params['title']);

        if (strlen($params['description']) > 1) {
            $option->setDescription($params['description']);
        }

        $option->setVariable($params['variable']);

        if (strlen($params['defaults']) > 1) {
            $option->setDefaults($params['defaults']);
        }

        $option->setFieldType($params['type']);

        if ($params['type'] == "sql" && strlen($params['field_sql']) > 1) {
            $option->setFieldSql($params['field_sql']);
            $option->setFieldTitle($params['field_title']);
            $option->setFieldValue($params['field_value']);
        }

        $option->setDynamicId($params['dynamic_id']);
        $optionID = $option->save();

        return $optionID;
    }

    public function editValue($params)
    {
        $listOptions = $this->getList($params['dynamic_template']);

        foreach ($listOptions as $option) {
            $options = 'options' . $option['id'];

            $option = new Parameters($option['id']);
            $option->setValue($params[$options]);
            $optionID = $option->save();
        }
    }

    public function deleteOption($id)
    {
        $sql = $this->queryBuilder
            ->delete('dynamic_option')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function resets($id)
    {
        $option['value'] = '#';

        $sql = $this->queryBuilder
            ->update('dynamic_option')
            ->set($option)
            ->where("dynamic_id", $id)
            ->sql();

        $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}