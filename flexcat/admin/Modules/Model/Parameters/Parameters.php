<?php

namespace FlexCat\Admin\Modules\Model\Parameters;

use FlexCat\Database\ActiveRecord;

class Parameters
{
    use ActiveRecord;

    protected $table = 'dynamic_option';

    public $id;

    public $title;

    public $dynamic_id;

    public $variable;

    public $defaults;

    public $description;

    public $field_type;

    public $field_sql;

    public $field_title;

    public $field_value;

    public $value;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDynamicId()
    {
        return $this->dynamic_id;
    }

    /**
     * @param mixed $dynamic_id
     */
    public function setDynamicId($dynamic_id)
    {
        $this->dynamic_id = $dynamic_id;
    }

    /**
     * @return mixed
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * @param mixed $variable
     */
    public function setVariable($variable)
    {
        $this->variable = $variable;
    }

    /**
     * @return mixed
     */
    public function getDefaults()
    {
        return $this->defaults;
    }

    /**
     * @param mixed $defaults
     */
    public function setDefaults($defaults)
    {
        $this->defaults = $defaults;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getFieldType()
    {
        return $this->field_type;
    }

    /**
     * @param mixed $field_type
     */
    public function setFieldType($field_type)
    {
        $this->field_type = $field_type;
    }

    /**
     * @return mixed
     */
    public function getFieldSql()
    {
        return $this->field_sql;
    }

    /**
     * @param mixed $field_sql
     */
    public function setFieldSql($field_sql)
    {
        $this->field_sql = $field_sql;
    }

    /**
     * @return mixed
     */
    public function getFieldTitle()
    {
        return $this->field_title;
    }

    /**
     * @param mixed $field_title
     */
    public function setFieldTitle($field_title)
    {
        $this->field_title = $field_title;
    }

    /**
     * @return mixed
     */
    public function getFieldValue()
    {
        return $this->field_value;
    }

    /**
     * @param mixed $field_value
     */
    public function setFieldValue($field_value)
    {
        $this->field_value = $field_value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}