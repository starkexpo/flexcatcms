<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 07/12/2018
 * Time: 15:57
 */

namespace FlexCat\Admin\Modules\Model\Page;

use FlexCat\Model;

class PageRepository extends Model
{
    public function getPages($id)
    {
        if (isset($id) && $id > 0) {
            $sql = $this->queryBuilder->select()
                ->from('pages')
                ->where('section', $id)
                ->orderBy('id', 'DESC')
                ->sql();
        } else {
            $sql = $this->queryBuilder->select()
                ->from('pages')
                ->where('section', '0')
                ->orderBy('id', 'DESC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }


    public function getListPages()
    {
        $sql = $this->queryBuilder->select()
            ->from('pages')
            ->where('type', 'page')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListSections()
    {
        $sql = $this->queryBuilder->select()
            ->from('pages')
            ->where('type', 'section')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createPage($params)
    {
        $page = new Page();
        $page->setTitle($params['title']);
        $page->setContent($params['content']);
        $page->setAlias($params['alias']);
        $page->setPublished($params['published']);
        $page->setSection($params['section']);
        $page->setDate($params['date']);
        $page->setType('page');

        $pageID = $page->save();

        return $pageID;
    }

    public function getPageData($id)
    {
        $page = new Page($id);

        return $page->findOne();
    }

    public function updatePage($params)
    {
        if (isset($params['page_id'])) {

            $page = new Page($params['page_id']);
            $page->setTitle($params['title']);
            $page->setContent($params['content']);
            $page->setAlias($params['alias']);
            $page->setPublished($params['published']);
            $page->setSection($params['section']);
            $page->setDate($params['date']);
            $page->setType('page');

            $pageID = $page->save();
        }
    }

    public function createSections($params)
    {
        $section = new Page();
        $section->setTitle($params['title']);

        if (isset($params['section']) && $params['section'] > 0) {
            $section->setSection($params['section']);
        }
        $section->setType('section');



        $sectionID = $section->save();

        return $sectionID;
    }

    public function updateSection($params)
    {
        $section = new Page($params['section_id']);
        $section->setTitle($params['title']);

        if (isset($params['section']) && $params['section'] > 0) {
            $section->setSection($params['section']);
        }

        $sectionID = $section->save();

        return $sectionID;
    }

    public function deletePage($id)
    {
        $sql = $this->queryBuilder
            ->delete('pages')
            ->where('id', $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}