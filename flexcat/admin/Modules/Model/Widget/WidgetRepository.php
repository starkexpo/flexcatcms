<?php

namespace FlexCat\Admin\Modules\Model\Widget;

use FlexCat\Model;
use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class WidgetRepository extends Model
{
    public function getViewSection($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sections')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListWidgets($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('widget')
            ->where("sections_id", $id)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function getListBlockWidgets($id = '')
    {
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->select()
            ->from('widget')
            ->where("sections_id", $id)
            ->where("published", "enable")
            ->orderBy('sorting', 'ASC')
            ->sql();

        $db = new Connection();

        return json_encode($db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC));
    }

    public static function getSQL($baseSQL)
    {
        $sql = '';
        $queryBuilder = new QueryBuilder();
        eval($baseSQL);
        $db = new Connection();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function getListOption($id)
    {
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->select()
            ->from('dynamic_option')
            ->where("dynamic_id", $id)
            ->orderBy('id', 'DESC')
            ->sql();

        $db = new Connection();

        return json_encode($db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC));
    }

    public function getViewWidget($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('widget')
            ->where("id", $id)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListTemplate()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_page')
            ->where("type", "template")
            ->orderBy('parent', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListTemplateDynamic()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_page')
            ->where("type", "template")
            ->orderBy('parent', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }


    public function createWidget($params)
    {
        $widget = new Widget();
        if (strlen($params['classes'])) {
            $widget->setClasses($params['classes']);
        }
        if (strlen($params['styles'])) {
            $widget->setStyles($params['styles']);
        }
        if (strlen($params['description']) > 1) {
            $widget->setDescription($params['description']);
        }
        $widget->setSectionsId($params['sections_id']);
        $widget->setDynamicSections($params['dynamic_sections']);
        $widget->setDynamicTemplate($params['dynamic_template']);
        if ($params['sorting'] > 0) {
            $widget->setSorting($params['sorting']);
        }
        $widget->setPublished($params['published']);
        $widgetID = $widget->save();

        return $widgetID;
    }

    public function editWidget($params)
    {
//        var_dump($params);
        /*
         * classes
         * style
         * description
         * dynamic_sections
         * dynamic_template
         * sorting
         * published
         * widget_id
         *
         * */

        $widget = new Widget($params['widget_id']);
        if (strlen($params['classes'])) {
            $widget->setClasses($params['classes']);
        }
        if (strlen($params['styles'])) {
            $widget->setStyles($params['styles']);
        }
        if (strlen($params['description']) > 1) {
            $widget->setDescription($params['description']);
        }
        $widget->setSectionsId($params['sections_id']);
        $widget->setDynamicSections($params['dynamic_sections']);
        $widget->setDynamicTemplate($params['dynamic_template']);
        if ($params['sorting'] > 0) {
            $widget->setSorting($params['sorting']);
        }
        if (isset($params['published']) && strlen($params['published']) > 1) {
            $widget->setPublished('enable');
        } else {
            $widget->setPublished('disabled');
        }
        $widgetID = $widget->save();
    }

    public function statusWidget($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('widget')
                ->where('id', $id)
                ->sql();

            $itemBase = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

            $widget = new Widget($id);
            if ($itemBase[0]['published'] == "enable") {
                $status = 'disabled';
            } else {
                $status = 'enable';
            }
            $widget->setPublished($status);
            $widget->save();

            return $status;
        }
    }

    public function deleteWidget($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->delete('widget')
                ->where('id', $id)
                ->sql();

            $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }
}