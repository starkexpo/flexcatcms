<?php

namespace FlexCat\Admin\Modules\Model\Widget;

use FlexCat\Database\ActiveRecord;

class Widget
{
    use ActiveRecord;

    protected $table = 'widget';

    public $id;

    public $sections_id;

    public $classes;

    public $published;

    public $sorting;

    public $styles;

    public $description;

    public $dynamic_sections;

    public $dynamic_template;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSectionsId()
    {
        return $this->sections_id;
    }

    /**
     * @param mixed $sections_id
     */
    public function setSectionsId($sections_id)
    {
        $this->sections_id = $sections_id;
    }

    /**
     * @return mixed
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * @param mixed $classes
     */
    public function setClasses($classes)
    {
        $this->classes = $classes;
    }

    /**
     * @return mixed
     */
    public function getStyles()
    {
        return $this->styles;
    }

    /**
     * @param mixed $styles
     */
    public function setStyles($styles)
    {
        $this->styles = $styles;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDynamicSections()
    {
        return $this->dynamic_sections;
    }

    /**
     * @param mixed $dynamic_sections
     */
    public function setDynamicSections($dynamic_sections)
    {
        $this->dynamic_sections = $dynamic_sections;
    }

    /**
     * @return mixed
     */
    public function getDynamicTemplate()
    {
        return $this->dynamic_template;
    }

    /**
     * @param mixed $dynamic_template
     */
    public function setDynamicTemplate($dynamic_template)
    {
        $this->dynamic_template = $dynamic_template;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * @param mixed $sorting
     */
    public function setSorting($sorting)
    {
        $this->sorting = $sorting;
    }
}