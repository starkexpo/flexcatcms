<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 09/01/2019
 * Time: 03:21
 */

namespace FlexCat\Admin\Modules\Model\Tpltemp;

use FlexCat\Model;

class TpltempRepository extends Model
{
    public function getList($id)
    {
        if (isset($id) && $id > 0) {
            $sql = $this->queryBuilder->select()
                ->from('tpl_template')
                ->where("parent", $id)
                ->orderBy('type', 'DESC')
                ->sql();

        } else {
            $sql = $this->queryBuilder->select()
                ->from('tpl_template')
                ->where("parent", '0')
                ->orderBy('type', 'DESC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getSections()
    {
        $sql = $this->queryBuilder->select()
            ->from('tpl_template')
            ->where('type', 'section')
            ->orderBy('type', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createSection($params)
    {
        $tpltemp = new Tpltemp();
        $tpltemp->setTitle($params['title']);
        $tpltemp->setSite($params['site']);
        $tpltemp->setType("section");

        if ($params['parent'] > 0) {
            $tpltemp->setParent($params['parent']);
        }

        $sectionID = $tpltemp->save();

        return $sectionID;
    }

    public function createTpl($params)
    {
        $tpltemp = new Tpltemp();
        $tpltemp->setTitle($params['title']);
        $tpltemp->setSite($params['site']);
        $tpltemp->setType("tpl");
        $tpltemp->setCode($params['code']);

        if ($params['parent'] > 0) {
            $tpltemp->setParent($params['parent']);
        }

        $tplID = $tpltemp->save();

        return $tplID;
    }

    public function editSection($params)
    {
        $tpltemp = new Tpltemp();
        $tpltemp->setTitle($params['title']);
        $tpltemp->setSite($params['site']);
        $tpltemp->setId($params['sections_id']);

        if ($params['parent'] > 0) {
            $tpltemp->setParent($params['parent']);
        }

        $tpltemp->save();
    }

    public function getSectionData($id)
    {
        $tpltemp = new Tpltemp($id);

        return $tpltemp->findOne();
    }
}