<?php

namespace FlexCat\Admin\Modules\Model\Mailing;

use FlexCat\Model;

class MailingRepository extends Model
{
    public function getListMailing($id = '')
    {
        if ($id > 0) {
            $sql = $this->queryBuilder
                ->select()
                ->from('mailing')
                ->where('id', $id)
                ->orderBy('id', 'DESC')
                ->sql();
        } else {
            $sql = $this->queryBuilder
                ->select()
                ->from('mailing')
                ->where('parent', '0')
                ->orderBy('id', 'DESC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListRelease($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('mailing')
            ->where('parent', $id)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListUsers($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('mailing_users')
            ->where('mailing_id', $id)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getUser($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('mailing_users')
            ->where('id', $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createMailing($params)
    {
        $mailing = new Mailing();
        $mailing->setTitle($params['title']);

        if (strlen($params['description']) > 1) {
            $mailing->setDescription($params['description']);
        }
        $mailing->setEmailFrom($params['emailFrom']);

        $mailing->setNameFrom($params['nameFrom']);

        $mailing->setMailTemplate($params['mailTemplate']);
        if (strlen($params['themeFrom']) > 0) {
            $mailing->setTheme($params['themeFrom']);
        }

        $mailingID = $mailing->save();

        return $mailingID;
    }

    public function editMailing($params)
    {
        $mailing = new Mailing($params['mailing_id']);
        $mailing->setTitle($params['title']);
        $mailing->setId($params['mailing_id']);

        if (strlen($params['description']) > 1) {
            $mailing->setDescription($params['description']);
        }
        $mailing->setEmailFrom($params['emailFrom']);

        $mailing->setNameFrom($params['nameFrom']);

        $mailing->setMailTemplate($params['mailTemplate']);
        if (strlen($params['themeFrom']) > 0) {
            $mailing->setTheme($params['themeFrom']);
        }

        $mailingID = $mailing->save();

        return $mailingID;
    }

    public function getViewRelease($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('mailing')
            ->where('id', $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function deleteRelease($id)
    {
        $sqlDel = $this->queryBuilder
            ->delete('mailing')
            ->where('id', $id)
            ->sql();

        return $this->db->query($sqlDel, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createUser($params)
    {
        $email = $params['email'];
        $name = $params['name'];
        $mailing = $params['mailing'];

        $this->db->query("INSERT INTO mailing_users (name, email, mailing_id) VALUES('$name', '$email', '$mailing')");
    }

    public function updateUser($params)
    {
        $email = $params['email'];
        $name = $params['name'];
        $mailing = $params['mailing'];
        $idUs = $params['user_id'];

        $this->db->query("UPDATE mailing_users SET name = '$name', email = '$email', mailing_id = '$mailing' WHERE id = $idUs ");
    }
    public function deleteUser($id)
    {
        $sql = $this->queryBuilder
            ->delete('mailing_users')
            ->where("id", $id)
            ->sql();

        $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}