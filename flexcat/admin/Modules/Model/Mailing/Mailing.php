<?php

namespace FlexCat\Admin\Modules\Model\Mailing;

use FlexCat\Database\ActiveRecord;

class Mailing
{
    use ActiveRecord;

    protected $table = 'mailing';

    public $id;
    public $title;
    public $description;
    public $emailFrom;
    public $nameFrom;
    public $template;
    public $mailTemplate;
    public $theme;



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getEmailFrom()
    {
        return $this->emailFrom;
    }

    /**
     * @param mixed $emailFrom
     */
    public function setEmailFrom($emailFrom)
    {
        $this->emailFrom = $emailFrom;
    }

    /**
     * @return mixed
     */
    public function getNameFrom()
    {
        return $this->nameFrom;
    }

    /**
     * @param mixed $nameFrom
     */
    public function setNameFrom($nameFrom)
    {
        $this->nameFrom = $nameFrom;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getMailTemplate()
    {
        return $this->mailTemplate;
    }

    /**
     * @param mixed $mailTemplate
     */
    public function setMailTemplate($mailTemplate)
    {
        $this->mailTemplate = $mailTemplate;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }
}