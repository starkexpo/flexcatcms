<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 08/01/2019
 * Time: 19:03
 */

namespace FlexCat\Admin\Modules\Model\Sections;

use FlexCat\Database\ActiveRecord;

class Sections
{
    use ActiveRecord;

    protected $table = 'sections';

    public $id;

    public $title;

    public $alias;

    public $layouts_id;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return mixed
     */
    public function getLayoutsId()
    {
        return $this->layouts_id;
    }

    /**
     * @param mixed $layouts_id
     */
    public function setLayoutsId($layouts_id)
    {
        $this->layouts_id = $layouts_id;
    }
}