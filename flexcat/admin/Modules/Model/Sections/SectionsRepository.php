<?php

namespace FlexCat\Admin\Modules\Model\Sections;

use FlexCat\Model;
use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class SectionsRepository extends Model
{
    public function getListSections($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sections')
            ->where("layouts_id", $id)
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function gelBlockSection($id)
    {
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->select()
            ->from('sections')
            ->where("id", $id)
            ->orderBy('id', 'ASC')
            ->sql();

        $db = new Connection();

        return json_encode($db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC));
    }

    public function getListWidgets()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('widget')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getViewSection($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sections')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createSection($params)
    {
        $sections = new Sections();
        $sections->setTitle($params['title']);
        $sections->setAlias($params['alias']);
        $sections->setLayoutsId($params['layouts_id']);

        $sectionsID = $sections->save();

        $idTemp = intval($params['sites']);

        $sql = $this->queryBuilder
            ->select()
            ->from('sites')
            ->where("id", $idTemp)
            ->sql();

        $temp = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        $sql = $this->queryBuilder
            ->select()
            ->from('sites_templates')
            ->where("id", $temp[0]['template'])
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        $folder = "default";

        if (!empty($templates)) {
            $folder = $templates[0]['folder'];
        }

        $fileName = $params['alias'] . '.php';

        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Theme/" . $folder . "/" . $fileName;


        $codes = '<?php
        $blockID = ' . $sectionsID . ';
        Widgets::loadWidget($blockID);
        ';

//        //нужно указать путь для какого сайта создается шаблон
        @file_put_contents($path, $codes);

        return $sectionsID;
    }

    public function editSection($params)
    {
        $sections = new Sections($params['sections_id']);
        $sections->setTitle($params['title']);
        $sectionsID = $sections->save();

        return $sectionsID;
    }

    public function getSite($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites_templates')
            ->where("id", $id)
            ->sql();

        $templates = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        return $templates;
    }

    public function deleteSection($id)
    {
        $sql = $this->queryBuilder
            ->delete('sections')
            ->where("id", $id)
            ->sql();

        $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

}