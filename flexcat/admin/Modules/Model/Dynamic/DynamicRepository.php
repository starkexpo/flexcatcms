<?php

namespace FlexCat\Admin\Modules\Model\Dynamic;

use FlexCat\Model;

class DynamicRepository extends Model
{
    public function getList($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('dynamic_page')
                ->where("parent", $id)
                ->orderBy('type', 'ASC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        } else {

            $sql = $this->queryBuilder
                ->select()
                ->from('dynamic_page')
                ->where("parent", '0')
                ->orderBy('type', 'ASC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public function getSectionData($id)
    {
        $section = new Dynamic($id);

        return $section->findOne();
    }

    public function getListSection()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('dynamic_page')
            ->where("type", "section")
            ->orderBy('parent', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createSection($params)
    {
        $section = new Dynamic();
        $section->setTitle($params['title']);
        $section->setType("section");

        if ($params['parent'] > 0) {
            $section->setParent($params['parent']);
        }

        $sectionID = $section->save();

        return $sectionID;
    }


    public function updateSection($params)
    {
        if (isset($params['section_id']) && $params['section_id'] > 0) {
            $section = new Dynamic($params['section_id']);
            $section->setTitle($params['title']);
            $section->setType("section");

            if ($params['parent'] > 0) {
                $section->setParent($params['parent']);
            }

            $sectionID = $section->save();

            return $sectionID;
        }
    }

    public function createTemplate($params)
    {
//        var_dump($params);

        $template = new Dynamic();
        $template->setTitle($params['title']);
        $template->setType("template");

        if (strlen($params['description']) > 1) {
            $template->setDescription($params['description']);
        }
        $template->setCode($params['code']);

        if (strlen($params['css_lees']) > 1) {
            $template->setCss($params['css_lees']);
        }

        if (strlen($params['java_script']) > 1) {
            $template->setJavascript($params['java_script']);
        }

        if ($params['parent'] > 0) {
            $template->setParent($params['parent']);
        }
        $templateID = $template->save();

        $folder = "template" . $templateID;

        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Dynamic/";

        if (@chdir($path . $folder) === false) {
            @chdir($path);
            @mkdir($folder, 0777);
        }


        $filePath = $path . "/" . $folder . "/";

        if (strlen($params['code']) > 2) {
            @file_put_contents($filePath . "template.php", $params['code']);
        }

        if (strlen($params['css_lees']) > 2) {
            @file_put_contents($filePath . "style.css", $params['css_lees']);
        }

        if (strlen($params['java_script']) > 2) {
            @file_put_contents($filePath . "javascript.js", $params['java_script']);
        }

        return $templateID;
    }

    public function editTemplate($params)
    {
//        var_dump($params);

        $template = new Dynamic($params['template_id']);
        $template->setTitle($params['title']);
        $template->setType("template");

        if (strlen($params['description']) > 1) {
            $template->setDescription($params['description']);
        }
        $template->setCode($params['code']);

        if (strlen($params['css_lees']) > 1) {
            $template->setCss($params['css_lees']);
        }

        if (strlen($params['java_script']) > 1) {
            $template->setJavascript($params['java_script']);
        }

        if ($params['parent'] > 0) {
            $template->setParent($params['parent']);
        }
        $templateID = $template->save();

//        $folder = "template".$templateID;
        $folder = "template" . $params['template_id'];

        $path = $_SERVER['DOCUMENT_ROOT'] . "/Template/Dynamic/";

//        if (@chdir($path . $folder) === false) {
        @chdir($path);
//            @mkdir($folder, 0777);
//        }


        $filePath = $path . "/" . $folder . "/";

        if (strlen($params['code']) > 2) {
            @file_put_contents($filePath . "template.php", $params['code']);
        }

        if (strlen($params['css_lees']) > 2) {
            @file_put_contents($filePath . "style.css", $params['css_lees']);
        }

        if (strlen($params['java_script']) > 2) {
            @file_put_contents($filePath . "javascript.js", $params['java_script']);
        }

        return $templateID;
    }

    public function deleteTemplate($id)
    {
        $sql = $this->queryBuilder
            ->delete('dynamic_page')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}