<?php

namespace FlexCat\Admin\Modules\Model\Sidebar;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class  SidebarRepository
{
    public function getList()
    {
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder->select()
            ->from('admin_sidebar')
            ->orderBy('parent', 'ASC')
            ->orderBy('sorting', 'ASC')
            ->sql();

        $db = new Connection();


        return json_encode($db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC));
    }

    public function getUser($id)
    {
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->select()
            ->from('users')
            ->where('id', $id)
            ->sql();

        $db = new Connection();

        return json_encode($db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC));
    }
}