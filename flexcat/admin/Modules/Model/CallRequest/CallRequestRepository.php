<?php
namespace FlexCat\Admin\Modules\Model\CallRequest;

use FlexCat\Model;
use Grpc\Call;

class CallRequestRepository extends Model
{
    public function getListCall()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('call_request')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function setStatus($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('call_request')
            ->where('id', $id)
            ->orderBy('id', 'DESC')
            ->sql();

        $callID = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        foreach ($callID as $item) {
            if ($item['status'] <= 0) {
                $call = new CallRequest($item['id']);
                $call->setStatus(1);
                $call->save();
            } else {
                $def = 0;

                $this->db->query("UPDATE call_request SET status = '$def' WHERE id = '$id'");
            }
        }
    }

    public function deleteCall($id)
    {
        $sql = $this->queryBuilder
            ->delete('call_request')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}