<?php

namespace FlexCat\Admin\Modules\Model\Menu;

use FlexCat\Model;

class MenuRepository extends Model
{
    public function getMenu()
    {
        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getNowActive();
        $idSite = intval($nowSite[0]['id']);

        $sql = $this->queryBuilder
            ->select()
            ->from('menu')
            ->where('site_id', $idSite)
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getMenuData($id)
    {
        $menu = new Menu($id);

        return $menu->findOne();
    }

    public function createMenu($params)
    {
        $menu = new Menu();
        $menu->setTitle($params['title']);
        $menu->setSiteId($params['site_id']);
        $menuID = $menu->save();

        return $menuID;
    }

    public function updateMenu($params)
    {
        if (isset($params['menu_id']) && $params['menu_id'] > 0 && isset($params['title']) && strlen($params['title']) > 0) {

            $menu = new Menu($params['menu_id']);
            $menu->setTitle($params['title']);
            $menu->setSiteId($params['site_id']);
            $menuID = $menu->save();
        }
    }

    public function deleteMenu($id)
    {
        $sql = $this->queryBuilder
            ->delete('menu')
            ->where('id', $id)
            ->sql();

        return $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

}