<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 04/03/2019
 * Time: 22:39
 */

namespace FlexCat\Admin\Modules\Model\Infosys;

use FlexCat\Model;

class InfosysRepository extends Model
{
    public function getList($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('infosys')
                ->where("section", $id)
                ->orderBy('type', 'ASC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        } else {

            $sql = $this->queryBuilder
                ->select()
                ->from('infosys')
                ->where("section", '0')
                ->orderBy('type', 'ASC')
                ->sql();

            return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public function getListReleaseAll()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('infosys')
            ->where("type", 'release')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListRelease($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('infosys')
            ->where("section", $id)
            ->where("type", 'release')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getInfo($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('infosys')
            ->where("id", $id)
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getListSection()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('infosys')
            ->where("parent", '0')
            ->where('type', 'section')
            ->orderBy('type', 'ASC')
            ->sql();

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createInfo($params)
    {
        $info = new Infosys();
        $info->setTitle($params['title']);
        $info->setType("info");

        if ($params['section'] > 0) {
            $info->set($params['parent']);
        }
        $info->setSortField($params['sort_field']);
        $info->setSortDirection($params['sort_direction']);
        $info->setAlias($params['alias']);

        if (strlen($params['description']) > 1) {
            $info->setDescription($params['description']);
        }

        $info->setTypeUrl($params['type_url']);

        $infoID = $info->save();

        return $infoID;
    }

    public function updateInfo($params)
    {
        $info = new Infosys($params['id_info']);
        $info->setTitle($params['title']);
        $info->setType("info");

        if ($params['section'] > 0) {
            $info->set($params['parent']);
        }
        $info->setSortField($params['sort_field']);
        $info->setSortDirection($params['sort_direction']);
        $info->setAlias($params['alias']);

        if (strlen($params['description']) > 1) {
            $info->setDescription($params['description']);
        }

        $info->setTypeUrl($params['type_url']);

        $infoID = $info->save();

        return $infoID;
    }

    public function createRelease($params)
    {
        $release = new Infosys();
        $release->setTitle($params['title']);
        $release->setSection($params['section']);
        $release->setType('release');

        if (strlen($params['image_big']) > 0) {
            $release->setImgBig($params['image_big']);
        }
        if (strlen($params['description']) > 0) {
            $release->setDescription($params['description']);
        }
        if (strlen($params['text']) > 0) {
            $release->setText($params['text']);
        }
        $release->setAlias($params['alias']);

        if (isset($params['published']) && $params['published'] == "enable") {
            $release->setPublished($params['published']);
        } else {
            $release->setPublished('disabled');
        }
        if (strlen($params['date']) > 0) {
            $newDate = strtotime($params['date']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDate($newDate);
        }
        if (strlen($params['date_public']) > 0) {
            $newDate = strtotime($params['date_public']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDatePublic($newDate);
        }
        if (strlen($params['date_delpublic']) > 0) {
            $newDate = strtotime($params['date_delpublic']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDateDelpublic($newDate);
        }

        if (strlen($params['meta_title']) > 0 && $params['meta_title'] != '') {
            $release->setSeoTitle(trim($params['meta_title']));
        }

        if (strlen($params['meta_description']) > 0 && $params['meta_description'] != '') {
            $release->setSeoDescription(trim($params['meta_description']));
        }

        if (strlen($params['meta_keywords']) > 0 && $params['meta_keywords'] != '') {
            $release->setSeoKeywords(trim($params['meta_keywords']));
        }

        if (is_array($params['options']) && !empty($params['options'])) {
            $newOption = array();
            foreach ($params['options'] as $key => $option) {
                if ($option <> '') {
                    $newOption[] = ["$key" => $option];
                }
            }
            if (!empty($newOption)) {
                $release->setOptions(base64_encode(serialize($newOption)));
            }
        }

        $releaseID = $release->save();

        return $releaseID;
    }

    public function updateRelease($params)
    {
        $release = new Infosys($params['id_release']);
        $release->setTitle($params['title']);
        $release->setSection($params['section']);
        $release->setType('release');

        if (strlen($params['image_big']) > 0) {
            $release->setImgBig($params['image_big']);
        }
        if (strlen($params['description']) > 0) {
            $release->setDescription($params['description']);
        }
        if (strlen($params['text']) > 0) {
            $release->setText($params['text']);
        }
        $release->setAlias($params['alias']);

        if (isset($params['published']) && $params['published'] == "enable") {
            $release->setPublished($params['published']);
        } else {
            $release->setPublished('disabled');
        }
        if (strlen($params['date']) > 0) {
            $newDate = strtotime($params['date']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDate($newDate);
        }
        if (strlen($params['date_public']) > 0) {
            $newDate = strtotime($params['date_public']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDatePublic($newDate);
        }
        if (strlen($params['date_delpublic']) > 0) {
            $newDate = strtotime($params['date_delpublic']);
            $newDate = date("Y-m-d H:i:s", $newDate);
            $release->setDateDelpublic($newDate);
        }

        if (strlen($params['meta_title']) > 0 && $params['meta_title'] != '') {
            $release->setSeoTitle(trim($params['meta_title']));
        }

        if (strlen($params['meta_description']) > 0 && $params['meta_description'] != '') {
            $release->setSeoDescription(trim($params['meta_description']));
        }

        if (strlen($params['meta_keywords']) > 0 && $params['meta_keywords'] != '') {
            $release->setSeoKeywords(trim($params['meta_keywords']));
        }

        if (is_array($params['options']) && !empty($params['options'])) {
            $newOption = array();
            foreach ($params['options'] as $key => $option) {
                if ($option <> '') {
                    $newOption[] = ["$key" => $option];
                }
            }
            if (!empty($newOption)) {
                $release->setOptions(base64_encode(serialize($newOption)));
            }
        }
        $releaseID = $release->save();

        return $releaseID;
    }

    public function statusInfo($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->select()
                ->from('infosys')
                ->where('id', $id)
                ->sql();

            $itemBase = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);


            $release = new Infosys($id);
            if ($itemBase[0]['published'] == "enable") {
                $status = 'disabled';
            } else {
                $status = 'enable';
            }
            $release->setPublished($status);
            $release->save();

            return $status;
        }
    }

    public function deleteInfo($id)
    {
        if (isset($id) && $id > 0) {

            $sql = $this->queryBuilder
                ->delete('infosys')
                ->where('id', $id)
                ->sql();
            $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public function imgUpdate($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('infosys')
            ->where("id", $id)
            ->sql();

        $sites = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        @unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/img/infosys/" . $sites[0]["img_big"]);

        $data['img_big'] = '';
        $sql = $this->queryBuilder
            ->update('infosys')
            ->set($data)
            ->where("id", $id)
            ->sql();

        $this->db->execute($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}