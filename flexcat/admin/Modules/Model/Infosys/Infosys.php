<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 04/03/2019
 * Time: 22:25
 */

namespace FlexCat\Admin\Modules\Model\Infosys;

use FlexCat\Database\ActiveRecord;

class Infosys
{
    use  ActiveRecord;

    protected $table = 'infosys';

    public $id;
    public $title;
    public $section;
    public $sort_field;
    public $sort_direction;
    public $description;
    public $type;
    public $type_url;
    public $alias;
    public $text;
    public $date;
    public $date_public;
    public $date_delpublic;
    public $img_mini;
    public $img_big;
    public $published;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
    public $options;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSortField()
    {
        return $this->sort_field;
    }

    /**
     * @param mixed $sort_field
     */
    public function setSortField($sort_field)
    {
        $this->sort_field = $sort_field;
    }

    /**
     * @return mixed
     */
    public function getSortDirection()
    {
        return $this->sort_direction;
    }

    /**
     * @param mixed $sort_direction
     */
    public function setSortDirection($sort_direction)
    {
        $this->sort_direction = $sort_direction;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTypeUrl()
    {
        return $this->type_url;
    }

    /**
     * @param mixed $type_url
     */
    public function setTypeUrl($type_url)
    {
        $this->type_url = $type_url;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

    /**
     * @return mixed
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param mixed $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDatePublic()
    {
        return $this->date_public;
    }

    /**
     * @param mixed $date_public
     */
    public function setDatePublic($date_public)
    {
        $this->date_public = $date_public;
    }

    /**
     * @return mixed
     */
    public function getDateDelpublic()
    {
        return $this->date_delpublic;
    }

    /**
     * @param mixed $date_delpublic
     */
    public function setDateDelpublic($date_delpublic)
    {
        $this->date_delpublic = $date_delpublic;
    }

    /**
     * @return mixed
     */
    public function getImgMini()
    {
        return $this->img_mini;
    }

    /**
     * @param mixed $img_mini
     */
    public function setImgMini($img_mini)
    {
        $this->img_mini = $img_mini;
    }

    /**
     * @return mixed
     */
    public function getImgBig()
    {
        return $this->img_big;
    }

    /**
     * @param mixed $img_big
     */
    public function setImgBig($img_big)
    {
        $this->img_big = $img_big;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * @param mixed $seo_title
     */
    public function setSeoTitle($seo_title)
    {
        $this->seo_title = $seo_title;
    }

    /**
     * @return mixed
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * @param mixed $seo_description
     */
    public function setSeoDescription($seo_description)
    {
        $this->seo_description = $seo_description;
    }

    /**
     * @return mixed
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * @param mixed $seo_keywords
     */
    public function setSeoKeywords($seo_keywords)
    {
        $this->seo_keywords = $seo_keywords;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
}