<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 07/12/2018
 * Time: 15:57
 */

namespace FlexCat\Admin\Modules\Model\User;

use FlexCat\Model;

class UserRepository extends Model
{
    public function getUsers($id = '')
    {

        if (!isset($id) or strlen($id) <= 0) {
            $sql = $this->queryBuilder->select()
                ->from('users')
                ->where('role', 'admin')
                ->orderBy('id', 'DESC')
                ->sql();
        } elseif(isset($id) && $id > 0) {
            $sql = $this->queryBuilder->select()
                ->from('users')
                ->where('id', $id)
                ->orderBy('id', 'DESC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function getUserSite($id = '')
    {
        if (!isset($id) or strlen($id) <= 0) {
            $sql = $this->queryBuilder->select()
                ->from('users')
                ->where('role', 'user')
                ->orderBy('id', 'DESC')
                ->sql();
        } elseif(isset($id) && $id > 0) {
            $sql = $this->queryBuilder->select()
                ->from('users')
                ->where('id', $id)
                ->orderBy('id', 'DESC')
                ->sql();
        }

        return $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function createUser($params)
    {
//        var_dump($params);

        $user = new User();
        $user->setLogin($params['login']);
        $user->setPassword(md5($params['password']));
        $user->setRole($params['role']);

        if (strlen($params['lastname']) > 0) {
            $user->setLastname($params['lastname']);
        }

        if (strlen($params['firstname']) > 0) {
            $user->setFirstname($params['firstname']);
        }

        if (strlen($params['patronymic']) > 0) {
            $user->setPatronymic($params['patronymic']);
        }

        if (strlen($params['adress']) > 0) {
            $user->setAdress($params['adress']);
        }

        if (strlen($params['favicon']) > 0) {
            $user->setPhoto($params['favicon']);
        }

        $userId = $user->save();

        return $userId;
    }

    public function updateUser($params)
    {
//        var_dump($params);

        $user = new User($params['user_id']);
        $user->setLogin($params['login']);

        if (strlen($params['password']) > 8 && ($params['password'] == $params['password2'])) {
            $user->setPassword(md5($params['password']));
        }
        $user->setRole($params['role']);

        if (strlen($params['lastname']) > 0) {
            $user->setLastname($params['lastname']);
        }

        if (strlen($params['firstname']) > 0) {
            $user->setFirstname($params['firstname']);
        }

        if (strlen($params['patronymic']) > 0) {
            $user->setPatronymic($params['patronymic']);
        }

        if (strlen($params['adress']) > 0) {
            $user->setAdress($params['adress']);
        }

        if (strlen($params['favicon']) > 0) {
            $user->setPhoto($params['favicon']);
        }

        $userId = $user->save();

        return $userId;
    }

    public function avatarUpdate($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('users')
            ->where("id", $id)
            ->sql();

        $users = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        @unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/img/avatar/" . $users[0]["photo"]);

        $data['photo'] = '';
        $sqlUs = $this->queryBuilder
            ->update('users')
            ->set($data)
            ->where("id", $id)
            ->sql();

        $this->db->execute($sqlUs, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function deleteUser($id)
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('users')
            ->where("id", $id)
            ->sql();

        $users = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
        @unlink($_SERVER['DOCUMENT_ROOT'] . "/upload/img/avatar/" . $users[0]["photo"]);

        $sqlDel = $this->queryBuilder
            ->delete('users')
            ->where('id', $id)
            ->sql();

        return $this->db->query($sqlDel, $this->queryBuilder->values, \PDO::FETCH_ASSOC);
    }
}