<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <?php if (!empty($now_id)): ?>
            <span> / </span>
            <a href="/flexcat/admin/layouts/<?= $now_id[0]['id'] ?>"><?= $now_id[0]['title'] ?></a>
        <?php endif; ?>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <div class="container-bar-buttons">
        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-layout"></i>Макет
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/layouts/create/<?php if (isset($now_id)) {
                        echo $now_id[0]['id'];
                    } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

        <div class="group-but-sub m-r-1 group-blue" <?php if (isset($now_id) && $now_id[0]['type'] == "layouts") {
            echo ' style="display: none;" ';
        } ?>>
            <a href="#" class="btn btn-blue btn-group btn-bar">
                <i class="icofont-ui-folder"></i>Раздел макетов
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/layouts/section/create/<?php if (isset($now_id) && $now_id[0]['type'] == "section") {
                        echo $now_id[0]['id'];
                    } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

    </div>

    <?php

    //        var_dump($now_id);
    ?>

    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th>Сайт</th>
            <th width="10" align="center"><i class="icofont-navigation-menu sectionButton"></i></th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($layouts as $layout): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $layout['id'] ?>" class="check-item"></th>
                <th><?= $layout['id'] ?></th>
                <th width="22" align="center">
                    <?php if ($layout['type'] == "section"): ?>
                        <i class="icofont-ui-folder" style="font-size: 16px"></i>
                    <?php endif; ?>

                    <?php if ($layout['type'] == "layouts"): ?>
                        <i class="icofont-layout" style="font-size: 16px"></i>
                    <?php endif; ?>
                </th>
                <th>
                    <a href="/flexcat/admin/layouts/<?= $layout['id'] ?>"
                       class="title-item layouts-title"><?= $layout['title'] ?></a>
                </th>
                <td>
                    <?php
                    foreach ($sites as $site) {
                        if ($layout['site'] == $site->id) {
                            echo $site->title;
                        }
                    }
                    ?>
                </td>
                <td>
                    <?php if ($layout['type'] == "layouts"): ?>
                        <a href="/flexcat/admin/layouts/maket/section/<?= $layout['id'] ?>"
                           class="tooltip sectionButton" title="Секции макета">
                            <i class="icofont-navigation-menu"></i>
                            <?php
                            $countSections = 0;
                            foreach ($sectionsList as $section) {
                                if ($section['layouts_id'] == $layout['id']) {
                                    $countSections++;
                                }
                            }
                            ?>
                            <?php if ($countSections > 0): ?>
                                <span class="babgers-icons badge-red">
                                <?= $countSections ?>
                            </span>
                            <?php endif; ?>
                        </a>
                    <?php endif; ?>


                </td>
                <td>
                    <div class="btn-mini-group">
                        <a href="<?php
                        if ($layout['type'] == "section") {
                            echo '/flexcat/admin/layouts/section/edit/' . $layout['id'];
                        } else {
                            echo '/flexcat/admin/layouts/edit/' . $layout['id'];
                        }

                        ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="<?php
                        if ($layout['type'] == "section") {
                            echo '/flexcat/admin/layouts/section/delete/' . $layout['id'];
                        } else {
                            echo '/flexcat/admin/layouts/delete/' . $layout['id'];
                        }

                        ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
