<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



    <div class="container-pages-title">
        <div>
            <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
            <span> / </span>
            Редактирование макета
        </div>
        <?php $this->theme->block('controlPanel'); ?>
    </div>



    <div class="container-content">

        <form action="/flexcat/admin/layouts/update/" method="post" enctype="multipart/form-data" id="sendForm">

            <ul class="flexcat-tabs">
                <li><a href="#tab-1" class="tab-control">Основные</a></li>
                <li class="active-tabs"><a href="#tab-2" class="tab-control">Макет</a></li>
                <li><a href="#tab-3" class="tab-control">CSS/LESS</a></li>
                <li><a href="#tab-4" class="tab-control">JavaScript</a></li>
            </ul>


            <div class="tab-panel" id="tab-1">
                <label for="formTitle">Название макета</label>
                <input type="text" name="title" class="input-form" value="<?= $layoutsEdit->title ?>">
                <br>

                <input type="hidden" name="layouts_id" value="<?= $layoutsEdit->id ?>">


                <br>
                <label for="formTitle">Раздел макетов</label> <br>
                <select name="parent" id="siteLayout" class="select">
                    <option value="0" selected="selected">...</option>
                    <?php foreach ($sections as $section): ?>
                        <option value="<?= $section['id'] ?>" <?php
                            if ($section['id'] == $layoutsEdit->parent) :
                        ?> selected="selected"<?php endif; ?> ><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>

                <br>
                <br>
                <label for="formTitle">Родительский макет</label> <br>
                <select name="layouts" id="siteLayout" class="select">
                    <option value="0" selected="selected">...</option>
                    <?php foreach ($layouts as $layout): ?>
                        <option value="<?= $layout['id'] ?>" <?php
                        if ($layout['id'] == $layoutsEdit->parent_layouts) :
                            ?> selected="selected"<?php endif; ?> ><?= $layout['title'] ?></option>
                    <?php endforeach; ?>
                </select>

            </div>

            <div class="tab-panel active-tab" id="tab-2">
                <label for="formTitle" class="tooltip" title="Код макета">Макет</label> <br>
                <textarea cols="140" name="layout" rows="30" id="layoutCode"><?= $layoutsEdit->layout ?></textarea>
            </div>

            <div class="tab-panel" id="tab-3">
                <label for="formTitle" class="tooltip" title="CSS/LESS код стилей">CSS/LESS</label> <br>
                <textarea name="css_lees" id="cssLassCode" cols="140" rows="30"
                          class="form-control"><?= $layoutsEdit->css ?></textarea>
            </div>

            <div class="tab-panel" id="tab-4">
                <label for="formTitle" class="tooltip" title="JavaScript код">JavaScript</label> <br>
                <textarea name="java_script" id="javascriptCode" cols="140" rows="30"
                          class="form-control"><?= $layoutsEdit->javascript ?></textarea>
            </div>

            <div class="container-bar-buttons editing-bar">
                <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
                <button type="submit" class="btn btn-green" name="save_create">Применить</button>
                <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
            </div>
        </form>


    </div>
    <script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
    <!--<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>-->


    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.js"></script>
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.css">
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.css">

    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/css/css.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/javascript/javascript.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/clike/clike.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/php/php.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/xml/xml.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/smarty/smarty.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/selection/active-line.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/search.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/searchcursor.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.js"></script>


    <script src="/flexcat/admin/Assets/js/Plugin/Layouts/layouts.js"></script>
<?php $this->theme->footer(); ?>