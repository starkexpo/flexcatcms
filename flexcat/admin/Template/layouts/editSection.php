<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <span> / </span>
        <i class="icofont-ui-folder"></i>Редактирование раздела макетов
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">

    <form action="/flexcat/admin/layouts/section/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела макетов</label>
            <input type="text" name="title" class="input-form" value="<?= $sectionsEdit->title ?>">
            <br>

            <br>
            <label for="formTitle">Родительский раздел макетов</label> <br>
            <select name="parent" id="siteLayout" class="select">
                <option value="0" selected="selected">...</option>
                <?php foreach ($sections as $section): ?>
                    <option value="<?= $section['id'] ?>" <?php
                        if ($section['id'] == $sectionsEdit->parent) {
                            echo ' selected="selected"';
                        }
                    ?>><?= $section['title'] ?></option>
                <?php endforeach; ?>
            </select>

            <input type="hidden" name="site" value="<?= $sites[0]['id'] ?>">
            <input type="hidden" name="sections_id" value="<?= $sectionsEdit->id ?>">
        </div>




        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<?php $this->theme->footer(); ?>

