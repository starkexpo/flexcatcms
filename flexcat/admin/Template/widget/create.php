<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <span> / </span>
        <a href="/flexcat/admin/layouts/maket/section/<?= $makets[0]['layouts_id'] ?>">Секции макета "<?= $makets[0]['title'] ?>"
        </a>
        <span> / </span>
        Добавление виджета
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <form action="/flexcat/admin/layouts/maket/widget/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Дополнительные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">

            <div style="float: left; width: 45%;">
                <label for="formTitle">Атрибут "class"</label>
                <input type="text" name="classes" class="input-form">
            </div>

            <div style="float: right; width: 45%; margin-right: 14px;">
                <label for="formTitle">Атрибут "style"</label>
                <input type="text" name="styles" class="input-form">
            </div>

            <br><br><br><br>
            <label for="formTitle">Описание виджета</label><br>
            <textarea name="description" cols="30" rows="10" class="form-control" style="height: 60px"></textarea>

            <br>
            <div style="float: left; width: 45%;">
                <label for="formTitle">Раздел</label><br>
                <select name="dynamic_sections" id="sectionPage" class="select">
                    <?php foreach ($dynamic_sections as $section): ?>
                    <option value="<?= $section['id'] ?>"><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>

                <br><br><br>

                <label for="formTitle">Страница</label><br>
                <select name="dynamic_template" id="templatePage" class="select">
                    <option value="0">...</option>
                    <?php foreach ($dynamic_template as $template): ?>
                    <option value="<?= $template['id'] ?>"><?= $template['title'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div style="float: right; width: 45%; align-content: right; margin-right: 14px;">

                <label for="formTitle">Порядок сортировки</label>
                <input type="text" name="sorting" class="input-form" value="0">

                <br>
                <label class="label-page">Активность</label>
                <input type="checkbox" name="published" class="check-switch-ios" value="enable" checked/>
            </div>

            <br><br><br><br><br>
            <div id="optionLoad" style=" width: 100%; margin-top: 50px; padding-top: 20px;">
            </div>

        </div>

        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Идентификатор секции макета</label>
            <input type="text" name="sections_id" class="input-form" value="<?= $makets[0]['id'] ?>" readonly>
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/layouts/maket/widget/<?= $makets[0]['id'] ?>" class="btn btn-red"
               id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.switch-ios.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/Widget/widget.js"></script>


<?php $this->theme->footer(); ?>

