<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <span> / </span>
        <a href="/flexcat/admin/layouts/maket/section/<?= $makets[0]['layouts_id'] ?>">Секции макета
            "<?= $makets[0]['title'] ?>"
        </a>
        <span> / </span>
        <a href="/flexcat/admin/layouts/maket/widget/<?= $now_id ?>">Виджеты секции макета сайта</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">


    <div class="container-bar-buttons">
        <a href="/flexcat/admin/layouts/maket/widget/create/<?= $now_id ?>" class="btn btn-green btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>Добавить</a>
    </div>



    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="40%">Виджет</th>
            <th width="40%">Описание виджета</th>
            <th width="10" align="center"><i class="icofont-light-bulb public-structure"></i></th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($widgets as $widget): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $widget['id'] ?>" class="check-item"></th>
                <th><?= $widget['id'] ?></th>
                <th class="br-grey-column">
                    <?php
                    foreach ($dynamicsTemplate as $template) {
                        if ($widget['dynamic_template'] == $template['id']) {
                            $templateName = $template['title'];
                        }
                    }
                    echo 'Виджет::' . $templateName;
                    ?>
                </th>
                <td>
                    <?= $widget['description'] ?>
                </td>
                <td align="center">
                    <a href="/flexcat/admin/layouts/maket/widget/setpublic/<?= $widget['id'] ?>" class="published-button <?php
                    if ($widget['published'] == 'disabled') {
                        echo ' public-off';
                    }
                    ?>">
                        <i class="icofont-light-bulb"></i>
                    </a>
                </td>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/layouts/maket/widget/edit/<?= $widget['id'] ?>"
                           class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/layouts/maket/widget/delete/<?= $widget['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->
    <script src="/flexcat/admin/Assets/js/Plugin/Sites/jquery.publicButton.js"></script>

</div>
<?php $this->theme->footer(); ?>
