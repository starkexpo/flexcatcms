<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/callrequest/"><i class="icofont-support"></i>Заказы звонков</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">


    <div class="container-bar-buttons">
        <div class="statusCall call-green"></div> <div class="status-title-call"> - отмеченный исходящим вызовом</div>
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th>Имя</th>
            <th>Номер</th>
            <th width="80" align="center">Статус</th>
            <th width="140">Дата</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($calls as $item): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $item['id'] ?>" class="check-item"></th>
                <th><?= $item['id'] ?></th>
                <th><?= $item['name'] ?></th>
                <th><?= $item['phone'] ?></th>
                <th align="center">
                    <div class="statusCall <?php

                    if ($item['status'] > 0) {
                     echo " call-green";
                    }
                    ?>">
                    </div>
                </th>
                <th><?= $item['date'] ?></th>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/callrequest/status/<?= $item['id'] ?>" class="statusCall btn-mini-green">
                            <i class="icofont-check"></i>
                        </a>
                        <a href="/flexcat/admin/callrequest/delete/<?= $item['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <script src="/flexcat/admin/Assets/js/Plugin/CallRequest/callrequest.js"></script>

</div>
<?php $this->theme->footer(); ?>
