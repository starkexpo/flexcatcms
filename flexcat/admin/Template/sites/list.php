<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/sites/"><i class="icofont-world"></i>Сайты</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <div class="container-bar-buttons">
        <a href="/flexcat/admin/sites/create/" class="btn btn-green m-r-1 btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>Сайт
        </a>
        <!--<a href="#" class="btn btn-blue btn-bar">
            <i class="icofont-computer"></i>Темы
        </a>-->
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="30" align="center">Шаблон</th>
            <th>Название</th>
            <th align="center">По умолчанию</th>
            <th>Язык сайта</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($sites as $site): ?>
            <tr>
                <th><input type="checkbox" name="page<?= $site->id ?>" class="check-item"></th>
                <th><?= $site->id ?></th>
                <th align="center">
                    <i class="icofont-world tooltip-sites"></i>
                    <img src="/upload/img/sites/<?= $site->img ?>" alt="" class="img-sites-logo">
                </th>
                <td><?= $site->title ?></td>
                <td align="center" class="default-ico <?php if ($site->defaults == "enable"): ?>default_template_star
                <?php endif; ?>"><i class="icofont-star"></i></td>
                <td><?= $site->lang ?></td>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/sites/edit/<?= $site->id ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="/flexcat/admin/sites/defaults/<?= $site->id ?>" class="default-but btn-mini-yellow">
                            <i class="icofont-star"></i>
                        </a>
                        <a href="/flexcat/admin/sites/edit/<?= $site->id ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>

</div>
<?php $this->theme->footer(); ?>
