<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/sites/"><i class="icofont-world"></i>Список сайтов</a>
        <span> / </span>
        Редактирование сайта
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/sites/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основное</a></li>
            <li><a href="#tab-2" class="tab-control">Шаблон</a></li>
            <li><a href="#tab-3" class="tab-control">Ошибки</a></li>
            <li><a href="#tab-4" class="tab-control">robots.txt</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название сайта</label>
            <input type="text" name="title" class="input-form" required value="<?= $sites->title ?>">
            <input type="hidden" name="site_id" value="<?= $sites->id ?>">

            <br>
            <div style="float: left; ">
                <label class="label-page">Активность</label>
                <input type="checkbox" name="active" class="check-switch-ios" value="enable" <?php
                if ($sites->active == "enable"):?> checked<?php endif; ?>/>
            </div>

            <div style="float: left; margin-left: 20px">
                <label for="formTitle">Кодировка</label>
                <input type="text" name="encoding" class="input-form" value="<?= $sites->encoding ?>"
                       placeholder="UTF-8"
                       style="width: 150px">
            </div>

            <div style="float: left; margin-left: 20px; margin-top: -2px">
                <label class="label-page">Локализация</label>
                Установлено: <?= $sites->lang ?><br>
                <select name="locale" class="select" id="codingSite">
                    <option value="en_NZ">en_NZ</option>
                    <option value="nl_NL.UTF-8">nl_NL.UTF-8</option>
                    <option value="pt_BR.UTF-8">pt_BR.UTF-8</option>
                    <option value="fr_CH.ISO8859-15">fr_CH.ISO8859-15</option>
                    <option value="eu_ES.ISO8859-15">eu_ES.ISO8859-15</option>
                    <option value="en_US.US-ASCII">en_US.US-ASCII</option>
                    <option value="af_ZA">af_ZA</option>
                    <option value="bg_BG">bg_BG</option>
                    <option value="cs_CZ.UTF-8">cs_CZ.UTF-8</option>
                    <option value="fi_FI">fi_FI</option>
                    <option value="zh_CN.UTF-8">zh_CN.UTF-8</option>
                    <option value="eu_ES">eu_ES</option>
                    <option value="sk_SK.ISO8859-2">sk_SK.ISO8859-2</option>
                    <option value="nl_BE">nl_BE</option>
                    <option value="fr_BE">fr_BE</option>
                    <option value="sk_SK">sk_SK</option>
                    <option value="en_US.UTF-8">en_US.UTF-8</option>
                    <option value="en_NZ.ISO8859-1">en_NZ.ISO8859-1</option>
                    <option value="de_CH">de_CH</option>
                    <option value="sk_SK.UTF-8">sk_SK.UTF-8</option>
                    <option value="de_DE.UTF-8">de_DE.UTF-8</option>
                    <option value="am_ET.UTF-8">am_ET.UTF-8</option>
                    <option value="zh_HK">zh_HK</option>
                    <option value="be_BY.UTF-8">be_BY.UTF-8</option>
                    <option value="uk_UA">uk_UA</option>
                    <option value="pt_PT.ISO8859-1">pt_PT.ISO8859-1</option>
                    <option value="en_AU.US-ASCII">en_AU.US-ASCII</option>
                    <option value="kk_KZ.PT154">kk_KZ.PT154</option>
                    <option value="en_US">en_US</option>
                    <option value="nl_BE.ISO8859-15">nl_BE.ISO8859-15</option>
                    <option value="de_AT.ISO8859-1">de_AT.ISO8859-1</option>
                    <option value="hr_HR.ISO8859-2">hr_HR.ISO8859-2</option>
                    <option value="fr_FR.ISO8859-1">fr_FR.ISO8859-1</option>
                    <option value="af_ZA.UTF-8">af_ZA.UTF-8</option>
                    <option value="am_ET">am_ET</option>
                    <option value="fi_FI.ISO8859-1">fi_FI.ISO8859-1</option>
                    <option value="ro_RO.UTF-8">ro_RO.UTF-8</option>
                    <option value="af_ZA.ISO8859-15">af_ZA.ISO8859-15</option>
                    <option value="en_NZ.UTF-8">en_NZ.UTF-8</option>
                    <option value="fi_FI.UTF-8">fi_FI.UTF-8</option>
                    <option value="hr_HR.UTF-8">hr_HR.UTF-8</option>
                    <option value="da_DK.UTF-8">da_DK.UTF-8</option>
                    <option value="ca_ES.ISO8859-1">ca_ES.ISO8859-1</option>
                    <option value="en_AU.ISO8859-15">en_AU.ISO8859-15</option>
                    <option value="ro_RO.ISO8859-2">ro_RO.ISO8859-2</option>
                    <option value="de_AT.UTF-8">de_AT.UTF-8</option>
                    <option value="pt_PT.ISO8859-15">pt_PT.ISO8859-15</option>
                    <option value="sv_SE">sv_SE</option>
                    <option value="fr_CA.ISO8859-1">fr_CA.ISO8859-1</option>
                    <option value="fr_BE.ISO8859-1">fr_BE.ISO8859-1</option>
                    <option value="en_US.ISO8859-15">en_US.ISO8859-15</option>
                    <option value="it_CH.ISO8859-1">it_CH.ISO8859-1</option>
                    <option value="en_NZ.ISO8859-15">en_NZ.ISO8859-15</option>
                    <option value="en_AU.UTF-8">en_AU.UTF-8</option>
                    <option value="de_AT.ISO8859-15">de_AT.ISO8859-15</option>
                    <option value="af_ZA.ISO8859-1">af_ZA.ISO8859-1</option>
                    <option value="hu_HU.UTF-8">hu_HU.UTF-8</option>
                    <option value="et_EE.UTF-8">et_EE.UTF-8</option>
                    <option value="he_IL.UTF-8">he_IL.UTF-8</option>
                    <option value="uk_UA.KOI8-U">uk_UA.KOI8-U</option>
                    <option value="be_BY">be_BY</option>
                    <option value="kk_KZ">kk_KZ</option>
                    <option value="hu_HU.ISO8859-2">hu_HU.ISO8859-2</option>
                    <option value="it_CH">it_CH</option>
                    <option value="pt_BR">pt_BR</option>
                    <option value="ko_KR">ko_KR</option>
                    <option value="it_IT">it_IT</option>
                    <option value="fr_BE.UTF-8">fr_BE.UTF-8</option>
                    <option value="ru_RU.ISO8859-5">ru_RU.ISO8859-5</option>
                    <option value="zh_TW">zh_TW</option>
                    <option value="zh_CN.GB2312">zh_CN.GB2312</option>
                    <option value="no_NO.ISO8859-15">no_NO.ISO8859-15</option>
                    <option value="de_DE.ISO8859-15">de_DE.ISO8859-15</option>
                    <option value="en_CA">en_CA</option>
                    <option value="fr_CH.UTF-8">fr_CH.UTF-8</option>
                    <option value="sl_SI.UTF-8">sl_SI.UTF-8</option>
                    <option value="uk_UA.ISO8859-5">uk_UA.ISO8859-5</option>
                    <option value="pt_PT">pt_PT</option>
                    <option value="hr_HR">hr_HR</option>
                    <option value="cs_CZ">cs_CZ</option>
                    <option value="fr_CH">fr_CH</option>
                    <option value="he_IL">he_IL</option>
                    <option value="zh_CN.GBK">zh_CN.GBK</option>
                    <option value="zh_CN.GB18030">zh_CN.GB18030</option>
                    <option value="fr_CA">fr_CA</option>
                    <option value="pl_PL.UTF-8">pl_PL.UTF-8</option>
                    <option value="ja_JP.SJIS">ja_JP.SJIS</option>
                    <option value="sr_YU.ISO8859-5">sr_YU.ISO8859-5</option>
                    <option value="be_BY.CP1251">be_BY.CP1251</option>
                    <option value="sr_YU.ISO8859-2">sr_YU.ISO8859-2</option>
                    <option value="sv_SE.UTF-8">sv_SE.UTF-8</option>
                    <option value="sr_YU.UTF-8">sr_YU.UTF-8</option>
                    <option value="de_CH.UTF-8">de_CH.UTF-8</option>
                    <option value="sl_SI">sl_SI</option>
                    <option value="pt_PT.UTF-8">pt_PT.UTF-8</option>
                    <option value="ro_RO">ro_RO</option>
                    <option value="en_NZ.US-ASCII">en_NZ.US-ASCII</option>
                    <option value="ja_JP">ja_JP</option>
                    <option value="zh_CN">zh_CN</option>
                    <option value="fr_CH.ISO8859-1">fr_CH.ISO8859-1</option>
                    <option value="ko_KR.eucKR">ko_KR.eucKR</option>
                    <option value="be_BY.ISO8859-5">be_BY.ISO8859-5</option>
                    <option value="nl_NL.ISO8859-15">nl_NL.ISO8859-15</option>
                    <option value="en_GB.ISO8859-1">en_GB.ISO8859-1</option>
                    <option value="en_CA.US-ASCII">en_CA.US-ASCII</option>
                    <option value="is_IS.ISO8859-1">is_IS.ISO8859-1</option>
                    <option value="ru_RU.CP866">ru_RU.CP866</option>
                    <option value="nl_NL">nl_NL</option>
                    <option value="fr_CA.ISO8859-15">fr_CA.ISO8859-15</option>
                    <option value="sv_SE.ISO8859-15">sv_SE.ISO8859-15</option>
                    <option value="hy_AM">hy_AM</option>
                    <option value="en_CA.ISO8859-15">en_CA.ISO8859-15</option>
                    <option value="en_US.ISO8859-1">en_US.ISO8859-1</option>
                    <option value="zh_TW.Big5">zh_TW.Big5</option>
                    <option value="ca_ES.UTF-8">ca_ES.UTF-8</option>
                    <option value="ru_RU.CP1251">ru_RU.CP1251</option>
                    <option value="en_GB.UTF-8">en_GB.UTF-8</option>
                    <option value="en_GB.US-ASCII">en_GB.US-ASCII</option>
                    <option value="ru_RU.UTF-8">ru_RU.UTF-8</option>
                    <option value="eu_ES.UTF-8">eu_ES.UTF-8</option>
                    <option value="es_ES.ISO8859-1">es_ES.ISO8859-1</option>
                    <option value="hu_HU">hu_HU</option>
                    <option value="el_GR.ISO8859-7">el_GR.ISO8859-7</option>
                    <option value="en_AU">en_AU</option>
                    <option value="it_CH.UTF-8">it_CH.UTF-8</option>
                    <option value="en_GB">en_GB</option>
                    <option value="sl_SI.ISO8859-2">sl_SI.ISO8859-2</option>
                    <option value="ru_RU.KOI8-R">ru_RU.KOI8-R</option>
                    <option value="nl_BE.UTF-8">nl_BE.UTF-8</option>
                    <option value="et_EE">et_EE</option>
                    <option value="fr_FR.ISO8859-15">fr_FR.ISO8859-15</option>
                    <option value="cs_CZ.ISO8859-2">cs_CZ.ISO8859-2</option>
                    <option value="lt_LT.UTF-8">lt_LT.UTF-8</option>
                    <option value="pl_PL.ISO8859-2">pl_PL.ISO8859-2</option>
                    <option value="fr_BE.ISO8859-15">fr_BE.ISO8859-15</option>
                    <option value="is_IS.UTF-8">is_IS.UTF-8</option>
                    <option value="tr_TR.ISO8859-9">tr_TR.ISO8859-9</option>
                    <option value="da_DK.ISO8859-1">da_DK.ISO8859-1</option>
                    <option value="lt_LT.ISO8859-4">lt_LT.ISO8859-4</option>
                    <option value="lt_LT.ISO8859-13">lt_LT.ISO8859-13</option>
                    <option value="zh_TW.UTF-8">zh_TW.UTF-8</option>
                    <option value="bg_BG.CP1251">bg_BG.CP1251</option>
                    <option value="el_GR.UTF-8">el_GR.UTF-8</option>
                    <option value="be_BY.CP1131">be_BY.CP1131</option>
                    <option value="da_DK.ISO8859-15">da_DK.ISO8859-15</option>
                    <option value="is_IS.ISO8859-15">is_IS.ISO8859-15</option>
                    <option value="no_NO.ISO8859-1">no_NO.ISO8859-1</option>
                    <option value="nl_NL.ISO8859-1">nl_NL.ISO8859-1</option>
                    <option value="nl_BE.ISO8859-1">nl_BE.ISO8859-1</option>
                    <option value="sv_SE.ISO8859-1">sv_SE.ISO8859-1</option>
                    <option value="pt_BR.ISO8859-1">pt_BR.ISO8859-1</option>
                    <option value="zh_CN.eucCN">zh_CN.eucCN</option>
                    <option value="it_IT.UTF-8">it_IT.UTF-8</option>
                    <option value="en_CA.UTF-8">en_CA.UTF-8</option>
                    <option value="uk_UA.UTF-8">uk_UA.UTF-8</option>
                    <option value="de_CH.ISO8859-15">de_CH.ISO8859-15</option>
                    <option value="de_DE.ISO8859-1">de_DE.ISO8859-1</option>
                    <option value="ca_ES">ca_ES</option>
                    <option value="sr_YU">sr_YU</option>
                    <option value="hy_AM.ARMSCII-8">hy_AM.ARMSCII-8</option>
                    <option value="ru_RU">ru_RU</option>
                    <option value="zh_HK.UTF-8">zh_HK.UTF-8</option>
                    <option value="eu_ES.ISO8859-1">eu_ES.ISO8859-1</option>
                    <option value="is_IS">is_IS</option>
                    <option value="bg_BG.UTF-8">bg_BG.UTF-8</option>
                    <option value="ja_JP.UTF-8">ja_JP.UTF-8</option>
                    <option value="it_CH.ISO8859-15">it_CH.ISO8859-15</option>
                    <option value="fr_FR.UTF-8">fr_FR.UTF-8</option>
                    <option value="ko_KR.UTF-8">ko_KR.UTF-8</option>
                    <option value="et_EE.ISO8859-15">et_EE.ISO8859-15</option>
                    <option value="kk_KZ.UTF-8">kk_KZ.UTF-8</option>
                    <option value="ca_ES.ISO8859-15">ca_ES.ISO8859-15</option>
                    <option value="en_IE.UTF-8">en_IE.UTF-8</option>
                    <option value="es_ES">es_ES</option>
                    <option value="de_CH.ISO8859-1">de_CH.ISO8859-1</option>
                    <option value="en_CA.ISO8859-1">en_CA.ISO8859-1</option>
                    <option value="es_ES.ISO8859-15">es_ES.ISO8859-15</option>
                    <option value="en_AU.ISO8859-1">en_AU.ISO8859-1</option>
                    <option value="el_GR">el_GR</option>
                    <option value="da_DK">da_DK</option>
                    <option value="no_NO">no_NO</option>
                    <option value="it_IT.ISO8859-1">it_IT.ISO8859-1</option>
                    <option value="en_IE">en_IE</option>
                    <option value="zh_HK.Big5HKSCS">zh_HK.Big5HKSCS</option>
                    <option value="hi_IN.ISCII-DEV">hi_IN.ISCII-DEV</option>
                    <option value="ja_JP.eucJP">ja_JP.eucJP</option>
                    <option value="it_IT.ISO8859-15">it_IT.ISO8859-15</option>
                    <option value="pl_PL">pl_PL</option>
                    <option value="ko_KR.CP949">ko_KR.CP949</option>
                    <option value="fr_CA.UTF-8">fr_CA.UTF-8</option>
                    <option value="fi_FI.ISO8859-15">fi_FI.ISO8859-15</option>
                    <option value="en_GB.ISO8859-15">en_GB.ISO8859-15</option>
                    <option value="fr_FR">fr_FR</option>
                    <option value="hy_AM.UTF-8">hy_AM.UTF-8</option>
                    <option value="no_NO.UTF-8">no_NO.UTF-8</option>
                    <option value="es_ES.UTF-8">es_ES.UTF-8</option>
                    <option value="de_AT">de_AT</option>
                    <option value="tr_TR.UTF-8">tr_TR.UTF-8</option>
                    <option value="de_DE">de_DE</option>
                    <option value="lt_LT">lt_LT</option>
                    <option value="tr_TR">tr_TR</option>
                    <option value="C">C</option>
                    <option value="POSIX">POSIX</option>
                </select>
            </div>

            <div style="float: left; margin-left: 20px; margin-top: -2px">
                <label class="label-page" style="width: 250px">Часовой пояс</label>
                Установлено: <?= $sites->timezone ?><br>
                <select name="timezone" class="select" id="timeZ">
                    <option value="Europe/Moscow" selected="selected">По умолчанию</option>
                    <option value="Africa/Abidjan">Africa/Abidjan</option>
                    <option value="Africa/Accra">Africa/Accra</option>
                    <option value="Africa/Addis_Ababa">Africa/Addis_Ababa</option>
                    <option value="Africa/Algiers">Africa/Algiers</option>
                    <option value="Africa/Asmara">Africa/Asmara</option>
                    <option value="Africa/Bamako">Africa/Bamako</option>
                    <option value="Africa/Bangui">Africa/Bangui</option>
                    <option value="Africa/Banjul">Africa/Banjul</option>
                    <option value="Africa/Bissau">Africa/Bissau</option>
                    <option value="Africa/Blantyre">Africa/Blantyre</option>
                    <option value="Africa/Brazzaville">Africa/Brazzaville</option>
                    <option value="Africa/Bujumbura">Africa/Bujumbura</option>
                    <option value="Africa/Cairo">Africa/Cairo</option>
                    <option value="Africa/Casablanca">Africa/Casablanca</option>
                    <option value="Africa/Ceuta">Africa/Ceuta</option>
                    <option value="Africa/Conakry">Africa/Conakry</option>
                    <option value="Africa/Dakar">Africa/Dakar</option>
                    <option value="Africa/Dar_es_Salaam">Africa/Dar_es_Salaam</option>
                    <option value="Africa/Djibouti">Africa/Djibouti</option>
                    <option value="Africa/Douala">Africa/Douala</option>
                    <option value="Africa/El_Aaiun">Africa/El_Aaiun</option>
                    <option value="Africa/Freetown">Africa/Freetown</option>
                    <option value="Africa/Gaborone">Africa/Gaborone</option>
                    <option value="Africa/Harare">Africa/Harare</option>
                    <option value="Africa/Johannesburg">Africa/Johannesburg</option>
                    <option value="Africa/Juba">Africa/Juba</option>
                    <option value="Africa/Kampala">Africa/Kampala</option>
                    <option value="Africa/Khartoum">Africa/Khartoum</option>
                    <option value="Africa/Kigali">Africa/Kigali</option>
                    <option value="Africa/Kinshasa">Africa/Kinshasa</option>
                    <option value="Africa/Lagos">Africa/Lagos</option>
                    <option value="Africa/Libreville">Africa/Libreville</option>
                    <option value="Africa/Lome">Africa/Lome</option>
                    <option value="Africa/Luanda">Africa/Luanda</option>
                    <option value="Africa/Lubumbashi">Africa/Lubumbashi</option>
                    <option value="Africa/Lusaka">Africa/Lusaka</option>
                    <option value="Africa/Malabo">Africa/Malabo</option>
                    <option value="Africa/Maputo">Africa/Maputo</option>
                    <option value="Africa/Maseru">Africa/Maseru</option>
                    <option value="Africa/Mbabane">Africa/Mbabane</option>
                    <option value="Africa/Mogadishu">Africa/Mogadishu</option>
                    <option value="Africa/Monrovia">Africa/Monrovia</option>
                    <option value="Africa/Nairobi">Africa/Nairobi</option>
                    <option value="Africa/Ndjamena">Africa/Ndjamena</option>
                    <option value="Africa/Niamey">Africa/Niamey</option>
                    <option value="Africa/Nouakchott">Africa/Nouakchott</option>
                    <option value="Africa/Ouagadougou">Africa/Ouagadougou</option>
                    <option value="Africa/Porto-Novo">Africa/Porto-Novo</option>
                    <option value="Africa/Sao_Tome">Africa/Sao_Tome</option>
                    <option value="Africa/Tripoli">Africa/Tripoli</option>
                    <option value="Africa/Tunis">Africa/Tunis</option>
                    <option value="Africa/Windhoek">Africa/Windhoek</option>
                    <option value="America/Adak">America/Adak</option>
                    <option value="America/Anchorage">America/Anchorage</option>
                    <option value="America/Anguilla">America/Anguilla</option>
                    <option value="America/Antigua">America/Antigua</option>
                    <option value="America/Araguaina">America/Araguaina</option>
                    <option value="America/Argentina/Buenos_Aires">America/Argentina/Buenos_Aires</option>
                    <option value="America/Argentina/Catamarca">America/Argentina/Catamarca</option>
                    <option value="America/Argentina/Cordoba">America/Argentina/Cordoba</option>
                    <option value="America/Argentina/Jujuy">America/Argentina/Jujuy</option>
                    <option value="America/Argentina/La_Rioja">America/Argentina/La_Rioja</option>
                    <option value="America/Argentina/Mendoza">America/Argentina/Mendoza</option>
                    <option value="America/Argentina/Rio_Gallegos">America/Argentina/Rio_Gallegos</option>
                    <option value="America/Argentina/Salta">America/Argentina/Salta</option>
                    <option value="America/Argentina/San_Juan">America/Argentina/San_Juan</option>
                    <option value="America/Argentina/San_Luis">America/Argentina/San_Luis</option>
                    <option value="America/Argentina/Tucuman">America/Argentina/Tucuman</option>
                    <option value="America/Argentina/Ushuaia">America/Argentina/Ushuaia</option>
                    <option value="America/Aruba">America/Aruba</option>
                    <option value="America/Asuncion">America/Asuncion</option>
                    <option value="America/Atikokan">America/Atikokan</option>
                    <option value="America/Bahia">America/Bahia</option>
                    <option value="America/Bahia_Banderas">America/Bahia_Banderas</option>
                    <option value="America/Barbados">America/Barbados</option>
                    <option value="America/Belem">America/Belem</option>
                    <option value="America/Belize">America/Belize</option>
                    <option value="America/Blanc-Sablon">America/Blanc-Sablon</option>
                    <option value="America/Boa_Vista">America/Boa_Vista</option>
                    <option value="America/Bogota">America/Bogota</option>
                    <option value="America/Boise">America/Boise</option>
                    <option value="America/Cambridge_Bay">America/Cambridge_Bay</option>
                    <option value="America/Campo_Grande">America/Campo_Grande</option>
                    <option value="America/Cancun">America/Cancun</option>
                    <option value="America/Caracas">America/Caracas</option>
                    <option value="America/Cayenne">America/Cayenne</option>
                    <option value="America/Cayman">America/Cayman</option>
                    <option value="America/Chicago">America/Chicago</option>
                    <option value="America/Chihuahua">America/Chihuahua</option>
                    <option value="America/Costa_Rica">America/Costa_Rica</option>
                    <option value="America/Creston">America/Creston</option>
                    <option value="America/Cuiaba">America/Cuiaba</option>
                    <option value="America/Curacao">America/Curacao</option>
                    <option value="America/Danmarkshavn">America/Danmarkshavn</option>
                    <option value="America/Dawson">America/Dawson</option>
                    <option value="America/Dawson_Creek">America/Dawson_Creek</option>
                    <option value="America/Denver">America/Denver</option>
                    <option value="America/Detroit">America/Detroit</option>
                    <option value="America/Dominica">America/Dominica</option>
                    <option value="America/Edmonton">America/Edmonton</option>
                    <option value="America/Eirunepe">America/Eirunepe</option>
                    <option value="America/El_Salvador">America/El_Salvador</option>
                    <option value="America/Fort_Nelson">America/Fort_Nelson</option>
                    <option value="America/Fortaleza">America/Fortaleza</option>
                    <option value="America/Glace_Bay">America/Glace_Bay</option>
                    <option value="America/Godthab">America/Godthab</option>
                    <option value="America/Goose_Bay">America/Goose_Bay</option>
                    <option value="America/Grand_Turk">America/Grand_Turk</option>
                    <option value="America/Grenada">America/Grenada</option>
                    <option value="America/Guadeloupe">America/Guadeloupe</option>
                    <option value="America/Guatemala">America/Guatemala</option>
                    <option value="America/Guayaquil">America/Guayaquil</option>
                    <option value="America/Guyana">America/Guyana</option>
                    <option value="America/Halifax">America/Halifax</option>
                    <option value="America/Havana">America/Havana</option>
                    <option value="America/Hermosillo">America/Hermosillo</option>
                    <option value="America/Indiana/Indianapolis">America/Indiana/Indianapolis</option>
                    <option value="America/Indiana/Knox">America/Indiana/Knox</option>
                    <option value="America/Indiana/Marengo">America/Indiana/Marengo</option>
                    <option value="America/Indiana/Petersburg">America/Indiana/Petersburg</option>
                    <option value="America/Indiana/Tell_City">America/Indiana/Tell_City</option>
                    <option value="America/Indiana/Vevay">America/Indiana/Vevay</option>
                    <option value="America/Indiana/Vincennes">America/Indiana/Vincennes</option>
                    <option value="America/Indiana/Winamac">America/Indiana/Winamac</option>
                    <option value="America/Inuvik">America/Inuvik</option>
                    <option value="America/Iqaluit">America/Iqaluit</option>
                    <option value="America/Jamaica">America/Jamaica</option>
                    <option value="America/Juneau">America/Juneau</option>
                    <option value="America/Kentucky/Louisville">America/Kentucky/Louisville</option>
                    <option value="America/Kentucky/Monticello">America/Kentucky/Monticello</option>
                    <option value="America/Kralendijk">America/Kralendijk</option>
                    <option value="America/La_Paz">America/La_Paz</option>
                    <option value="America/Lima">America/Lima</option>
                    <option value="America/Los_Angeles">America/Los_Angeles</option>
                    <option value="America/Lower_Princes">America/Lower_Princes</option>
                    <option value="America/Maceio">America/Maceio</option>
                    <option value="America/Managua">America/Managua</option>
                    <option value="America/Manaus">America/Manaus</option>
                    <option value="America/Marigot">America/Marigot</option>
                    <option value="America/Martinique">America/Martinique</option>
                    <option value="America/Matamoros">America/Matamoros</option>
                    <option value="America/Mazatlan">America/Mazatlan</option>
                    <option value="America/Menominee">America/Menominee</option>
                    <option value="America/Merida">America/Merida</option>
                    <option value="America/Metlakatla">America/Metlakatla</option>
                    <option value="America/Mexico_City">America/Mexico_City</option>
                    <option value="America/Miquelon">America/Miquelon</option>
                    <option value="America/Moncton">America/Moncton</option>
                    <option value="America/Monterrey">America/Monterrey</option>
                    <option value="America/Montevideo">America/Montevideo</option>
                    <option value="America/Montserrat">America/Montserrat</option>
                    <option value="America/Nassau">America/Nassau</option>
                    <option value="America/New_York">America/New_York</option>
                    <option value="America/Nipigon">America/Nipigon</option>
                    <option value="America/Nome">America/Nome</option>
                    <option value="America/Noronha">America/Noronha</option>
                    <option value="America/North_Dakota/Beulah">America/North_Dakota/Beulah</option>
                    <option value="America/North_Dakota/Center">America/North_Dakota/Center</option>
                    <option value="America/North_Dakota/New_Salem">America/North_Dakota/New_Salem</option>
                    <option value="America/Ojinaga">America/Ojinaga</option>
                    <option value="America/Panama">America/Panama</option>
                    <option value="America/Pangnirtung">America/Pangnirtung</option>
                    <option value="America/Paramaribo">America/Paramaribo</option>
                    <option value="America/Phoenix">America/Phoenix</option>
                    <option value="America/Port-au-Prince">America/Port-au-Prince</option>
                    <option value="America/Port_of_Spain">America/Port_of_Spain</option>
                    <option value="America/Porto_Velho">America/Porto_Velho</option>
                    <option value="America/Puerto_Rico">America/Puerto_Rico</option>
                    <option value="America/Punta_Arenas">America/Punta_Arenas</option>
                    <option value="America/Rainy_River">America/Rainy_River</option>
                    <option value="America/Rankin_Inlet">America/Rankin_Inlet</option>
                    <option value="America/Recife">America/Recife</option>
                    <option value="America/Regina">America/Regina</option>
                    <option value="America/Resolute">America/Resolute</option>
                    <option value="America/Rio_Branco">America/Rio_Branco</option>
                    <option value="America/Santarem">America/Santarem</option>
                    <option value="America/Santiago">America/Santiago</option>
                    <option value="America/Santo_Domingo">America/Santo_Domingo</option>
                    <option value="America/Sao_Paulo">America/Sao_Paulo</option>
                    <option value="America/Scoresbysund">America/Scoresbysund</option>
                    <option value="America/Sitka">America/Sitka</option>
                    <option value="America/St_Barthelemy">America/St_Barthelemy</option>
                    <option value="America/St_Johns">America/St_Johns</option>
                    <option value="America/St_Kitts">America/St_Kitts</option>
                    <option value="America/St_Lucia">America/St_Lucia</option>
                    <option value="America/St_Thomas">America/St_Thomas</option>
                    <option value="America/St_Vincent">America/St_Vincent</option>
                    <option value="America/Swift_Current">America/Swift_Current</option>
                    <option value="America/Tegucigalpa">America/Tegucigalpa</option>
                    <option value="America/Thule">America/Thule</option>
                    <option value="America/Thunder_Bay">America/Thunder_Bay</option>
                    <option value="America/Tijuana">America/Tijuana</option>
                    <option value="America/Toronto">America/Toronto</option>
                    <option value="America/Tortola">America/Tortola</option>
                    <option value="America/Vancouver">America/Vancouver</option>
                    <option value="America/Whitehorse">America/Whitehorse</option>
                    <option value="America/Winnipeg">America/Winnipeg</option>
                    <option value="America/Yakutat">America/Yakutat</option>
                    <option value="America/Yellowknife">America/Yellowknife</option>
                    <option value="Antarctica/Casey">Antarctica/Casey</option>
                    <option value="Antarctica/Davis">Antarctica/Davis</option>
                    <option value="Antarctica/DumontDUrville">Antarctica/DumontDUrville</option>
                    <option value="Antarctica/Macquarie">Antarctica/Macquarie</option>
                    <option value="Antarctica/Mawson">Antarctica/Mawson</option>
                    <option value="Antarctica/McMurdo">Antarctica/McMurdo</option>
                    <option value="Antarctica/Palmer">Antarctica/Palmer</option>
                    <option value="Antarctica/Rothera">Antarctica/Rothera</option>
                    <option value="Antarctica/Syowa">Antarctica/Syowa</option>
                    <option value="Antarctica/Troll">Antarctica/Troll</option>
                    <option value="Antarctica/Vostok">Antarctica/Vostok</option>
                    <option value="Arctic/Longyearbyen">Arctic/Longyearbyen</option>
                    <option value="Asia/Aden">Asia/Aden</option>
                    <option value="Asia/Almaty">Asia/Almaty</option>
                    <option value="Asia/Amman">Asia/Amman</option>
                    <option value="Asia/Anadyr">Asia/Anadyr</option>
                    <option value="Asia/Aqtau">Asia/Aqtau</option>
                    <option value="Asia/Aqtobe">Asia/Aqtobe</option>
                    <option value="Asia/Ashgabat">Asia/Ashgabat</option>
                    <option value="Asia/Atyrau">Asia/Atyrau</option>
                    <option value="Asia/Baghdad">Asia/Baghdad</option>
                    <option value="Asia/Bahrain">Asia/Bahrain</option>
                    <option value="Asia/Baku">Asia/Baku</option>
                    <option value="Asia/Bangkok">Asia/Bangkok</option>
                    <option value="Asia/Barnaul">Asia/Barnaul</option>
                    <option value="Asia/Beirut">Asia/Beirut</option>
                    <option value="Asia/Bishkek">Asia/Bishkek</option>
                    <option value="Asia/Brunei">Asia/Brunei</option>
                    <option value="Asia/Chita">Asia/Chita</option>
                    <option value="Asia/Choibalsan">Asia/Choibalsan</option>
                    <option value="Asia/Colombo">Asia/Colombo</option>
                    <option value="Asia/Damascus">Asia/Damascus</option>
                    <option value="Asia/Dhaka">Asia/Dhaka</option>
                    <option value="Asia/Dili">Asia/Dili</option>
                    <option value="Asia/Dubai">Asia/Dubai</option>
                    <option value="Asia/Dushanbe">Asia/Dushanbe</option>
                    <option value="Asia/Famagusta">Asia/Famagusta</option>
                    <option value="Asia/Gaza">Asia/Gaza</option>
                    <option value="Asia/Hebron">Asia/Hebron</option>
                    <option value="Asia/Ho_Chi_Minh">Asia/Ho_Chi_Minh</option>
                    <option value="Asia/Hong_Kong">Asia/Hong_Kong</option>
                    <option value="Asia/Hovd">Asia/Hovd</option>
                    <option value="Asia/Irkutsk">Asia/Irkutsk</option>
                    <option value="Asia/Jakarta">Asia/Jakarta</option>
                    <option value="Asia/Jayapura">Asia/Jayapura</option>
                    <option value="Asia/Jerusalem">Asia/Jerusalem</option>
                    <option value="Asia/Kabul">Asia/Kabul</option>
                    <option value="Asia/Kamchatka">Asia/Kamchatka</option>
                    <option value="Asia/Karachi">Asia/Karachi</option>
                    <option value="Asia/Kathmandu">Asia/Kathmandu</option>
                    <option value="Asia/Khandyga">Asia/Khandyga</option>
                    <option value="Asia/Kolkata">Asia/Kolkata</option>
                    <option value="Asia/Krasnoyarsk">Asia/Krasnoyarsk</option>
                    <option value="Asia/Kuala_Lumpur">Asia/Kuala_Lumpur</option>
                    <option value="Asia/Kuching">Asia/Kuching</option>
                    <option value="Asia/Kuwait">Asia/Kuwait</option>
                    <option value="Asia/Macau">Asia/Macau</option>
                    <option value="Asia/Magadan">Asia/Magadan</option>
                    <option value="Asia/Makassar">Asia/Makassar</option>
                    <option value="Asia/Manila">Asia/Manila</option>
                    <option value="Asia/Muscat">Asia/Muscat</option>
                    <option value="Asia/Nicosia">Asia/Nicosia</option>
                    <option value="Asia/Novokuznetsk">Asia/Novokuznetsk</option>
                    <option value="Asia/Novosibirsk">Asia/Novosibirsk</option>
                    <option value="Asia/Omsk">Asia/Omsk</option>
                    <option value="Asia/Oral">Asia/Oral</option>
                    <option value="Asia/Phnom_Penh">Asia/Phnom_Penh</option>
                    <option value="Asia/Pontianak">Asia/Pontianak</option>
                    <option value="Asia/Pyongyang">Asia/Pyongyang</option>
                    <option value="Asia/Qatar">Asia/Qatar</option>
                    <option value="Asia/Qyzylorda">Asia/Qyzylorda</option>
                    <option value="Asia/Riyadh">Asia/Riyadh</option>
                    <option value="Asia/Sakhalin">Asia/Sakhalin</option>
                    <option value="Asia/Samarkand">Asia/Samarkand</option>
                    <option value="Asia/Seoul">Asia/Seoul</option>
                    <option value="Asia/Shanghai">Asia/Shanghai</option>
                    <option value="Asia/Singapore">Asia/Singapore</option>
                    <option value="Asia/Srednekolymsk">Asia/Srednekolymsk</option>
                    <option value="Asia/Taipei">Asia/Taipei</option>
                    <option value="Asia/Tashkent">Asia/Tashkent</option>
                    <option value="Asia/Tbilisi">Asia/Tbilisi</option>
                    <option value="Asia/Tehran">Asia/Tehran</option>
                    <option value="Asia/Thimphu">Asia/Thimphu</option>
                    <option value="Asia/Tokyo">Asia/Tokyo</option>
                    <option value="Asia/Tomsk">Asia/Tomsk</option>
                    <option value="Asia/Ulaanbaatar">Asia/Ulaanbaatar</option>
                    <option value="Asia/Urumqi">Asia/Urumqi</option>
                    <option value="Asia/Ust-Nera">Asia/Ust-Nera</option>
                    <option value="Asia/Vientiane">Asia/Vientiane</option>
                    <option value="Asia/Vladivostok">Asia/Vladivostok</option>
                    <option value="Asia/Yakutsk">Asia/Yakutsk</option>
                    <option value="Asia/Yangon">Asia/Yangon</option>
                    <option value="Asia/Yekaterinburg">Asia/Yekaterinburg</option>
                    <option value="Asia/Yerevan">Asia/Yerevan</option>
                    <option value="Atlantic/Azores">Atlantic/Azores</option>
                    <option value="Atlantic/Bermuda">Atlantic/Bermuda</option>
                    <option value="Atlantic/Canary">Atlantic/Canary</option>
                    <option value="Atlantic/Cape_Verde">Atlantic/Cape_Verde</option>
                    <option value="Atlantic/Faroe">Atlantic/Faroe</option>
                    <option value="Atlantic/Madeira">Atlantic/Madeira</option>
                    <option value="Atlantic/Reykjavik">Atlantic/Reykjavik</option>
                    <option value="Atlantic/South_Georgia">Atlantic/South_Georgia</option>
                    <option value="Atlantic/St_Helena">Atlantic/St_Helena</option>
                    <option value="Atlantic/Stanley">Atlantic/Stanley</option>
                    <option value="Australia/Adelaide">Australia/Adelaide</option>
                    <option value="Australia/Brisbane">Australia/Brisbane</option>
                    <option value="Australia/Broken_Hill">Australia/Broken_Hill</option>
                    <option value="Australia/Currie">Australia/Currie</option>
                    <option value="Australia/Darwin">Australia/Darwin</option>
                    <option value="Australia/Eucla">Australia/Eucla</option>
                    <option value="Australia/Hobart">Australia/Hobart</option>
                    <option value="Australia/Lindeman">Australia/Lindeman</option>
                    <option value="Australia/Lord_Howe">Australia/Lord_Howe</option>
                    <option value="Australia/Melbourne">Australia/Melbourne</option>
                    <option value="Australia/Perth">Australia/Perth</option>
                    <option value="Australia/Sydney">Australia/Sydney</option>
                    <option value="Europe/Amsterdam">Europe/Amsterdam</option>
                    <option value="Europe/Andorra">Europe/Andorra</option>
                    <option value="Europe/Astrakhan">Europe/Astrakhan</option>
                    <option value="Europe/Athens">Europe/Athens</option>
                    <option value="Europe/Belgrade">Europe/Belgrade</option>
                    <option value="Europe/Berlin">Europe/Berlin</option>
                    <option value="Europe/Bratislava">Europe/Bratislava</option>
                    <option value="Europe/Brussels">Europe/Brussels</option>
                    <option value="Europe/Bucharest">Europe/Bucharest</option>
                    <option value="Europe/Budapest">Europe/Budapest</option>
                    <option value="Europe/Busingen">Europe/Busingen</option>
                    <option value="Europe/Chisinau">Europe/Chisinau</option>
                    <option value="Europe/Copenhagen">Europe/Copenhagen</option>
                    <option value="Europe/Dublin">Europe/Dublin</option>
                    <option value="Europe/Gibraltar">Europe/Gibraltar</option>
                    <option value="Europe/Guernsey">Europe/Guernsey</option>
                    <option value="Europe/Helsinki">Europe/Helsinki</option>
                    <option value="Europe/Isle_of_Man">Europe/Isle_of_Man</option>
                    <option value="Europe/Istanbul">Europe/Istanbul</option>
                    <option value="Europe/Jersey">Europe/Jersey</option>
                    <option value="Europe/Kaliningrad">Europe/Kaliningrad</option>
                    <option value="Europe/Kiev">Europe/Kiev</option>
                    <option value="Europe/Kirov">Europe/Kirov</option>
                    <option value="Europe/Lisbon">Europe/Lisbon</option>
                    <option value="Europe/Ljubljana">Europe/Ljubljana</option>
                    <option value="Europe/London">Europe/London</option>
                    <option value="Europe/Luxembourg">Europe/Luxembourg</option>
                    <option value="Europe/Madrid">Europe/Madrid</option>
                    <option value="Europe/Malta">Europe/Malta</option>
                    <option value="Europe/Mariehamn">Europe/Mariehamn</option>
                    <option value="Europe/Minsk">Europe/Minsk</option>
                    <option value="Europe/Monaco">Europe/Monaco</option>
                    <option value="Europe/Moscow">Europe/Moscow</option>
                    <option value="Europe/Oslo">Europe/Oslo</option>
                    <option value="Europe/Paris">Europe/Paris</option>
                    <option value="Europe/Podgorica">Europe/Podgorica</option>
                    <option value="Europe/Prague">Europe/Prague</option>
                    <option value="Europe/Riga">Europe/Riga</option>
                    <option value="Europe/Rome">Europe/Rome</option>
                    <option value="Europe/Samara">Europe/Samara</option>
                    <option value="Europe/San_Marino">Europe/San_Marino</option>
                    <option value="Europe/Sarajevo">Europe/Sarajevo</option>
                    <option value="Europe/Saratov">Europe/Saratov</option>
                    <option value="Europe/Simferopol">Europe/Simferopol</option>
                    <option value="Europe/Skopje">Europe/Skopje</option>
                    <option value="Europe/Sofia">Europe/Sofia</option>
                    <option value="Europe/Stockholm">Europe/Stockholm</option>
                    <option value="Europe/Tallinn">Europe/Tallinn</option>
                    <option value="Europe/Tirane">Europe/Tirane</option>
                    <option value="Europe/Ulyanovsk">Europe/Ulyanovsk</option>
                    <option value="Europe/Uzhgorod">Europe/Uzhgorod</option>
                    <option value="Europe/Vaduz">Europe/Vaduz</option>
                    <option value="Europe/Vatican">Europe/Vatican</option>
                    <option value="Europe/Vienna">Europe/Vienna</option>
                    <option value="Europe/Vilnius">Europe/Vilnius</option>
                    <option value="Europe/Volgograd">Europe/Volgograd</option>
                    <option value="Europe/Warsaw">Europe/Warsaw</option>
                    <option value="Europe/Zagreb">Europe/Zagreb</option>
                    <option value="Europe/Zaporozhye">Europe/Zaporozhye</option>
                    <option value="Europe/Zurich">Europe/Zurich</option>
                    <option value="Indian/Antananarivo">Indian/Antananarivo</option>
                    <option value="Indian/Chagos">Indian/Chagos</option>
                    <option value="Indian/Christmas">Indian/Christmas</option>
                    <option value="Indian/Cocos">Indian/Cocos</option>
                    <option value="Indian/Comoro">Indian/Comoro</option>
                    <option value="Indian/Kerguelen">Indian/Kerguelen</option>
                    <option value="Indian/Mahe">Indian/Mahe</option>
                    <option value="Indian/Maldives">Indian/Maldives</option>
                    <option value="Indian/Mauritius">Indian/Mauritius</option>
                    <option value="Indian/Mayotte">Indian/Mayotte</option>
                    <option value="Indian/Reunion">Indian/Reunion</option>
                    <option value="Pacific/Apia">Pacific/Apia</option>
                    <option value="Pacific/Auckland">Pacific/Auckland</option>
                    <option value="Pacific/Bougainville">Pacific/Bougainville</option>
                    <option value="Pacific/Chatham">Pacific/Chatham</option>
                    <option value="Pacific/Chuuk">Pacific/Chuuk</option>
                    <option value="Pacific/Easter">Pacific/Easter</option>
                    <option value="Pacific/Efate">Pacific/Efate</option>
                    <option value="Pacific/Enderbury">Pacific/Enderbury</option>
                    <option value="Pacific/Fakaofo">Pacific/Fakaofo</option>
                    <option value="Pacific/Fiji">Pacific/Fiji</option>
                    <option value="Pacific/Funafuti">Pacific/Funafuti</option>
                    <option value="Pacific/Galapagos">Pacific/Galapagos</option>
                    <option value="Pacific/Gambier">Pacific/Gambier</option>
                    <option value="Pacific/Guadalcanal">Pacific/Guadalcanal</option>
                    <option value="Pacific/Guam">Pacific/Guam</option>
                    <option value="Pacific/Honolulu">Pacific/Honolulu</option>
                    <option value="Pacific/Kiritimati">Pacific/Kiritimati</option>
                    <option value="Pacific/Kosrae">Pacific/Kosrae</option>
                    <option value="Pacific/Kwajalein">Pacific/Kwajalein</option>
                    <option value="Pacific/Majuro">Pacific/Majuro</option>
                    <option value="Pacific/Marquesas">Pacific/Marquesas</option>
                    <option value="Pacific/Midway">Pacific/Midway</option>
                    <option value="Pacific/Nauru">Pacific/Nauru</option>
                    <option value="Pacific/Niue">Pacific/Niue</option>
                    <option value="Pacific/Norfolk">Pacific/Norfolk</option>
                    <option value="Pacific/Noumea">Pacific/Noumea</option>
                    <option value="Pacific/Pago_Pago">Pacific/Pago_Pago</option>
                    <option value="Pacific/Palau">Pacific/Palau</option>
                    <option value="Pacific/Pitcairn">Pacific/Pitcairn</option>
                    <option value="Pacific/Pohnpei">Pacific/Pohnpei</option>
                    <option value="Pacific/Port_Moresby">Pacific/Port_Moresby</option>
                    <option value="Pacific/Rarotonga">Pacific/Rarotonga</option>
                    <option value="Pacific/Saipan">Pacific/Saipan</option>
                    <option value="Pacific/Tahiti">Pacific/Tahiti</option>
                    <option value="Pacific/Tarawa">Pacific/Tarawa</option>
                    <option value="Pacific/Tongatapu">Pacific/Tongatapu</option>
                    <option value="Pacific/Wake">Pacific/Wake</option>
                    <option value="Pacific/Wallis">Pacific/Wallis</option>
                    <option value="UTC">UTC</option>
                </select>
            </div>


            <div style="margin-top: 80px">
                <label for="formTitle">E-mail адрес</label>
                <input type="text" name="email" class="input-form" required value="<?= $sites->email ?>">
            </div>
            <div style="margin-top: 20px">
                <label for="formTitle">Favicon</label><br>

                <?php if (strlen($sites->favicon) <= 0){
                    printf('<input name="file" type="file" accept="image/*" multiple="multiple">
                <div class="ajax-reply hide"></div>');
                } else {
                    printf('<a href="/flexcat/admin/sites/favdelete/%s" class="delete-favicon"><i class="icofont-close-circled"></i></a> 
                    <img src="/upload/img/sites/%s" class="favicon-load">',$sites->id, $sites->favicon);
                }
                ?>



                <input type="hidden" name="favicon" id="favicons" value="<?= $sites->favicon ?>">
            </div>

        </div>

        <div class="tab-panel" id="tab-2">
            <fieldset style="padding: 20px">
                <legend>Выберите шаблон:</legend>
                <label for="radio-1">Default<br><br>
                    <img src="/upload/img/sites/flexcats.png" alt="logo" class="img-site-theme">
                </label>
                <input type="radio" name="template" id="radio-1" value="default" <?php
                if ($sites->template == '0'): ?>checked<?php endif; ?>>
                <br><br>
                <?php
                $i = 1;
                foreach ($templates as $template) {
                    $i++;


                    if ($sites->template == $template->id) {
                        $cheked = ' checked';
                    } else {
                        $cheked = '';
                    }

                    printf('
                <label for="radio-%s">%s<br><br>
                    <img src="/upload/img/sites/%s" alt="logo" class="img-site-theme">
                </label>
                <input type="radio" name="template" id="radio-%s" value="%s" %s><br><br>',
                        $i, $template->title, $template->img, $i, $template->id, $cheked);
                }
                ?>
            </fieldset>

        </div>

        <div class="tab-panel" id="tab-3">

            <label for="formTitle" class="tooltip"
                   title="Страница, отображаемая при возникновении 404 ошибки (страница не найдена),
                    если страница не указана - при возникновении 404 ошибки производится
                    редирект на главную страницу">Страница для "Ошибка 404" (страница не найдена)</label><br>
            <select name="page404" id="Page404">
                <option value="0">...</option>
                <?php foreach ($pages as $page): ?>
                    <option value="<?= $page['id'] ?>" <?php
                    if ($page['id'] == $sites->page404): ?>selected<?php endif; ?>><?= $page['title'] ?></option>
                <? endforeach; ?>
            </select>

            <br><br>
            <label for="formTitle" class="tooltip" title="Страница, отображаемая при попытке доступа
            к разделу сайта пользователем, не имеющим права доступа">Страница для "Ошибка 403" (доступ запрещен)</label>
            <br>
            <select name="page403" id="Page403">
                <option value="0">...</option>
                <?php foreach ($pages as $page): ?>
                    <option value="<?= $page['id'] ?>" <?php
                    if ($page['id'] == $sites->page403): ?>selected<?php endif; ?>><?= $page['title'] ?></option>
                <? endforeach; ?>
            </select>

            <br><br>
            <label for="formTitle" class="tooltip" title="Страница, отображаемая
            если сайт отключен администратором">Страница, отображаемая при отключении сайта</label><br>
            <select name="page_off" id="PageOff">
                <option value="0">...</option>
                <?php foreach ($pages as $page): ?>
                    <option value="<?= $page['id'] ?>" <?php
                    if ($page['id'] == $sites->page_off): ?>selected<?php endif; ?>><?= $page['title'] ?></option>
                <? endforeach; ?>
            </select>
        </div>

        <div class="tab-panel" id="tab-4">
            <label for="formTitle" class="tooltip"
                   title="Содержимое файла robots.txt  для данного сайта">robots.txt</label>
            <textarea name="robots" class="form-control" cols="5" rows="250" style="height: 340px;"><?= $sites->robots ?></textarea>
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/sites/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.switch-ios.js"></script>

<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>
<?php $this->theme->footer(); ?>

