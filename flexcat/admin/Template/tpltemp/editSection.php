<?php $this->theme->header();
//
//$this->theme->block('header');
?>
<?php $this->theme->block('sidebar'); ?>

<div class="page-title-block">
    <a href="/flexcat/admin/tpltemp/"><i class="icofont-light-bulb"></i>TPL-шаблоны</a>
    <span> / </span>
    <i class="icofont-ui-folder"></i>Редактирование раздела TPL-шаблонов
</div>
<div class="container-content">

    <form action="/flexcat/admin/tpltemp/section/edit/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела</label>
            <input type="text" name="title" class="input-form" value="<?= $sectionsEdit->title ?>">
            <br>




            <br>
            <label for="formTitle">Родительский раздел</label> <br>
            <select name="parent"  class="select">
                <option value="0">...</option>
                <?php foreach ($sections as $section): ?>
                    <?php if ($section['id'] != $sectionsEdit->id): ?>
                    <option value="<?= $section['id'] ?>" <?php
                    if (isset($section_id) && $section_id > 0 && $section['id'] == $sectionsEdit->parent):
                    ?> selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                    <?php endif; ?>
                <?php endforeach; ?>
            </select>


            <input type="hidden" name="site" value="<?= $sites[0]['id'] ?>">
            <input type="hidden" name="sections_id" value="<?= $sectionsEdit->id ?>">

        </div>




        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<!--<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>-->


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

