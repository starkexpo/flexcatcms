<?php $this->theme->header();
//
//$this->theme->block('header');
?>
<?php $this->theme->block('sidebar'); ?>

<div class="page-title-block">
    <a href="/flexcat/admin/tpltemp/"><i class="icofont-light-bulb"></i>TPL-шаблоны</a>
    <span> / </span>
    Создание TPL-шаблона
</div>
<div class="container-content">

    <form action="/flexcat/admin/tpltemp/tpl/create/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li><a href="#tab-1" class="tab-control">Основные</a></li>
            <li class="active-tabs"><a href="#tab-2" class="tab-control">Код</a></li>
        </ul>

        <div class="tab-panel" id="tab-1">
            <label for="formTitle">Название TPL-шаблона</label>
            <input type="text" name="title" class="input-form">
            <br>




            <br>
            <label for="formTitle">Раздел TPL-шаблона</label> <br>
            <select name="parent"  class="select">
                <option value="0" selected="selected">...</option>
                <?php foreach ($sections as $section): ?>
                    <option value="<?= $section['id'] ?>" <?php
                    if (isset($section_id) && $section_id > 0 && $section['id'] == $section_id):
                    ?> selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                <?php endforeach; ?>
            </select>


            <input type="hidden" name="site" value="<?= $sites[0]['id'] ?>">
        </div>

        <div class="tab-panel active-tab" id="tab-2">
            <label for="formTitle" class="tooltip" title="Код макета">Код TPL-шаблона</label> <br>
            <textarea cols="140" name="code" rows="30"  id="layoutCode"></textarea>
        </div>




        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<!--<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>-->



<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.js"></script>
<link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.css">
<link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.css">

<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/css/css.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/javascript/javascript.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/clike/clike.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/php/php.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/xml/xml.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/smarty/smarty.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/selection/active-line.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/search.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/searchcursor.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.js"></script>


<script src="/flexcat/admin/Assets/js/Plugin/Tpltemp/tpltemp.js"></script>

<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

