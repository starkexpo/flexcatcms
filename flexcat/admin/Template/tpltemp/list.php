<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="page-title-block">
    <a href="/flexcat/admin/tpltemp/"><i class="icofont-light-bulb"></i>TPL-шаблоны</a>
</div>
<div class="container-content">

    <div class="container-bar-buttons">
        <div class="group-but-sub">
            <a href="#" class="btn btn-green button-group" data-color="green_group"><!--show-button-group-->
                <i class="icofont-code-alt"></i>TPL-шаблон
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="button-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/tpltemp/tpl/create/<?php
                    if (isset($section_id)) {
                        echo $section_id;
                    }
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

        <div class="group-but-sub">
            <a href="#" class="btn btn-blue button-group" data-color="blue_group"><!--show-button-group-->
                <i class="icofont-ui-folder"></i>Раздел TPL
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="button-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/tpltemp/section/create/<?php
                    if (isset($section_id)) {
                        echo $section_id;
                    }
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

    </div>



    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th>Сайт</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($tpl_temp as $tpl): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $tpl['id'] ?>" class="check-item"></th>
                <th><?= $tpl['id'] ?></th>
                <th width="22" align="center">
                    <?php if ($tpl['type'] == "section"): ?>
                        <i class="icofont-ui-folder" style="font-size: 16px"></i>
                    <?php endif; ?>

                    <?php if ($tpl['type'] == "tpl"): ?>
                        <i class="icofont-light-bulb"></i>
                    <?php endif; ?>
                </th>
                <th>
                    <?php if ($tpl['type'] == "section"): ?>
                        <a href="/flexcat/admin/tpltemp/<?= $tpl['id'] ?>"
                           class="title-item layouts-title"><?= $tpl['title'] ?></a>
                    <?php endif; ?>

                    <?php if ($tpl['type'] == "tpl"): ?>
                        <?= $tpl['title'] ?>
                    <?php endif; ?>
                </th>
                <td>
                    <?php
                    foreach ($sites as $site) {
                        if ($tpl['site'] == $site->id) {
                            echo $site->title;
                        }
                    }
                    ?>
                    <?/*= $layout['site'] */?>
                </td>

                <td>
                    <div class="btn-mini-group">
                        <a href="<?php
                        if ($tpl['type'] == "section") {
                            echo '/flexcat/admin/Tpltemp/section/edit/' . $tpl['id'];
                        } else {
                            echo '/flexcat/admin/Tpltemp/tpl/edit/' . $tpl['id'];
                        }

                        ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/sites/edit/<? /*= $site->id */ ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
