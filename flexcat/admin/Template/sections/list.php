<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <span> / </span>
        <a href="/flexcat/admin/layouts/maket/section/<?= $now_id[0]['id'] ?>">Секции макета "<?= $now_id[0]['title'] ?>"
        </a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">


    <div class="container-bar-buttons">
        <a href="/flexcat/admin/layouts/maket/section/create/<?= $now_id[0]['id'] ?>" class="btn btn-green btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>Добавить</a>
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="40%">Название</th>
            <th width="40%">Псевдоним</th>
            <th width="10" align="center"><i class="icofont-layers tooltip" style="font-size: 25px"
                                             title="Виджеты секции"></i></th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($makets as $maket): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $maket['id'] ?>" class="check-item"></th>
                <th><?= $maket['id'] ?></th>
                <th style="background-color: hsla(0, 0%, 80%, 0.3);">
                    <?= $maket['title'] ?>
                </th>
                <td>
                    <?= $maket['alias'] ?>
                </td>
                <td>
                    <a href="/flexcat/admin/layouts/maket/widget/<?= $maket['id'] ?>" class="tooltip sectionButton"
                       title="Виджеты секции">
                        <i class="icofont-layers" style="font-size: 25px"></i>
                        <?php
                        $countWidget = 0;
                        foreach ($widgetList as $item) {
                            if ($item['sections_id'] == $maket['id']) {
                                $countWidget++;
                            }
                        }
                        ?>
                        <?php if ($countWidget > 0): ?>
                        <span class="babgers-icons badge-azure"><?= $countWidget ?></span>
                        <?php endif; ?>
                    </a>
                </td>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/layouts/maket/section/edit/<?= $maket['id'] ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/layouts/maket/section/delete/<?= $maket['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
