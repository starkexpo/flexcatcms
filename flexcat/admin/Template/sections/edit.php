<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/layouts/"><i class="icofont-layout"></i>Макеты сайта</a>
        <span> / </span>
        <!-- <a href="/flexcat/admin/layouts/maket/section/<?/*//= $now_id[0]['id'] */?>">Секции макета "<?/*//= $now_id[0]['title'] */?>"
    </a>
    <span> / </span>-->
        Редактирование секции макета
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/layouts/maket/section/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Дополнительные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела макетов</label>
            <input type="text" name="title" class="input-form" value="<?= $nowEdit[0]['title'] ?>">
            <br>


            <label for="formTitle">Псевдоним (латинскими буквами)</label>
            <input type="text" name="alias" class="input-form" style="width: 300px;" value="<?= $nowEdit[0]['alias'] ?>" disabled>

        </div>

        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Идентификатор макета</label>
            <input type="text" name="layouts_id" class="input-form" value="<?= $nowEdit[0]['layouts_id'] ?>" readonly>
        </div>

        <input type="hidden" name="sites" value="<?//= $sites[0]['id'] ?>">
        <input type="hidden" name="sections_id" value="<?= $nowEdit[0]['id'] ?>">


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/layouts/maket/section/<?//= $now_id[0]['id'] ?>" class="btn btn-red"
               id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<!--<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>-->


<?php $this->theme->footer(); ?>

