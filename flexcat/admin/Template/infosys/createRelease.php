<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/infosys/"><i class="icofont-newspaper"></i>Информационные системы</a>
        <span> / </span>
        <a href="/flexcat/admin/infosys/info/<?= $now_id[0]['id'] ?>"><?= $now_id[0]['title'] ?></a>
        <span> / </span>
        Создание выпуска
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <form action="/flexcat/admin/infosys/info/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Описание</a></li>
            <li><a href="#tab-3" class="tab-control">Публикация</a></li>
            <li><a href="#tab-4" class="tab-control">SEO</a></li>
            <li><a href="#tab-5" class="tab-control">Дополнительно</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название информационного выпуска</label>
            <input type="text" name="title" class="input-form">


            <br>
            <label for="formTitle">Информационная система</label><br>
            <select name="section" id="sectionPage" class="select">
                <?php foreach ($sections as $section): ?>
                    <option value="<?= $section['id'] ?>" <?php
                    if (isset($now_id) && $now_id[0]['id'] == $section['id']):
                    ?>selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                <?php endforeach; ?>
            </select>


            <br><br>
            <label for="formTitle">Изображение</label><br>
            <input name="img_big" type="file" accept="image/*" data-size="big">
            <div class="ajax-reply hide"></div>
            <input type="hidden" name="image_big" id="bigIMG">


        </div>

        <div class="tab-panel" id="tab-2">
            <label for="formContent">Краткое описание иформационного выпуска</label><br>
            <textarea name="description" class="input-form" style="height: 200px;"></textarea>

            <br>
            <label for="formContent">Подробное описание иформационного выпуска</label><br>
            <textarea id="content" name="text"></textarea>
        </div>

        <div class="tab-panel" id="tab-3">
            <label for="formTitle">Алиас названия информационного выпуска</label>
            <input type="text" name="alias" class="input-form" placeholder="алиас заголовка">
            <br>
            <label for="formTitle">Дата</label>
            <input type="text" name="date" class="input-form" placeholder="возможно не указывать" style="width: 300px;">


            <br>
            <label for="formTitle">Дата публикации</label>
            <input type="text" name="date_public" class="input-form"  style="width: 300px;">

            <br>
            <label for="formTitle">Дата завершения публикации</label>
            <input type="text" name="date_delpublic" class="input-form" style="width: 300px;">


            <br>
            <label class="label-page">Статус публикации</label>
            <input type="checkbox" name="published" class="check-switch-ios" value="enable" checked/>
        </div>

        <div class="tab-panel" id="tab-4">
            <label for="formTitle">Заголовок страницы [Title]</label><br>
            <textarea name="meta_title"
                      class="input-form seo-input"></textarea>

            <label for="formTitle">Описание страницы [Description]</label><br>
            <textarea name="meta_description"
                      class="input-form seo-input"></textarea>

            <label for="formTitle">Ключевые слова [Keywords]</label><br>
            <textarea name="meta_keywords"
                      class="input-form seo-input"></textarea>
        </div>

        <div class="tab-panel" id="tab-5">
            <label for="formTitle">Ссылка на загрузку файла</label>
            <input type="text" name="options[link]" class="input-form">
            <br>
            <label for="formTitle">Просмотров</label>
            <input type="text" name="options[views]" class="input-form onlyNum" placeholder="0">
            <br>
            <label for="formTitle">Скачиваний</label>
            <input type="text" name="options[downloads]" class="input-form onlyNum" placeholder="0">
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/infosys/info/<?= $now_id[0]['id'] ?>" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.switch-ios.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/jquery-1.7.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.css"/>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.js"></script>-->
<script src="/flexcat/admin/Assets/js/Plugin/Infosys/infosys.js"></script>


<?php $this->theme->footer(); ?>

