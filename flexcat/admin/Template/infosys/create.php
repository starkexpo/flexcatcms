<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/infosys/"><i class="icofont-newspaper"></i>Информационные системы</a>
        <span> / </span>
        Создание системы
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/infosys/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Сортировка</a></li>
            <li><a href="#tab-3" class="tab-control">Публикация</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название информационной системы</label>
            <input type="text" name="title" class="input-form" placeholder="Новости">
            <!--<br>
            <label for="formContent">Описание информационной системы</label><br>
            <textarea id="content" name="description"></textarea>-->

            <br>
            <div style="margin-top: -10px">
                <label for="formTitle">Тип формирования URL</label><br>
                <select name="type_url"  class="select">
                    <option value="id" selected="selected">Идентификатор</option>
                    <option value="alias">Транслитерация</option>
                </select>
            </div>


            <label for="formTitle" style="float: right; margin-top: -50px; margin-right: 200px">Раздел</label>
            <div style="float: right; margin-top: -20px">
                <select name="section" id="sectionPage" class="select">
                    <option value="0">...</option>
                    <?php foreach ($sections as $section): ?>
                        <option value="<?= $section['id'] ?>" <?php
                        if (isset($now_section) && $now_section == $section['id']):
                        ?>selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

        </div>

        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Поле сортировки элементов</label><br>
            <select name="sort_field"  class="select">
                <option value="date" selected="selected">Дата</option>
                <option value="title">Название</option>
                <option value="order">Порядок сортировки</option>
            </select>

            <br><br><br>

            <label for="sendForm">Направление сортировки элементов</label><br>
            <select name="sort_direction"  class="select">
                <option value="asc" selected="selected">По возрастанию</option>
                <option value="desc">По убыванию</option>
            </select>
        </div>

        <div class="tab-panel" id="tab-3">
            <label for="formTitle">Алиас заголовока</label>
            <input type="text" name="alias" class="input-form" placeholder="алиас заголовка">
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/infosys/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/jquery-1.7.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.css"/>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.js"></script>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Page/page.js"></script>-->

<!--<script src="/flexcat/admin/Assets/js/jquery/jquery-3.3.1.min.js"></script>-->
<!--<script src="/flexcat/admin/Assets/js/jquery/jquery-ui.min.js"></script>-->

<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

