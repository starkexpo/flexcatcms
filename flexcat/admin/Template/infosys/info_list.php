<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/infosys/"><i class="icofont-newspaper"></i>Информационные системы</a>
        <span>/</span>
        <a href="/flexcat/admin/infosys/info/<?= $now_id[0]['id'] ?>"><?= $now_id[0]['title'] ?></a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <div class="container-bar-buttons">

        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-newspaper"></i>Информационный выпуск
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/infosys/info/create/<?php if (isset($now_id)) {
                        echo $now_id[0]['id'];
                    } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th width="180">Дата</th>
<!--            <th width="20"><i class="icofont-speech-comments" style="font-size: 18px;"></i></th>-->
            <th width="10" align="center"><i class="icofont-light-bulb public-structure" style="font-size: 18px !important;"></i></th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($viewinfo as $info): ?>
            <tr>
                <th><input type="checkbox" name="info<?= $info['id'] ?>" class="check-item"></th>
                <th><?= $info['id'] ?></th>
                <th>
                    <i class="icofont-paper" style="font-size: 16px;"></i>
                </th>
                <td>
                    <?= $info['title'] ?>
                </td>
                <td>
                    <?= $info['date'] ?>
                </td>
                <!--<td>
                    <a href="#" class="view-comments">
                        <i class="icofont-speech-comments" style="font-size: 16px;"></i>
                    </a>
                </td>-->
                <td>
                    <a href="/flexcat/admin/infosys/info/setpublic/<?= $info['id'] ?>" class="published-button <?php
                    if ($info['published'] == 'disabled') {
                        echo ' public-off';
                    }
                    ?>">
                        <i class="icofont-light-bulb public-structure" style="font-size: 16px !important;"></i>
                    </a>
                </td>
                <td>
                    <div class="btn-mini-group" style="justify-content: flex-start;">
                        <a href="/flexcat/admin/infosys/info/edit/<?= $info['id'] ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>

                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <?php if ($info['type'] == 'info'): ?>
                            <a href="#" class="delete-but btn-mini-blue">
                                <i class="icofont-ui-copy"></i>
                            </a>
                        <?php endif; ?>
                        <a href="/flexcat/admin/infosys/info/delete/<?= $info['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


    <script src="/flexcat/admin/Assets/js/Plugin/Sites/jquery.publicButton.js"></script>

</div>
<?php $this->theme->footer(); ?>
