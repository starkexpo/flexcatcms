<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/infosys/"><i class="icofont-newspaper"></i>Информационные системы</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">


    <div class="container-bar-buttons">
        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar"><!--show-button-group-->
                <i class="icofont-newspaper"></i>Информационная система
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/infosys/create/<?php if (isset($now_id)) {
                        echo $now_id[0]['id'];
                    } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

        <div class="group-but-sub hide" <?php if (isset($now_id) && $now_id[0]['type'] == "layouts") {
            echo ' style="display: none;" ';
        } ?>>
            <a href="#" class="btn btn-blue button-group" data-color="blue_group"><!--show-button-group-->
                <i class="icofont-ui-folder"></i>Раздел
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="button-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/infosys/section/create/<?php if (isset($now_id) && $now_id[0]['type'] == "section") {
                        echo $now_id[0]['id'];
                    } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

    </div>

    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($infosys as $info): ?>
            <tr>
                <th><input type="checkbox" name="info<?= $info['id'] ?>" class="check-item"></th>
                <th><?= $info['id'] ?></th>
                <th>
                    <?php if ($info['type'] == 'section'): ?>
                        <i class="icofont-ui-folder" style="font-size: 16px"></i>
                    <?php endif; ?>

                    <?php if ($info['type'] == 'info'): ?>
                        <i class="icofont-newspaper" style="font-size: 18px"></i>
                    <?php endif; ?>
                </th>
                <td>
                    <?php if ($info['type'] == 'section'): ?>
                        <a href="/flexcat/admin/infosys/section/<?= $info['id'] ?>" class="title-item layouts-title">
                            <?= $info['title'] ?>
                        </a>
                    <?php endif; ?>

                    <?php if ($info['type'] == 'info'): ?>
                        <a href="/flexcat/admin/infosys/info/<?= $info['id'] ?>" class="title-item layouts-title">
                            <?= $info['title'] ?>
                        </a><span class="count-info"><i class="icofont-paper"></i><?php
                            $i = 0;
                            foreach ($infosysList as $item) {
                                if ($info['id'] == $item['section']) {
                                    $i++;
                                }
                            }
                            echo $i;
                            ?></span>
                    <?php endif; ?>
                </td>
                <td>
                    <div class="btn-mini-group" style="justify-content: flex-start;">
                        <a href="<?php
                        if ($info['type'] == 'info') {
                            echo '/flexcat/admin/infosys/edit/' . $info['id'];
                        } else {
                            echo '/flexcat/admin/infosys/section/edit/' . $info['id'];
                        }
                        ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <?php if ($info['type'] == 'info'): ?>
                            <a href="#" class="delete-but btn-mini-blue">
                                <i class="icofont-ui-copy"></i>
                            </a>
                        <?php endif; ?>
                        <a href="/flexcat/admin/infosys/info/delete/<?= $info['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
<?php $this->theme->footer(); ?>
