<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/user/"><i class="icofont-user"></i>Сотрудники</a>
        <span> / </span>
        Редактирование информации о сотруднике
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">
    <form action="/flexcat/admin/user/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Личные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Логин</label>
            <input type="text" name="login" class="input-form" value="<?= $user[0]['login'] ?>">
            <br>

            <div class="block-item-create">
                <div class="block-create-for-admin">

                    <label for="formTitle">Пароль</label>
                    <input type="text" name="password" class="input-form" placeholder="Минимум 9 символов">
                </div>

                <div class="block-create-for-admin">

                    <label for="formTitle">Подтверждение пароля</label>
                    <input type="text" name="password2" class="input-form" placeholder="Минимум 9 символов">
                </div>

                <input type="hidden" name="user_id" value="<?= $user[0]['id'] ?>">

            </div>

            <br>
            <label for="formTitle">Отдел пользователя</label> <br>
            <select name="role" id="siteLayout" class="select">
                <option value="admin"
                    <?php if ($user[0]['role'] == 'admin'): ?> selected="selected" <?php endif; ?>>Администратор</option>
                <option value="moderator"
                    <?php if ($user[0]['role'] == 'moderator'): ?> selected="selected" <?php endif; ?>>Модератор</option>
                <option value="user"
                    <?php if ($user[0]['role'] == 'user'): ?> selected="selected" <?php endif; ?>>Пользователь</option>
            </select>
        </div>


        <div class="tab-panel" id="tab-2">
            <div class="block-user-create">
                <div class="block-create-user-admin">
                    <label for="formTitle">Фамилия</label>
                    <input type="text" name="lastname" class="input-form" value="<?= $user[0]['lastname'] ?>">
                </div>
                <div class="block-create-user-admin">
                    <label for="formTitle">Имя</label>
                    <input type="text" name="firstname" class="input-form" value="<?= $user[0]['firstname'] ?>">
                </div>
                <div class="block-create-user-admin">
                    <label for="formTitle">Отчество</label>
                    <input type="text" name="patronymic" class="input-form" value="<?= $user[0]['patronymic'] ?>">
                </div>
            </div>

            <br>
            <label for="formTitle">Адрес</label>
            <input type="text" name="adress" class="input-form" value="<?= $user[0]['adress'] ?>">

            <br>
            <label for="formTitle">Фотография</label><br>
            <?php if (strlen($user[0]['photo']) <= 0): ?>
            <input name="file" type="file" accept="image/*" multiple="multiple">
            <div class="ajax-reply hide"></div>
            <?php endif; ?>

            <?php if (strlen($user[0]['photo']) > 0): ?>
                <a href="/flexcat/admin/user/avatardelete/<?= $user[0]['id'] ?>" class="delete-favicon">
                    <i class="icofont-close-circled"></i>
                </a>
                <img src="/upload/img/avatar/<?= $user[0]['photo'] ?>" class="favicon-load">
            <?php endif; ?>

            <input type="hidden" name="favicon" id="favicons" value="<?= $user[0]['photo'] ?>">



        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/user/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/User/user.js"></script>


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

