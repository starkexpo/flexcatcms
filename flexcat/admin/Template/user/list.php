<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

    <div class="container-pages-title">
        <div>
            <a href="/flexcat/admin/user/"><i class="icofont-user"></i>Сотрудники</a>
        </div>
        <?php $this->theme->block('controlPanel'); ?>
    </div>


    <div class="container-content">


        <div class="container-bar-buttons">
            <a href="/flexcat/admin/user/create/" class="btn btn-green btn-bar m-r-1">
                <i class="icofont-plus" style="font-size: 14px"></i>Добавить
            </a>

            <!--<a href="/flexcat/admin/user/papers/" class="btn btn-blue btn-bar">
                <i class="icofont-image"></i></i>Обои
            </a>-->
        </div>


        <table class="table-list">
            <thead>
            <tr>
                <th width="22">&nbsp;</th>
                <th width="22">Код</th>
                <th width="80" align="center"><i class="icofont-navigation-menu"></i></th>
                <th width="40%">Полное имя</th>
                <th width="40%">Логин</th>
                <th width="15%">Отдел</th>
                <th width="100"></th>
            </tr>
            </thead>

            <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <th><input type="checkbox" name="layout<?= $user['id'] ?>" class="check-item"></th>
                    <th><?= $user['id'] ?></th>
                    <th align="center">
                        <?php if (strlen($user['photo']) > 0): ?>
                            <img src="/upload/img/avatar/<?= $user['photo'] ?>" class="admin-avatar">
                        <?php endif; ?>
                    </th>
                    <th><?= $user['lastname'] . " " . $user['firstname'] . " " . $user['patronymic']; ?></th>
                    <td><?= $user['login'] ?></td>
                    <td><?= $user['role'] ?></td>
                    <td>
                        <div class="btn-mini-group">
                            <a href="/flexcat/admin/user/edit/<?= $user['id'] ?>" class="edit-but btn-mini-green">
                                <i class="icofont-ui-edit"></i>
                            </a>
                            <a href="/flexcat/admin/user/delete/<?= $user['id'] ?>" class="delete-but btn-mini-red">
                                <i class="icofont-bin"></i>
                            </a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

        <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

    </div>
<?php $this->theme->footer(); ?>