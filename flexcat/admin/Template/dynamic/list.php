<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/dynamic/"><i class="icofont-briefcase"></i>Динамические страницы</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <div class="container-bar-buttons">
        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-code-alt"></i>Страница
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/dynamic/create/<?php
                    if (isset($section_id)) {
                        echo $section_id;
                    }
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

        <div class="group-but-sub m-r-1 group-blue">
            <a href="#" class="btn btn-blue btn-group btn-bar" data-color="blue_group"><!--show-button-group-->
                <i class="icofont-ui-folder"></i>Раздел
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/dynamic/section/create/<?php
                    if (isset($section_id)) {
                        echo $section_id;
                    }
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

    </div>



    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th>Описание</th>
            <th width="20"><i class="icofont-listine-dots tooltip" title="Параметры"></i></th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($dynamics as $dynamic): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $dynamic['id'] ?>" class="check-item"></th>
                <th><?= $dynamic['id'] ?></th>
                <th width="22" align="center">
                    <?php if ($dynamic['type'] == "section"): ?>
                        <i class="icofont-ui-folder" style="font-size: 16px"></i>
                    <?php endif; ?>

                    <?php if ($dynamic['type'] == "template"): ?>
                        <i class="icofont-code-alt"></i>
                    <?php endif; ?>
                </th>
                <th>
                    <?php if ($dynamic['type'] == "section"): ?>
                        <a href="/flexcat/admin/dynamic/<?= $dynamic['id'] ?>"
                           class="title-item layouts-title"><?= $dynamic['title'] ?></a>
                    <?php endif; ?>

                    <?php if ($dynamic['type'] == "template"): ?>
                        <?= $dynamic['title'] ?>
                    <?php endif; ?>
                </th>
                <td>
                    <?= $dynamic['description'] ?>
                </td>
                <td>
                    <?php if ($dynamic['type'] == "template"): ?>
                        <a href="/flexcat/admin/dynamic/parameters/<?= $dynamic['id'] ?>" class="public-structure">
                            <i class="icofont-listine-dots dynamic-ico-option"></i>
                        </a>
                    <?php endif; ?>
                </td>
                <td>
                    <div class="btn-mini-group">
                        <a href="<?php
                        if ($dynamic['type'] == "section") {
                            echo '/flexcat/admin/dynamic/section/edit/' . $dynamic['id'];
                        } else {
                            echo '/flexcat/admin/dynamic/edit/' . $dynamic['id'];
                        }

                        ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="<?php
                        if ($dynamic['type'] == "section") {
                            echo '/flexcat/admin/dynamic/section/delete/' . $dynamic['id'];
                        } else {
                            echo '/flexcat/admin/dynamic/delete/' . $dynamic['id'];
                        }

                        ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
