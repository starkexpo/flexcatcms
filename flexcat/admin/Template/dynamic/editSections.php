<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/dynamic/"><i class="icofont-briefcase"></i>Динамические страницы</a>
        <span> / </span>
        <i class="icofont-ui-folder"></i>Создание раздела динамических страниц
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">

    <form action="/flexcat/admin/dynamic/section/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела</label>
            <input type="text" name="title" class="input-form" value="<?= $sectionsEdit->title ?>">
            <br>


            <br>
            <label for="formTitle">Родительский раздел макетов</label> <br>
            <select name="parent" id="siteLayout" class="select">
                <option value="0" selected="selected">...</option>
                <?php foreach ($sections as $section): ?>
                    <?php if ($section['id'] != $sectionsEdit->id) ?>
                        <?php if ($section['id'] != $sectionsEdit->id): ?>
                    <option value="<?= $section['id'] ?>" <?php
                    if ($section['id'] == $sectionsEdit->id):
                        ?> selected="selected" <?php endif; ?> ><?= $section['title'] ?></option>
                        <?php endif; ?>
                <?php endforeach; ?>
            </select>

            <input type="hidden" name="section_id" value="<?= $sectionsEdit->id ?>">

        </div>




        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<?php $this->theme->footer(); ?>

