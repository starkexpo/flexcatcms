<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



    <div class="container-pages-title">
        <div>
            <a href="/flexcat/admin/dynamic/"><i class="icofont-briefcase"></i>Динамические страницы</a>
            <span> / </span>
            Создание динамической страницы
        </div>
        <?php $this->theme->block('controlPanel'); ?>
    </div>


    <div class="container-content">

        <form action="/flexcat/admin/dynamic/add/" method="post" enctype="multipart/form-data" id="sendForm">

            <ul class="flexcat-tabs">
                <li><a href="#tab-1" class="tab-control">Основные</a></li>
                <li class="active-tabs"><a href="#tab-2" class="tab-control">Шаблон</a></li>
                <li><a href="#tab-3" class="tab-control">CSS/LESS</a></li>
                <li><a href="#tab-4" class="tab-control">JavaScript</a></li>
            </ul>


            <div class="tab-panel" id="tab-1">
                <label for="formTitle">Название типовой динамической страницы</label>
                <input type="text" name="title" class="input-form">
                <br>


                <br>
                <label for="formTitle">Раздел макетов</label> <br>
                <select name="parent" id="siteLayout" class="select">
                    <option value="0" selected="selected">...</option>
                    <?php foreach ($sections as $section): ?>
                        <option value="<?= $section['id'] ?>" <?php
                        if ($section['id'] == $sections_now):
                            ?> selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>

                <br>
                <br>
                <label for="formTitle">Описание типовой динамической страницы</label> <br>
                <textarea name="description" cols="30" rows="10" class="form-control"></textarea>


            </div>

            <div class="tab-panel active-tab" id="tab-2">
                <label for="formTitle" class="tooltip" title="Код макета">Шаблон</label> <br>
                <textarea cols="140" name="code" rows="30" id="layoutCode"></textarea>
            </div>

            <div class="tab-panel" id="tab-3">
                <label for="formTitle" class="tooltip" title="CSS/LESS код стилей">CSS/LESS</label> <br>
                <textarea name="css_lees" id="cssLassCode" cols="140" rows="30" class="form-control"></textarea>
            </div>

            <div class="tab-panel" id="tab-4">
                <label for="formTitle" class="tooltip" title="JavaScript код">JavaScript</label> <br>
                <textarea name="java_script" id="javascriptCode" cols="140" rows="30" class="form-control"></textarea>
            </div>

            <div class="container-bar-buttons editing-bar">
                <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
                <a href="/flexcat/admin/layouts/" class="btn btn-red" id="cancelButton">Отмена</a>
            </div>
        </form>


    </div>
    <script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
    <!--<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>-->


    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.js"></script>
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.css">
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.css">

    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/css/css.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/javascript/javascript.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/clike/clike.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/php/php.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/xml/xml.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/smarty/smarty.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/selection/active-line.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/search.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/searchcursor.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.js"></script>


    <script src="/flexcat/admin/Assets/js/Plugin/Dynamic/dynamic.js"></script>
<?php $this->theme->footer(); ?>