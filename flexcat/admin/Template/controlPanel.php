<div class="container-control-window">
    <a href="#" id="resizeWindow">
        <i class="icofont-expand" style="font-size: 22px"></i>
    </a>
    <a href="/" id="updateWindow">
        <i class="icofont-refresh"></i>
    </a>
    <a href="#" id="menuHide">
        <i class="icofont-exchange" style="font-size: 22px"></i>
    </a>
</div>