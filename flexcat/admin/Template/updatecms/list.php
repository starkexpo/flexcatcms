<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

    <div class="container-pages-title">
        <div>
            <a href="/flexcat/admin/update/"><i class="icofont-cloud-refresh"></i>Обновление CMS</a>
        </div>
        <?php $this->theme->block('controlPanel'); ?>
    </div>

    <div class="container-content">



        <div class="container-bar-buttons">
            <a href="/flexcat/admin/update/send/"
               class="btn btn-green update-cms-button btn-bar" id="testUpdate">Проверить обновление</a>

            <div class="loadUpdate">
                <h4>Подождите, идет проверка...</h4>
                <img src="/flexcat/admin/Assets/img/loading/loading11.gif" alt="">
            </div>

            <h4 class="title-update-acces">Доступно</h4>

        </div>

        <span class="title-version" data-version="<?php echo \FlexCat\Template\Define::VERSION; ?>">
            Текущая вресия v.<?php echo \FlexCat\Template\Define::VERSION; ?>
        </span>


        <div class="blockUpdate hide">
            <div class="main-block-updates">
                <div class="iconCMS">
                    <img src="/flexcat/admin/Assets/img/FlexCat_logo.svg" alt="logo-update">
                </div>
                <div class="blockInfoUpdate">
                    <h4>FlexCat CMS</h4>
                    <p>Версия: <span id="versionUpdatesInfo"></span></p>
                </div>
                <div class="block-button-update">
                    <a href="<?= $link ?>" class="start_update">
                        <span>обновить</span> <img src="/flexcat/admin/Assets/img/loading/loading1.gif" alt="loadCMS" class="loadCMS hide">
                    </a>
                </div>
            </div>
            <div class="main-block-updates">
               <ul class="list-update">
               </ul>
            </div>
        </div>


        <div class="notfin-update hide">
            <h4>Нет доступных обновлений</h4>
        </div>



        <div class="how-new">
            <!--    <h4>Что нового ?</h4>-->
        </div>


        <script src="/flexcat/admin/Assets/js/Plugin/Updating/update.js"></script>
    </div>
<?php $this->theme->footer(); ?>