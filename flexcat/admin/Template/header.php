<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>
        Система управления сайтом FlexCat CMS v.<?php  echo Define::VERSION; ?>
    </title>
    <link rel="shortcut icon" href="/flexcat/admin/Assets/img/flexcat_favicon.ico">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="/flexcat/admin/Assets/css/loadPage.css">
    <script src="/flexcat/admin/Assets/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="/flexcat/admin/Assets/js/jquery.loadPage.js"></script>

    <link rel="stylesheet" href="/flexcat/admin/Assets/js/jquery/jquery-ui.min.css">
    <link rel="stylesheet" href="/flexcat/admin/Assets/css/icofont.css">
    <link rel="stylesheet" href="/flexcat/admin/Assets/css/style.css">
    <script src="/flexcat/admin/Assets/js/jquery/jquery-ui.min.js"></script>
</head>
<body>



