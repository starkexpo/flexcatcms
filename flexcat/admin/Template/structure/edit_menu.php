<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/structure/"><i class="icofont-site-map"></i>Структура сайта</a>
        <span> / </span>
        <a href="/flexcat/admin/structure/menu/"><i class="icofont-listine-dots" style="font-size: 15px"></i>Меню сайта</a>
        <span> / </span>
        Редактирование меню
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">
    <form action="/flexcat/admin/structure/menu/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название</label>
            <input type="text" name="title" class="input-form" placeholder="Название меню..." required
                   value="<?= $menus->title ?>">
            <input type="hidden" name="menu_id" value="<?= $menus->id ?>">

            <br><br>
            <label for="formTitle">Сайт</label>
            <div>
                <select name="site_id" id="sectionPage" class="select">
                    <?php foreach ($sites as $site): ?>
                        <option value="<?= $site->id ?>" <?php
                        if ($menus->site_id == $site->id):  ?> selected="selected"<?php endif; ?>><?= $site->title ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/structure/menu/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

