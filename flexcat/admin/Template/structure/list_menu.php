<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/structure/"><i class="icofont-site-map"></i>Структура сайта</a>
        <span> / </span>
        <a href="/flexcat/admin/structure/menu/"><i class="icofont-listine-dots" style="font-size: 15px"></i>Меню сайта</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <div class="container-bar-buttons">
        <a href="/flexcat/admin/structure/menu/create/" class="btn btn-green btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>
            Меню
        </a>
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th>Название</th>
            <th>Сайт</th>
            <th width="10"></th>
        </tr>
        </thead>

        <tbody>
         <?php foreach ($menusList as $menu):  ?>
            <tr>
                <th><input type="checkbox" name="page<?= $menu['id'] ?>" class="check-item"></th>
                <th><?= $menu['id'] ?></th>
                <td><?= $menu['title'] ?></td>
                <td>
                    <?php
                    foreach ($sites as $site) {
                        if ($menu['site_id'] == $site->id) {
                            echo $site->title;
                        }
                    }
                    ?>
                </td>
                <td>
                    <a href="#" class="btn-show-option">
                        <i class="icofont-settings"></i>
                    </a>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/structure/menu/edit/<?= $menu['id'] ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="/flexcat/admin/structure/menu/delete/<?= $menu['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach;  ?>
        </tbody>
    </table>

    <script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>
</div>
<?php $this->theme->footer(); ?>
