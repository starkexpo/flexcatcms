<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/structure/"><i class="icofont-site-map"></i>Структура сайта</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <div class="container-bar-buttons">
        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-site-map"></i>Раздел
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/structure/create/<?php
                        if (isset($now_id) && $now_id > 0) {
                            echo $now_id;
                        }
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>

        <a href="/flexcat/admin/structure/menu/" class="btn btn-blue btn-bar">
            <i class="icofont-listine-dots" style="font-size: 15px"></i>
            Меню
        </a>
    </div>




    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th>Название</th>
            <th>Путь</th>
            <th width="120">Меню</th>
            <th width="10" align="center"><i class="icofont-light-bulb public-structure"></i></th>
<!--            <th width="10" align="center"><i class="icofont-search-2 public-structure-finds"></i></th>-->
            <th width="10"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($items as $item): ?>
            <tr>
                <th><input type="checkbox" name="item<?= $item['id'] ?>" class="check-item"></th>
                <th><?= $item['id'] ?></th>
                <th class="br-grey-column">
                    <a href="/flexcat/admin/structure/<?= $item['id'] ?>"
                       class="structure-title"><?= $item['title'] ?></a>
                </th>
                <td>
                    <a href="/..<?= $item['path'] ?>" target="_blank"
                       class="block-path-structure"><?= $item['path'] ?></a>
                </td>
                <td>
                    <?php
                    foreach ($menusList as $menu) {
                        if ($item['menu'] == $menu['id']) {
                            echo $menu['title'];
                        }
                    }
                    ?>
                </td>
                <td align="center">
                    <a href="/flexcat/admin/structure/setpublic/<?= $item['id'] ?>" class="published-button <?php
                    if ($item['published'] == 'disabled') {
                        echo ' public-off';
                    }
                    ?>">
                        <i class="icofont-light-bulb"></i>
                    </a>
                </td>
                <!--<td align="center">
                    <a href="#" class="public-structure-finds"><i class="icofont-search-2"></i></a>
                </td>-->
                <td>
                    <a href="#" class="btn-show-option">
                        <i class="icofont-settings"></i>
                    </a>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/structure/edit/<?= $item['id'] ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/structure/delete/<?= $item['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <script src="/flexcat/admin/Assets/js/Plugin/Sites/jquery.publicButton.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>
</div>
<?php $this->theme->footer(); ?>
