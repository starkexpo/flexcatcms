<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/structure/"><i class="icofont-site-map"></i>Структура
            сайта</a>
        <span> / </span>
        Редактирование узла структуры "<?= $now_id->title ?>"
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">

    <form action="/flexcat/admin/structure/update/" method="post"
          enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a>
            </li>
            <li><a href="#tab-2" class="tab-control">SEO</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела в меню</label>
            <input type="text" name="title" class="input-form"
                   value="<?= $now_id->title ?>" required>
            <br>
            <div class="block-item-create">
                <div class="block-create-for-admin">

                    <div class="block-status-create-item">
                        <div>
                            <label for="formTitle">Родительский
                                раздел</label><br>
                            <select name="parent" id="sectionPage"
                                    class="select">
                                <option value="0">...</option>
                                <?php foreach ($listItems as $item): ?>
                                    <?php if ($item['id'] != $now_id->id): ?>
                                        <option value="<?= $item['id'] ?>" <?php
                                        if ($item['id'] == $now_id->parent) {
                                            echo ' selected="selected" ';
                                        }
                                        ?>><?= $item['title'] ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div>
                            <label for="formTitle">Порядок
                                сортировки</label><br>
                            <input type="text" name="sorting" class="input-form"
                                   style="width: 110px" value="0">
                        </div>
                    </div>


                    <label for="formTitle"
                           title="Фрагмент пути относительно родительского раздела, например contact"
                           class="tooltip">Путь</label>
                    <input type="text" name="path" class="input-form"
                           value="<?php
                           echo str_replace('/', '', $now_id->path);
                           ?>"
                           style="width: calc(100% - 25px);">

                    <br>

                    <label for="formTitle">Макет</label>
                    <select name="layouts" class="select"
                            id="layouts-create-item">
                        <?php foreach ($layouts as $layout): ?>
                            <option value="<?= $layout['id'] ?>" <?php
                            if ($layout['id'] == $now_id->layouts) {
                                echo ' selected="selected" ';
                            }
                            ?>><?= $layout['title'] ?></option>
                        <?php endforeach; ?>
                    </select>

                </div>

                <div class="block-create-for-admin">
                    <label for="formTitle">Меню</label>
                    <select name="menu" class="select">
                        <?php foreach ($menusList as $menu): ?>
                            <option value="<?= $menu['id'] ?>" <?php
                            if ($menu['id'] == $now_id->menu) {
                                echo ' selected="selected" ';
                            }
                            ?>><?= $menu['title'] ?></option>
                        <?php endforeach; ?>
                    </select>

                    <div class="block-status-create-item">
                        <div>
                            <label class="label-page" style="width:100%;">Статус
                                публикации</label>
                            <input type="checkbox" name="published"
                                   class="check-switch-ios" value="enable" <?php
                                   if ($now_id->published ==
                            'enable'): ?>checked<?php endif; ?> />
                        </div>

                        <div>
                            <label class="label-page" style="width:100%;">Индексировать</label>
                            <input type="checkbox" name="indexing"
                                   class="check-switch-ios" value="enable" <?php
                                   if ($now_id->indexing ==
                            'enable'): ?>checked<?php endif; ?>/>
                        </div>

                        <div>
                            <label class="label-page" style="width:100%;">Отображать
                                в меню сайта</label>
                            <input type="checkbox" name="show_menu"
                                   class="check-switch-ios" value="enable" <?php
                                   if ($now_id->show_menu ==
                            'enable'): ?>checked<?php endif; ?>/>
                        </div>
                    </div>

                    <br>
                    <label for="formTitle">Группа доступа</label>
                    <select name="access" class="select">
                        <option value="all" selected="selected">Все</option>
                        <option value="3">Private</option>
                    </select>
                </div>
            </div>


            <br><br>
            <label for="formTitle">Тип раздела</label>


            <div id="structure_types">
                <br>

                <label class="structure-radio button-type-page">
                    <input type="radio" name="typeItem" value="0" <?php
                    if ($now_id->typeItem == 'page'):
                    ?>checked="checked" <?php endif; ?>>
                    <span <?php
                    if ($now_id->typeItem == 'page'):
                        ?> class="activeButton" <?php endif; ?>><i
                                class="icofont-file-document"></i>Страница</span>
                </label>

                <label class="structure-radio button-type-dynamic-page">
                    <input type="radio" name="typeItem" value="1" <?php
                    if ($now_id->typeItem == 'dynamic'):
                    ?>checked="checked" <?php endif; ?>>
                    <span <?php
                          if ($now_id->typeItem == 'dynamic'):
                          ?>class="activeButton" <?php endif; ?>><i
                                class="icofont-listing-box"></i>Динамическая страница</span>
                </label>

                <label class="structure-radio button-type-link">
                    <input type="radio" name="typeItem" value="2" <?php
                    if ($now_id->typeItem == 'link'):
                    ?>checked="checked" <?php endif; ?>>
                    <span <?php
                          if ($now_id->typeItem == 'link'):
                          ?>class="activeButton" <?php endif; ?>><i
                                class="icofont-link"></i>Ссылка</span>
                </label>
            </div>
            <br>


            <div id="radio0" class="<?php
            if ($now_id->typeItem != 'page'):
                ?>hide<?php endif; ?> tab-type">
                <!--<label for="formTitle">Раздел документов</label><br>
                <select name="page_sections" class="select">
                    <option value="0" selected="selected">...</option>
                    <option value="3">Slower</option>
                    <option value="1">Slow</option>
                    <option value="2">Medium</option>
                    <option value="3">Fast</option>
                    <option value="4">Faster</option>
                    <option value="5">Very fast</option>
                </select>-->

                <br>
                <br>
                <label for="formTitle">Название документа</label><br>
                <select name="page" id="loadPages" class="select">
                    <option value="0" selected="selected">...</option>
                    <?php foreach ($pagesList as $page): ?>
                        <option value="<?= $page['id'] ?>" <?php
                        if ($page['id'] ==
                        $now_id->page_id): ?>selected="selected" <?php endif;
                        ?>><?= $page['title'] ?></option>
                    <?php endforeach; ?>
                </select>
                <br>
                <label for="formContent">Содержимое страницы</label><br>
                <textarea id="contentPage" name="contentPage"><?php
                    foreach ($pagesList as $page) {
                        if ($page['id'] == $now_id->page_id) {
                            echo $page['content'];
                        }
                    }
                    ?></textarea>
            </div>

            <div id="radio1" class="<?php
            if ($now_id->typeItem != 'dynamic'):
                ?>hide<?php endif; ?> tab-type">
                <label for="formTitle">Раздел</label><br>
                <select name="dynamic_sections" id="sectionPage" class="select">
                    <option value="0">...</option>
                    <?php foreach ($dynamic_sections as $section): ?>
                        <option value="<?= $section['id'] ?>"><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>

                <br><br><br>

                <label for="formTitle">Страница</label><br>
                <select name="dynamic_template" id="templatePage">
                    <option value="0">...</option>
                    <?php foreach ($dynamic_template as $template): ?>
                        <option value="<?= $template['id'] ?>" <?php
                        if ($template['id'] ==
                            $now_id->dynamic_page): ?> selected="selected" <?php endif;
                        ?>><?= $template['title'] ?></option>
                    <?php endforeach; ?>
                </select>

                <?php
                @$optionSave = base64_decode($now_id->options);
                @$optionSave = unserialize($optionSave);
                ?>

                <br>
                <div id="optionLoad"
                     style=" width: 100%; margin-top: 20px; padding-top: 20px;">
                    <?php foreach ($options as $option): ?>

                        <?php if ($option['field_type'] == "input"): ?>
                            <label for="formTitle"><?= $option['title'] ?></label>
                            <input type="text" name="options<?= $option['id'] ?>" class="input-form" value="<?php

                            if (!empty($optionSave)) {
                                foreach ($optionSave as $value) {
                                    if ($value['id'] == $option['id']) {
                                        echo $value['value'];
                                    }
                                }
                            }
                            ?>">
                            <br>
                        <?php endif; ?>


                        <?php if ($option['field_type'] == "sql"): ?>
                            <?php
                            $i = $option['id'];
                            $sql_list[$i] = \FlexCat\Admin\Modules\Model\Widget\WidgetRepository::getSQL($option['field_sql']);
                            ?>
                            <label for="formTitle"><?= $option['title'] ?></label>
                            <br>
                            <?php if (!empty($sql_list)): ?>
                                <select name="options<?= $option['id'] ?>"
                                        class="select">
                                    <?php foreach ($sql_list[$i] as $value): ?>
                                        <option value="<?= $value[$option['field_value']]; ?>"<?php


                                        if (!empty($optionSave)) {
                                            foreach ($optionSave as $values) {
                                                if ($values['value'] ==
                                                    $value[$option['field_value']]) {
                                                    echo ' selected="selected"';
                                                }
                                            }
                                        }
                                        ?>>
                                            <?= $value[$option['field_title']]; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                            <br><br>
                        <?php endif; ?>


                    <?php endforeach; ?>
                </div>

            </div>

            <div id="radio2" class="<?php
            if ($now_id->typeItem != 'link'):
                ?>hide<?php endif; ?> tab-type">
                <label for="formTitle" class="tooltip" title="Раздел сайта может являтся внешней ссылкой">Ссылка на другой файл</label><br>
                <input type="text" name="link" class="input-form" value="<?= $now_id->link ?>">
            </div>


            <!--конец блока 1-->
        </div>
        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Заголовок страницы [Title]</label><br>
            <textarea name="meta_title"
                      class="input-form seo-input"><?= $now_id->meta_title ?></textarea>

            <label for="formTitle">Описание страницы [Description]</label><br>
            <textarea name="description"
                      class="input-form seo-input"><?= $now_id->description ?></textarea>

            <label for="formTitle">Ключевые слова [Keywords]</label><br>
            <textarea name="keywords"
                      class="input-form seo-input"><?= $now_id->keywords ?></textarea>
        </div>

        <input type="hidden" name="item_id" value="<?= $now_id->id ?>">

        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить
            </button>
            <button type="submit" class="btn btn-green" name="save_create">
                Применить
            </button>
            <a href="/flexcat/admin/structure/" class="btn btn-red"
               id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.switch-ios.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/jquery-1.7.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.css"/>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.js"></script>-->


<script src="/flexcat/admin/Assets/js/Plugin/Structure/create_&_edit.js"></script>
<script src="/flexcat/admin/Assets/js/jquery/jquery-ui.min.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/Widget/widget.js"></script>
<?php $this->theme->footer(); ?>

