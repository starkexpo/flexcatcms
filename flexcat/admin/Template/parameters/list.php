<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/dynamic/"><i class="icofont-briefcase"></i>Динамические страницы</a>
        <span> / </span>
        <a href="/flexcat/admin/dynamic/parameters/<?= $dynamic[0]['id'] ?>">Параметры динамической страницы
            "<?= $dynamic[0]['title'] ?>"
        </a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <div class="container-bar-buttons">
        <a href="/flexcat/admin/dynamic/parameters/create/<?= $dynamic[0]['id'] ?>" class="btn btn-green  btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>Добавить
        </a>
    </div>



    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="40%">Название</th>
            <th width="40%">Название переменной</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($options as $option): ?>
            <tr>
                <th><input type="checkbox" name="layout<? //= $widget['id'] ?>" class="check-item"></th>
                <th><?= $option['id'] ?></th>
                <th class="br-grey-column"><?= $option['title'] ?></th>
                <td>
                    <?= $option['variable'] ?>
                </td>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/dynamic/parameters/edit/<?= $option['id'] ?>"
                           class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/dynamic/parameters/delete/<?= $option['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
