<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/dynamic/"><i class="icofont-briefcase"></i>Динамические страницы</a>
        <span> / </span>
        <a href="/flexcat/admin/dynamic/parameters/<?= $dynamic[0]['id'] ?>">
            <i class="icofont-code-alt"></i><?= $dynamic[0]['title'] ?>
        </a>
        <span> / </span>
        <i class="icofont-listine-dots dynamic-ico-option"></i>Добавление параметра динамической страницы
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/dynamic/parameters/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
            <li><a href="#tab-2" class="tab-control">Дополнительные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название параметра</label>
            <input type="text" name="title" class="input-form">
            <br>

            <label for="formTitle">Описание параметра</label>
            <textarea name="description" cols="30" rows="10" class="form-control" style="height: 60px"></textarea>


            <div class="block-item-create">
                <div class="block-create-for-admin">

                    <label for="formTitle">Название переменной</label>
                    <input type="text" name="variable" class="input-form">

                    <br>
                    <label for="formTitle">Значение по умолчанию</label>
                    <input type="text" name="defaults" class="input-form">

                </div>
                <div class="block-create-for-admin">

                    <label for="formTitle">Тип</label>
                    <select name="type" id="typeVariable" class="select">
                        <option value="input" selected="selected">Поле ввода</option>
                        <option value="checkbox">Флажок</option>
                        <option value="select">Список</option>
                        <option value="sql">SQL-запрос</option>
                        <option value="textarea">Большое текстовое поле</option>
                        <option value="template">TPL-шаблон</option>
                    </select>

                </div>
            </div>

            <div id="field_sql" class="hide">  <!--class="hide"-->
                <label for="formTitle">SQL-запрос</label>
                <textarea name="field_sql" cols="30" rows="10" class="form-control" style="height: 100px; font-size: 12px !important;"></textarea>

                <br>
                <div class="block-item-create">
                    <div class="block-create-for-admin">
                        <label for="formTitle">Поле заголовка</label>
                        <input type="text" name="field_title" class="input-form">
                    </div>
                    <div class="block-create-for-admin">
                        <label for="formTitle">Поле значения</label>
                        <input type="text" name="field_value" class="input-form">
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Идентификатор динамической страницы</label>
            <input type="text" name="dynamic_id" value="<?= $dynamic[0]['id'] ?>" class="input-form" readonly>
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/dynamic/parameters/<?= $dynamic[0]['id'] ?>" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<script src="/flexcat/admin/Assets/js/Plugin/Parameters/parameters.js"></script>


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

