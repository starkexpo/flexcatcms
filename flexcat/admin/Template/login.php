<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Вход в центр администрирования FlexCat CMS</title>
    <link rel="shortcut icon" href="/flexcat/admin/Assets/img/flexcat_favicon.ico">
    <link rel="stylesheet" href="../Assets/css/loadPage.css">
    <script src="../Assets/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="../Assets/js/pageLoad.js"></script>

    <script src="../Assets/js/jquery/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="../Assets/css/icofont.min.css">
    <link rel="stylesheet" href="../Assets/css/login.css">
</head>
<body>
<div id="page-preloader">
    <h1 class="title-load-page">FlexCat CMS</h1>
    <div class="contpre">
        <span class="spinner"></span>
        <br>Подождите<br><small>идет загрузка</small><br />
        <img src="../Assets/img/loading/loading11.gif" class="loadSite"/>
    </div>
</div>

<div class="container-authorization hide">
    <div class="bloc-logo-form-authorization">
        <img src="../Assets/img/FlexCat_logo.svg" alt="FlexCat"><br>
        <span>Flex<b>Cat</b> &nbsp;  <em>CMS</em></span>
    </div>
    <div class="block-form-authorization">

        <form action="/flexcat/admin/auth/" method="post" role="form" id="formLogin">
            <i class="icofont-user-alt-4"></i>
            <input type="text" name="login"  placeholder="Пользователь" class="autorizet-input logUs" id="login" required autofocus><br>
            <i class="icofont-key"></i>
            <input type="password" name="password"  placeholder="Пароль" class="autorizet-input" id="password" required><br>
            <button type="submit" name="entrance" id="sendUs">
                <span>Войди</span>
                <img src="../Assets/img/loading/puff.svg" height="24" alt="load" class="loadingImage">
            </button>
            <span class="message-error">Please try again soon</span>
        </form>
    </div>
</div>

<footer class="footer-authorization">
    <div class="copyright">
        <p>Copyright © 2018</p>
        <p>Официальный сайт FlexCat CMS:  <a href="http://www.flexcat.ru" target="_blank">www.flexcat.ru</a><br>
            Служба технической поддержки:  <a href="mailto:support@flexcat.ru">support@flexcat.ru</a>
        </p>
    </div>
</footer>

<script src="../Assets/js/loginFormLoad.js"></script>
</body>
</html>