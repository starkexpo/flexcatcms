<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/pages/"><i class="icofont-file-document"></i>Страницы и документы</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">


    <div class="container-bar-buttons">
        <a href="/flexcat/admin/pages/create/<?php if (isset($now_id)) { echo $now_id; } ?>" class="btn btn-green m-r-1 btn-bar">
            <i class="icofont-plus" style="font-size: 14px"></i>Документ
        </a>

        <div class="group-but-sub m-r-1 group-blue">
            <a href="#" class="btn btn-blue btn-group btn-bar">
                <i class="icofont-ui-folder"></i>Раздел
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/pages/section/create/<?php if (isset($now_id)) { echo $now_id; } ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>
    </div>


    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th width="10" align="center"><i class="icofont-navigation-menu"></i></th>
            <th>Название</th>
            <th>Макет</th>
            <th width="100">Дата</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($pages as $page): ?>
            <tr>
                <th><input type="checkbox" name="page<?= $page['id'] ?>" class="check-item"></th>
                <th><?= $page['id'] ?></th>
                <th>
                    <?php if ($page['type'] == 'section'): ?>
                        <i class="icofont-ui-folder" style="font-size: 16px"></i>
                    <?php endif; ?>

                    <?php if ($page['type'] == 'page'): ?>
                        <i class="icofont-file-document"></i>
                    <?php endif; ?>
                </th>
                <td>
                    <?php if ($page['type'] == 'section'): ?>
                        <a href="/flexcat/admin/pages/<?= $page['id'] ?>" class="title-item layouts-title">
                            <?= $page['title'] ?>
                        </a>
                    <?php endif; ?>

                    <?php if ($page['type'] == 'page'): ?>
                        <?= $page['title'] ?>
                    <?php endif; ?>
                </td>
                <td>
                    макет
                </td>
                <td><?= $page['date'] ?></td>
                <td>
                    <div class="btn-mini-group" style="justify-content: flex-start;">
                        <a href="<?php
                        if ($page['type'] == 'page') {
                            echo  '/flexcat/admin/pages/edit/' . $page['id'];
                        } else {
                            echo '/flexcat/admin/pages/section/edit/' . $page['id'];
                        }
                        ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <?php if ($page['type'] == 'page'): ?>
                        <a href="#" class="delete-but btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <?php endif; ?>
                        <a href="/flexcat/admin/pages/delete/<?= $page['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


</div>
<?php $this->theme->footer(); ?>
