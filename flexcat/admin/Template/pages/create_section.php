<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/pages/"><i class="icofont-file-document"></i>Страницы и документы</a>
        <span> / </span>
        <i class="icofont-ui-folder"></i>Создание раздела документов
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/pages/section/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Название раздела документов</label>
            <input type="text" name="title" class="input-form">
            <br>




            <br>
            <label for="formTitle">Родительский раздел документов</label> <br>
            <select name="section" id="siteLayout" class="select">
                <option value="0" selected="selected">...</option>
                <?php foreach ($sections as $section): ?>
                    <option value="<?= $section['id'] ?>" <?php
                    if ($section['id'] == $sections_now):
                        ?> selected="selected" <?php endif; ?> ><?= $section['title'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>




        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/pages/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

