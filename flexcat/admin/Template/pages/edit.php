<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/pages/"><i class="icofont-file-document"></i>Страницы и документы</a>
        <span> / </span>
        <i class="icofont-pencil"></i>Редактирование страницы
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">


    <form action="/flexcat/admin/page/update/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Описание</a></li>
            <li><a href="#tab-2" class="tab-control">Публикация</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">
            <label for="formTitle">Заголовок</label>
            <input type="text" name="title" class="input-form" placeholder="Заголовок страницы..."
                   value="<?= $page->title ?>">
            <br>
            <label for="formContent">Контент</label><br>
            <textarea id="content" name="content"><?=
                $page->content
                ?></textarea>
            <br>
            <div style="margin-top: -10px">
                <label class="label-page">Статус публикации</label>
                <input type="checkbox" name="published" class="check-switch-ios" value="enable" <?php
                if ($page->published == "enable"): ?>checked<?php endif; ?>/>
            </div>

            <input type="hidden" name="page_id" value="<?= $page->id ?>">

            <label for="formTitle" style="float: right; margin-top: -50px; margin-right: 200px">Раздел</label>
            <div style="float: right; margin-top: -20px">
                <select name="section" id="sectionPage" class="select">
                    <option value="0">...</option>
                    <?php foreach ($sections as $section): ?>
                        <option value="<?= $section['id'] ?>" <?php
                        if ($page->section == $section['id']):
                        ?>selected="selected" <?php endif; ?>><?= $section['title'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

        </div>

        <div class="tab-panel" id="tab-2">
            <label for="formTitle">Алиас заголовока</label>
            <input type="text" name="alias" class="input-form" placeholder="алиас заголовка"
                   value="<?= $page->alias ?>">
            <br>
            <label for="formTitle">Дата</label>
            <input type="text" name="date" class="input-form"
                   placeholder="возможно не указывать" style="width: 300px;" value="<?= $page->date ?>">


        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <button type="submit" class="btn btn-green" name="save_create">Применить</button>
            <a href="/flexcat/admin/pages/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.switch-ios.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/jquery-1.7.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.css"/>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Ckeditor/redactor.js"></script>-->
<!--<script src="/flexcat/admin/Assets/js/Plugin/Page/page.js"></script>-->
<?php $this->theme->footer(); ?>

