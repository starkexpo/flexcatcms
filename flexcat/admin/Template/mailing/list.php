<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/mailing/"><i class="icofont-paper-plane"></i>Почтовые рассылки</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>


<div class="container-content">


    <div class="container-bar-buttons">
        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-email"></i>Почтовая рассылка
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/mailing/create/<?php
                    /*if (isset($section_id)) {
                        echo $section_id;
                    }*/
                    ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>
    </div>



    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th width="22">Код</th>
            <th>Название рассылки</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($mailing as $item): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $item['id'] ?>" class="check-item"></th>
                <th><?= $item['id'] ?></th>
                <th>
                    <a href="/flexcat/admin/mailing/list/<?= $item['id'] ?>" class="structure-title"><?= $item['title'] ?></a>
                </th>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/mailing/edit/<?= $item['id'] ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="#" class="btn-mini-blue">
                            <i class="icofont-ui-copy"></i>
                        </a>
                        <a href="/flexcat/admin/mailing/delete/<?= $item['id'] ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
