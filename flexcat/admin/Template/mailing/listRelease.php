<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/mailing/"><i class="icofont-paper-plane"></i>Почтовые рассылки</a>
        <span>/</span>
        <a href="/flexcat/admin/mailing/list/<?= $nowMailing[0]['id'] ?>">Выпуски рассылки
            &laquo;<?= $nowMailing[0]['title'] ?>&raquo;</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">


    <div class="container-bar-buttons">

        <div class="group-but-sub m-r-1 group-green">
            <a href="#" class="btn btn-green btn-group btn-bar">
                <i class="icofont-envelope"></i>Выпуск
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/mailing/users/create/<?php
                    /*if (isset($section_id)) {
                        echo $section_id;
                    }*/
                    ?>" id="newMailing" class="newRealease">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>


        <div class="group-but-sub m-r-1 group-blue">
            <a href="#" class="btn btn-blue btn-group btn-bar"><!--show-button-group-->
                <i class="icofont-users"></i>Подписчики
                <span><i class="icofont-rounded-down"></i></span>
            </a>
            <ul class="btn-group-drop-menu hide">
                <li>
                    <a href="/flexcat/admin/mailing/users/<?= $nowMailing[0]['id'] ?>">
                        <i class="icofont-users-alt-3" style="font-size: 15px !important;"></i>Подписчики
                    </a>
                </li>
                <li>
                    <a href="/flexcat/admin/mailing/users/create/<?= $nowMailing[0]['id'] ?>">
                        <i class="icofont-plus" style="font-size: 12px !important;"></i>Добавить
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <?php
    //    var_dump($release);
    //    var_dump($nowMailing);
    //        var_dump($release);
    //    var_dump($admines);
    ?>

    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th>Тема письма</th>
            <th>Автор</th>
            <th width="150">Создан</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($release as $item): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $item['id'] ?>" class="check-item"></th>
                <th><?= $item['theme'] ?></th>
                <th>
                    <?php
                    foreach ($admines as $admine) {
                        if ($admine['id'] == $item['admin_id']) {
                            echo $admine['lastname'] . " " . $admine['firstname'];
                        }
                    }
                    ?>
                </th>
                <th>
                    <?= $item['date'] ?>
                </th>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/mailing/read/<?= $item['id'] ?>" class="btn-mini-blue readRelease">
                            <i class="icofont-eye" style="font-size: 15px;"></i>
                        </a>
                        <a href="/flexcat/admin/mailing/delete/<?= $item['id']  ?>"
                           class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>


    <div class="hide">
        <div id="newRelease">

            <form action="/flexcat/admin/mailing/sendmail/" enctype='multipart/form-data' method='POST' id="formMail">
                <label for="formTitle">Отправитель: </label>
                <input type="text" name="author" class="input-form" value="<?php

                if (strlen($nowMailing[0]['emailFrom']) > 0) {
                    echo $nowMailing[0]['nameFrom'] . " <" . $nowMailing[0]['emailFrom'] . ">";
                } else {
                    echo $nowMailing[0]['emailFrom'];
                }
                ?>">

                <label for="formTitle">Тема письма: </label>
                <input type="text" name="theme" class="input-form">

                <!--<br>
                <label for="">Прикрепить к письму: </label><br>
                <input type="file" name="mail_file" maxlength="64">-->
                <br>
                <label for="formTitle">Содержание письма:</label><br>
                <textarea name="template" cols="30" rows="20" class="form-control" id="layoutCode" style="height: 300px;">Для отписки от рассылки пройдите по ссылке: <a href="{LINK}">{LINK}</a> </textarea>

                <br>
                <input type="hidden" name="mailing_id" value="<?= $nowMailing[0]['id'] ?>">

                <?php
                $j = 0;
                foreach ($users as $user) {
                    $j++;
                }

                $limitStep = intval($limitStep);
                $sumLimit = intval(ceil($j / $limitStep));
                $procent = intval(ceil(100 / $sumLimit));

                ?>

                <input type="hidden" name="allStep" value="<?= $sumLimit ?>" id="allStep">
                <input type="hidden" name="stepOne" value="<?= $procent ?>" id="stepOne">
                <input type="hidden" name="stepNow" value="1" id="stepNow">

                <button type="submit" name="sendMail" class="btn btn-blue border-circle" id="sendMail">
                    Отправить<i class="icofont-paper-plane" style="margin-left: 5px"></i>
                </button>
            </form>

            <div class="blockLoad hide">
                <h1>Подождите, идет рассылка писем...</h1>
                <div id="progressBarMain" style="width: 100%;"></div>
                <img src="/flexcat/admin/Assets/img/loading/loading11.gif" alt="load" class="loadPost">
            </div>
        </div>
    </div>


    <div class="hide">
        <div id="formRead">
            <label for="formTitle">Отправитель: </label>
            <input type="text" name="authorRead" class="input-form" readonly>

            <label for="formTitle">Тема письма: </label>
            <input type="text" name="themeRead" class="input-form" readonly>

            <br>
            <label for="formTitle">Содержание письма:</label><br>
            <textarea name="templateRead" cols="30" rows="20" class="form-control" style="height: 300px;" readonly></textarea>

            <br>
            <h4 class="dateRead">Отправлено: <span></span></h4>

        </div>
    </div>


    <!--<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.js"></script>
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.css">
    <link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.css">

    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/css/css.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/htmlmixed/htmlmixed.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/javascript/javascript.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/clike/clike.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/php/php.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/xml/xml.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/smarty/smarty.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/selection/active-line.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/search.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/searchcursor.js"></script>
    <script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.js"></script>-->


    <script src="/flexcat/admin/Assets/js/Plugin/Mailing/createRelease.js"></script>

</div>
<?php $this->theme->footer(); ?>
