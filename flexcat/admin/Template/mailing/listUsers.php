<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>


<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/mailing/"><i class="icofont-paper-plane"></i>Почтовые рассылки</a>
        <span>/</span>
        <a href="/flexcat/admin/mailing/list/<?= $nowMailing[0]['id'] ?>">Подписчики рассылки &laquo;<?= $nowMailing[0]['title'] ?>&raquo;</a>
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>





<div class="container-content">


    <div class="container-bar-buttons">
        <div class="group-but-sub">
            <div class="container-bar-buttons">
                <a href="/flexcat/admin/mailing/users/create/<?= $nowMailing[0]['id'] ?>" class="btn btn-green">
                    <i class="icofont-plus" style="font-size: 14px"></i>Добавить подписчика
                </a>
            </div>
        </div>
    </div>

    <table class="table-list">
        <thead>
        <tr>
            <th width="22">&nbsp;</th>
            <th>Имя подписчика</th>
            <th>E-mail адрес</th>
            <th width="150">Подписан от</th>
            <th width="100"></th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <th><input type="checkbox" name="layout<?= $user['id']  ?>" class="check-item"></th>
                <th>
                    <?= $user['name']  ?>
                </th>
                <th>
                    <a href="mailto:<?= $user['email']  ?>" class="structure-title"><?= $user['email']  ?></a>
                </th>
                <th>
                    <?= $user['date']  ?>
                </th>
                <td>
                    <div class="btn-mini-group">
                        <a href="/flexcat/admin/mailing/users/edit/<?= $user['id']  ?>" class="edit-but btn-mini-green">
                            <i class="icofont-ui-edit"></i>
                        </a>
                        <a href="/flexcat/admin/mailing/users/delete/<?= $user['id']  ?>" class="delete-but btn-mini-red">
                            <i class="icofont-bin"></i>
                        </a>
                    </div>
                </td>
            </tr>
        <?php endforeach;  ?>
        </tbody>
    </table>

    <!--<script src="/flexcat/admin/Assets/js/Plugin/Sites/sites.js"></script>-->

</div>
<?php $this->theme->footer(); ?>
