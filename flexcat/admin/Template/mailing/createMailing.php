<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>

<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/mailing/"><i class="icofont-paper-plane"></i>Почтовые рассылки</a>
        <span> / </span>
        Добавление почтовой рассылки
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>



<div class="container-content">

    <form action="/flexcat/admin/mailing/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li><a href="#tab-1" class="tab-control">Основные</a></li>
            <li class="active-tabs"><a href="#tab-3" class="tab-control">Письмо подписки</a></li>
        </ul>

        <div class="tab-panel" id="tab-1">

            <label for="formTitle">Название почтовой рассылки</label>
            <input type="text" name="title" class="input-form">
            <br>

            <label for="formTitle">Описание почтовой рассылки</label><br>
            <textarea name="description" cols="30" rows="10" class="form-control" style="height: 50px"></textarea>
            <br>

            <div style="float: left; width: 45%;">
                <label for="formTitle">E-mail отправителя</label>
                <input type="text" name="emailFrom" class="input-form" placeholder="support@site.com">
            </div>

            <div style="float: right; width: 45%; margin-right: 10px;">
                <label for="formTitle">Отправитель</label>
                <input type="text" name="nameFrom" class="input-form" placeholder="Администратор">
            </div>

        </div>


        <div class="tab-panel active-tab" id="tab-3">
            <label for="formTitle">Тема письма</label>
            <input type="text" name="themeFrom" class="input-form" placeholder="Подписка на рассылку новостей успешно оформлена">

            <br>

            <label for="formTitle" class="tooltip" title="Данное письмо будет отправлено как только пользователь оформит подписку на сайте">
                Письмо подписки рассылки</label> <br>
            <textarea cols="140" name="mailTemplate" rows="30" class="form-control" style="height: 550px"><?php
                $text =  'Здравствуйте, {NAME}! ';
                $text = $text . '
                ';
                $text = $text . 'Вы успешно оформили подписку на рассылку новостей сайта: www.your-site.com ';
                $text = $text . '
                ';
                $text = $text . 'Данное письмо сгенерировано автоматически, отвечать на него не нужно! ';
                $text = $text . '
             
                ';
                $text = $text . 'С наилучшими пожеланиями, администрация сайта http://www.your-site.com';
                echo $text;
                ?></textarea>
        </div>


        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/mailing/" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>
<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<!--<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.js"></script>
<link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/lib/codemirror.css">
<link rel=stylesheet href="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.css">

<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/css/css.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/htmlmixed/htmlmixed.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/javascript/javascript.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/clike/clike.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/php/php.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/xml/xml.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/mode/smarty/smarty.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/selection/active-line.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/search.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/search/searchcursor.js"></script>
<script src="/flexcat/admin/Assets/js/Plugin/CodeMirror/addon/dialog/dialog.js"></script>-->

<script src="/flexcat/admin/Assets/js/Plugin/Mailing/mailing.js"></script>


<?php $this->theme->footer(); ?>

