<?php $this->theme->header(); ?>
<?php $this->theme->block('sidebar'); ?>



<div class="container-pages-title">
    <div>
        <a href="/flexcat/admin/mailing/"><i class="icofont-paper-plane"></i>Почтовые рассылки</a>
        <span> / </span>
        Добавление подписчика
    </div>
    <?php $this->theme->block('controlPanel'); ?>
</div>




<div class="container-content">

    <?php
    //    var_dump($sections);
    ?>

    <form action="/flexcat/admin/mailing/users/add/" method="post" enctype="multipart/form-data" id="sendForm">

        <ul class="flexcat-tabs">
            <li class="active-tabs"><a href="#tab-1" class="tab-control">Основные</a></li>
        </ul>

        <div class="tab-panel active-tab" id="tab-1">

            <div style="float: left; width: 45%;">
                <label for="formTitle">E-mail подписчика</label>
                <input type="text" name="email" class="input-form" placeholder="user@site.com">
            </div>

            <div style="float: right; width: 45%; margin-right: 10px;">
                <label for="formTitle">Имя подписчика</label>
                <input type="text" name="name" class="input-form" placeholder="Иван">
            </div>


            <br><br>
            <br><br>
            <label for="formTitle">Подписка на рассылку</label> <br>
            <select name="mailing"  class="select">
                <?php foreach ($mailing as $item): ?>
                    <option value="<?= $item['id'] ?>" <?php
                    if ($item['id'] == $nowID):
                        ?> selected="selected" <?php endif; ?>><?= $item['title'] ?></option>
                <?php endforeach; ?>
            </select>
            <br>
            <br>


        </div>

        <div class="container-bar-buttons editing-bar">
            <button type="submit" class="btn btn-blue" name="save">Сохранить</button>
            <a href="/flexcat/admin/mailing/users/<?= $nowID ?>" class="btn btn-red" id="cancelButton">Отмена</a>
        </div>
    </form>


</div>


<script src="/flexcat/admin/Assets/js/jquery.tabs.js"></script>
<script src="/flexcat/admin/Assets/js/jquery.sendForm.js"></script>


<?php
$this->theme->footer();
//$this->theme->block('footer');
?>

