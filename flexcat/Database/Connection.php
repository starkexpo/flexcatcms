<?php

namespace FlexCat\Database;

use \PDO;
use FlexCat\Config\Config;


class Connection
{
    /**
     * @var
     */
    private $link;

    /**
     * Connection constructor.
     */
    public function __construct()
    {
        $this->connect();
    }

    /**
     * @return $this
     */
    public function connect()
    {
        $config = Config::file('ConfigDatabase');

        $dns = 'mysql:host=' . $config['host'] . ';dbname=' . $config['db_name'] . ';charset=' . $config['charset'];

        $this->link = new PDO($dns, $config['username'], $config['password']);

        return $this;
    }

    /**
     * @param $sql
     * @param array $values
     * @return mixed
     */
    public function execute($sql, $values = [])
    {
        $sth = $this->link->prepare($sql);

        return $sth->execute($values);
    }


    /**
     * @param $sql
     * @param array $values
     * @return array
     */
    // $statement = PDO::FETCH_OBJ
    public function query($sql, $values = [], $statement = PDO::FETCH_OBJ)  //$statement = PDO::FETCH_OBJ - небыло
    {
        $sth = $this->link->prepare($sql);
        $sth->execute($values);

        $result = $sth->fetchAll($statement); //fetchAll(PDO::FETCH_ASSOC);

        if ($result === false) {
            return [];
        }

        return $result; //$result
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->link->lastInsertId();
    }
}