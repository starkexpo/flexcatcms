<?php

require_once __DIR__ . '/Libraries/vendor/autoload.php';

use FlexCat\Template\Engine;
use FlexCat\DI\DI;

class_alias('\FlexCat\Template\Theme', 'Core_Page');
class_alias('\FlexCat\Modules\Controller\PageController', 'Data_Page');
class_alias('\FlexCat\Admin\Modules\Controller\WidgetController', 'Widgets');
class_alias('\FlexCat\Modules\Controller\MenuController', 'Core_menu');
class_alias('\FlexCat\Template\Define', 'Define');
class_alias('\FlexCat\Modules\Controller\InfosysController', 'Core_infosys');
class_alias('\FlexCat\Modules\Controller\StatisticController', 'Core_statistic');
class_alias('\FlexCat\Modules\Controller\UsersSiteController', 'Core_users');

try {

    $di = new DI();

    $services = require __DIR__ . '/Config/Service.php';

    foreach ($services as $service) {
        $provider = new $service($di);
        $provider->init();
    }

    $define = new Engine($di);
    $define->run();

} catch (\ErrorException $e) {

    echo $e->getMessage();
}
