<?php

namespace FlexCat\Modules\Controller;


class HomeController extends CmsController
{
    public  function index()
    {
        $this->view->render('index');
    }
}
