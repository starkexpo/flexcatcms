<?php

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class MenuController
{
    public static function show($id)
    {
        if (isset($id) && $id > 0) {
            $queryBuilder = new QueryBuilder();

            $sql = "SELECT *, (SELECT COUNT(*) FROM menu_item t2 WHERE t2.parent = t1.id) AS counts  
            FROM menu_item t1 ORDER BY t1.id, t1.parent";

            $db = new Connection();

            return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
        }
        return false;
    }
}

