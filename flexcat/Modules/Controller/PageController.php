<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 08/01/2019
 * Time: 16:22
 */

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class PageController
{
    /*public function listing()
    {
        $this->load->model('Page');

//        $this->data['pages'] = $this->model->page->getPages();

        $this->data['pages'] = ['pages'=> 'Title page', 'id'=> '2'];


        $this->view->render('page', $this->data);
    }*/



    public static function page($id)
    {
        if (isset($id) && $id > 0) {
            $queryBuilder = new QueryBuilder();

            $sql = $queryBuilder->select()
                ->from('pages')
                ->where('id', $id)
                ->where('published', 'enable')
                ->sql();

            $db = new Connection();

            $pages = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);


            echo $pages[0]['content'];
        }

        return false;
    }
}