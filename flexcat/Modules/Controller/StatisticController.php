<?php

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class StatisticController extends CmsController
{
    public static function getUsersOnline()
    {
        @session_start();
        @clearstatcache();
        $SessionDir = session_save_path();
        $Timeout = 60 * 3;
        if ($Handler = scandir($SessionDir)) {
            $count = count($Handler);
            $users = 0;

            for ($i = 2; $i < $count; $i++) {
                if (time() - fileatime($SessionDir . '/' . $Handler[$i]) <
                    $Timeout) {
                    $users++;
                }
            }

            return $users;
        } else {
            return 'error';
        }
    }

    public static function setAllDownloads()
    {
        $path = \Core_Page::getUrl();
        $path = $_SERVER['DOCUMENT_ROOT'] . $path;
        $fileName = 'downloads.dat';

        if (file_exists($path . "/" . $fileName)) {
            $file = file_get_contents($path . "/" . $fileName);
            $allDownload = intval($file);
            $allDownload++;

            file_put_contents($path . "/" . $fileName, $allDownload);
        } else {
            file_put_contents($path . "/" . $fileName, 0);
            $allDownload = 0;
        }

    }

    public static function getAllDownloads()
    {
        $path = \Core_Page::getUrl();
        $path = $_SERVER['DOCUMENT_ROOT'] . $path;
        $fileName = 'downloads.dat';

        if (file_exists($path . "/" . $fileName)) {
            $file = file_get_contents($path . "/" . $fileName);
            $allDownload = intval($file);
        } else {
            file_put_contents($path . "/" . $fileName, 0);
            $allDownload = 0;
        }

        return $allDownload;
    }

    public static function getAllInfosys()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder->select('COUNT(*)')
            ->from('infosys')
            ->where('published', 'enable')
            ->where('type', 'release')
            ->sql();

        $count = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        foreach ($count[0] as $item) {
            $countNum = $item;
        }

        return $countNum;
    }

}