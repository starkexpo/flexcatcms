<?php

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;
use FlexCat\Helper\TestEmail;

class MailingController extends CmsController
{
    public function addMailing()
    {
        sleep(1);

        $queryBuilder = new QueryBuilder();
        $db = new Connection();
        $error = array();
        $errorSum = 0;
        $ajax = 0;

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $ajax++;
        }

        if (!isset($_POST['email']) || (TestEmail::verificationEmail(htmlspecialchars($_POST['email']))) == false) {
            $error[] = 'email';
            $errorSum++;
        }

        if (!isset($_POST['name']) || strlen($_POST['name']) <= 0) {
            $error[] = 'name';
            $errorSum++;
        }

        if ($errorSum > 0) {
            if ($ajax > 0) {
                echo json_encode($error);
            } else {
                foreach ($error as $item) {
                    switch ($item) {
                        case "email":
                            echo "<h1>Не правильно указан E-mail адрес</h1>";
                            break;
                        case "name":
                            echo "<h1>Не указано имя подписчика!</h1>";
                            break;
                    }
                }

                echo '<a href="' . $_SERVER['HTTP_REFERER'] . '">Вернутся назад</a>';
            }

            exit();
        }

        $email = trim(htmlspecialchars(stripslashes($_POST['email'])));
        $name = trim(htmlspecialchars(stripslashes($_POST['name'])));
        $mailing = trim(htmlspecialchars(stripcslashes($_POST['mailing'])));


        $sql = $queryBuilder
            ->select()
            ->from('mailing_users')
            ->where("email", $email)
            ->where('mailing_id', $mailing)
            ->sql();

        $subscribers = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        foreach ($subscribers as $subscriber) {
            if ($ajax > 0) {
                $error[] = 'subscribe';
                echo json_encode($error);
                exit();
            } else {
                echo "<h1>На данный E-mail адрес подписка уже оформлена!</h1>";
                echo '<a href="' . $_SERVER['HTTP_REFERER'] . '">Вернутся назад</a>';
                exit();
            }
        }

        $i = 1;
        do {
            $randkey = mt_rand(10000, 99999);
            if (strlen($randkey) == 5) {
                $i++;
            }
        } while ($i <= 1);

        $randkey = $randkey . mt_rand(10000, 99999);
        $randkey = $randkey . mt_rand(10000, 99999);
        $randkey = $randkey . mt_rand(10000, 99999);


        $db->query("INSERT INTO mailing_users (name, email, mailing_id, code) VALUES('$name', '$email', '$mailing', '$randkey')");

        $sqlMail = $queryBuilder
            ->select()
            ->from('mailing')
            ->where('id', $mailing)
            ->sql();
        $mailSubscribe = $db->query($sqlMail, $queryBuilder->values, \PDO::FETCH_ASSOC);


        $from = $mailSubscribe[0]['nameFrom'] . "<" . $mailSubscribe[0]['emailFrom'] . ">";
        $headers = "Content-type: text/plain; charset=utf-8\r\n";
        $headers .= "From: $from \r\n";

        $letter = $mailSubscribe[0]['mailTemplate'];

        if (strlen($name) > 0 && $name != "user") {
            $nameHelo = $name;
        } else {
            $nameHelo = $email;
        }

        $letter = strtr($letter, [
            "{NAME}" => $nameHelo,
            "{EMAIL}" => $email,
        ]);

        $letterText = <<<LTR
        $letter
LTR;

        mail($email, $mailSubscribe[0]['theme'], $letterText, $headers);


        if ($ajax > 0) {
            $error[] = 'ok';
            echo json_encode($error);
        } else {
            echo "<h1>Подписка успешно оформлена!</h1>";
            echo '<a href="' . $_SERVER['HTTP_REFERER'] . '">Вернутся назад</a>';
        }
    }

    public function unSubscribe($code)
    {
        $this->view->render('index');

        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sqlUsers = $queryBuilder
            ->select()
            ->from('mailing_users')
            ->where('code', $code)
            ->sql();

        $mailCodes = $db->query($sqlUsers, $queryBuilder->values, \PDO::FETCH_ASSOC);

        if (!empty($mailCodes)) {

            $idSubscribe = $mailCodes[0]['id'];

            $sqlUs = $queryBuilder
                ->delete('mailing_users')
                ->where('id', $idSubscribe)
                ->sql();

            $db->query($sqlUs, $queryBuilder->values, \PDO::FETCH_ASSOC);

            echo '<script>
                $(".errorsBlocks").html("").prepend("<h1>Вы отписаны от рассылки!</h1>").css({
                alignContent: "center",
                textAlign: "center",
                width: "100%",
                padding: "20px"
                });
              </script>';
        }
    }
}