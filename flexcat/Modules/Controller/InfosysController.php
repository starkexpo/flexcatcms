<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 06/03/2019
 * Time: 22:42
 */

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;
use FlexCat\Template\Theme;

class InfosysController extends CmsController
{

    public static function show($id, $limit = '', $order = '')
    {
        if ($id == '#') {
            $data = self::getDataID($id);

            $id = $data['id'];
            $limit = $data['limit'];
        }

        if (isset($id) && $id > 0) {
            $queryBuilder = new QueryBuilder();

            if ($order == '') {
                $order = 'DESC';
            } elseif ($order == 'DESC') {
                $order = 'DESC';
            } else {
                $order = 'ASC';
            }

            $page = self::getNowPage();
            $num = $limit;
            $posts = self::countList($id);
            $total = (($posts - 1) / $num) + 1;
            $total = @intval($total);
            $start = $page * $num - $num;

            if ($limit == '' || $limit <= 0) {
                $sql = $queryBuilder->select()
                    ->from('infosys')
                    ->where('section', $id)
                    ->where('published', 'enable')
                    ->where('type', 'release')
                    ->orderBy('id', $order)
                    ->sql();
            } elseif ($limit > 0) {
                $sql = $queryBuilder->select()
                    ->from('infosys')
                    ->where('section', $id)
                    ->where('published', 'enable')
                    ->where('type', 'release')
                    ->orderBy('id', $order)
                    ->limit($start, $limit)
                    ->sql();
            }

            $db = new Connection();
            return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
        }
        return false;
    }

    public static function getView()
    {
        $pageID = self::getNowPage();

        $queryBuilder = new QueryBuilder();

        if (is_numeric($pageID)) {
            $sql = $queryBuilder->select()
                ->from('infosys')
                ->where('id', $pageID)
                ->where('published', 'enable')
                ->where('type', 'release')
                ->sql();
        } else {
            $sql = $queryBuilder->select()
                ->from('infosys')
                ->where('alias', $pageID)
                ->where('published', 'enable')
                ->where('type', 'release')
                ->sql();
        }

        $db = new Connection();

        $array = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        if (!empty($array)) {

            return $array;
        }
        return false;
    }

    public static function getInfosys($id)
    {
        if ($id > 0) {
            $queryBuilder = new QueryBuilder();

            $sql = $queryBuilder->select()
                ->from('infosys')
                ->where('id', $id)
                ->where('type', 'info')
                ->sql();

            $db = new Connection();

            return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
        }
    }

    public static function showPagination($id, $limit = 1)
    {
        if ($id == '#') {
            $data = self::getDataID($id);

            $id = $data['id'];
            $limit = $data['limit'];
        }

        $list = self::countList($id);
        $countAll = $list;

        $link = json_decode(\Core_Page::getNowItem());
        $url = $link->path;

        $page = self::getNowPage();

        $num = $limit;
        $posts = $countAll;
        $total = (($posts - 1) / $num) + 1;
        $total = @intval($total);
        $page = @intval($page);

        if (empty($page) or $page < 0) {
            $page = 1;
        }
        if ($page > $total) {
            $page = $total;
        }

        $start = $page * $num - $num;

        $page5left = '';
        $page4left = '';
        $page3left = '';
        $page2left = '';
        $page1left = '';
        $page5right = '';
        $page4right = '';
        $page3right = '';
        $page2right = '';
        $page1right = '';

        if ($page - 5 > 0) {
            $page5left = '<li><a href=' . $url . '/' . ($page - 5) . '>' .
                ($page - 5) . '</a></li>';
        }
        if ($page - 4 > 0) {
            $page4left = '<li><a href=' . $url . '/' . ($page - 4) . '>' .
                ($page - 4) . '</a></li>';
        }
        if ($page - 3 > 0) {
            $page3left = '<li><a href=' . $url . '/' . ($page - 3) . '>' .
                ($page - 3) . '</a></li>';
        }
        if ($page - 2 > 0) {
            $page2left = '<li><a href=' . $url . '/' . ($page - 2) . '>' .
                ($page - 2) . '</a></li>';
        }
        if ($page - 1 > 0) {
            $page1left = '<li><a href=' . $url . '/' . ($page - 1) . '>' .
                ($page - 1) . '</a></li>';
        }
        if ($page + 5 <= $total) {
            $page5right = '<li><a href=' . $url . '/' . ($page + 5) . '>' .
                ($page + 5) . '</a></li>';
        }
        if ($page + 4 <= $total) {
            $page4right = '<li><a href=' . $url . '/' . ($page + 4) . '>' .
                ($page + 4) . '</a></li>';
        }
        if ($page + 3 <= $total) {
            $page3right = '<li><a href=' . $url . '/' . ($page + 3) . '>' .
                ($page + 3) . '</a></li>';
        }
        if ($page + 2 <= $total) {
            $page2right = '<li><a href=' . $url . '/' . ($page + 2) . '>' .
                ($page + 2) . '</a></li>';
        }
        if ($page + 1 <= $total) {
            $page1right = '<li><a href=' . $url . '/' . ($page + 1) . '>' .
                ($page + 1) . '</a></li>';
        }
        if ($total > 1) {
            echo $page5left . $page4left . $page3left . $page2left .
                $page1left . '<li class="pagination-active"><a>' . $page .
                '</a></li>' . $page1right . $page2right . $page3right .
                $page4right . $page5right;
        }
    }

    protected static function countList($id)
    {
        if (isset($id) && $id > 0) {
            $queryBuilder = new QueryBuilder();

            $sql = $queryBuilder->select('COUNT(*)')
                ->from('infosys')
                ->where('section', $id)
                ->where('published', 'enable')
                ->where('type', 'release')
                ->sql();

            $db = new Connection();

            $all = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

            foreach ($all[0] as $item) {
                $count = $item;
            }
            $count = intval($count);

            return intval($count);
        }

        return false;
    }

    protected static function getNowPage()
    {
        $uri = $_SERVER['REQUEST_URI'];
        $pageID = @preg_replace('/\/[a-zA-Z-_]*\//', '', $uri);
        $pageID = @preg_match('/[0-9]*/', $pageID, $matches);

        if (!empty($matches)) {
            $page = intval($matches[0]);
        }
        if (!isset($page) || $page == '' || $page == 0) {
            $page = 1;
        }

        return $page;
    }

    protected static function getDataID($id)
    {
        $nowID = \Core_Page::getNowItem();
        $nowID = json_decode($nowID);

        $optionVals = base64_decode($nowID->options);
        $optionVals = unserialize($optionVals);

        foreach ($optionVals as $val) {

            if ($val['field_type'] == 'sql') {
                $id = $val['value'];
            }
            if ($val['field_type'] == 'input') {
                $limit = $val['value'];
            }
        }

        return $data[] = ["id" => $id, "limit" => $limit];
    }

    public static function setCountView()
    {
        $pageID = self::getNowPage();
        $queryBuilder = new QueryBuilder();
        $link = '';
        $downloads = 0;
        $show = 0;

        $sql = $queryBuilder->select()
            ->from('infosys')
            ->where('id', $pageID)
            ->sql();

        $db = new Connection();
        $page = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        $options = base64_decode($page[0]['options']);
        $options = unserialize($options);

        foreach ($options as $option) {

            if (isset($option['views'])) {
                $views = intval($option['views']);
                $views =  $views  + 1;
                $data[0] = ['views' => $views];
                $show++;
            }
            if (isset($option['downloads'])) {
                $downloads = $option['downloads'];
                $data[1] = ['downloads' => $downloads];
            }
            if (isset($option['link'])) {
                $link = $option['link'];
                $data[2] = ['link' => $link];
            }
        }

        if ($show == 0) {
            $views = 1;
            $data[0] = ['views' => $views];
        }

        $data = serialize($data);
        $data = base64_encode($data);
        $pageData['options'] = $data;

        $sql = $queryBuilder->update('infosys')
            ->set($pageData)
            ->where('id', $pageID)
            ->sql();

        $db->query($sql, $queryBuilder->values);
    }

    public static function getBreadcrumbs()
    {
//        $breadcrumbs = '';
//        $breadcrumbs .= '<li><a href="' . $_SERVER['HTTP_HOST'] . '">Главная</a></li>';
//        $pageID = self::getNowPage();
//        $nowPage = self::getView();
//        $parentID = $nowPage[0]['section'];
//        $itemMenu = \FlexCat\Template\Builder::getListItem();
//        $itemID = 0;

        /*foreach ($itemMenu as $item) {
//            var_dump($item);
            if (!empty($item['options'])) {
                $options = base64_decode($item['options']);
                $options = unserialize($options);

                foreach ($options as $option) {
                    if ($option['field_type'] == 'sql' && $option['value'] == $parentID) {
//                        echo "<hr>";
//                        echo "itemID: " . $item['id'];
                        $itemID = $item['id'];
//                        var_dump($option);
                    }
                }
//                var_dump($options);
            }
        }*/

//        var_dump($pageID);
//        var_dump($nowPage[0]['section']);
//        var_dump($itemMenu);

//        return $breadcrumbs;
    }

    public static function getTitle()
    {
        $page = self::getView();

        $data['title'] = $page[0]['title'];
        $data['seo_title'] = $page[0]['seo_title'];
        $data['seo_description'] = $page[0]['seo_description'];
        $data['seo_keywords'] = $page[0]['seo_keywords'];

        if (!empty($data)) {
            return $data;
        }
        return false;
    }

    public function setDownloadCount()
    {
        $params = $this->request->post;
        foreach ($params as $key => $param) {
            $pageID = $key;
        }
//        echo $pageID;

        $queryBuilder = new QueryBuilder();
        $link = '';
        $views = 0;
        $downloads = 0;

        $sql = $queryBuilder->select()
            ->from('infosys')
            ->where('id', $pageID)
            ->sql();

        $db = new Connection();
        $page = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        $options = base64_decode($page[0]['options']);
        $options = unserialize($options);

        foreach ($options as $option) {
            if (isset($option['views'])) {
                $views = $option['views'];
                $data[0] = ['views' => $views];
            }
            if (isset($option['downloads'])) {
                $downloads = intval($option['downloads']) + 1;
                $data[1] = ['downloads' => $downloads];
            }
            if (isset($option['link'])) {
                $link = $option['link'];
                $data[2] = ['link' => $link];
            }
        }

        $data = serialize($data);
        $data = base64_encode($data);
        $pageData['options'] = $data;

        $sql = $queryBuilder->update('infosys')
            ->set($pageData)
            ->where('id', $pageID)
            ->sql();

        $db->query($sql, $queryBuilder->values);

        $allDownloads = \Core_statistic::setAllDownloads();

        echo $downloads;
    }

    public function downloadFile()
    {
        $params = $this->request->post;
        foreach ($params as $key => $param) {
            $pageID = $key;
        }
        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder->select()
            ->from('infosys')
            ->where('id', $pageID)
            ->where('published', 'enable')
            ->where('type', 'release')
            ->sql();

        $db = new Connection();
        $array = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

        $options = base64_decode($array[0]['options']);
        $options = unserialize($options);
        $downloads = 0;

        foreach ($options as $option) {
            if (isset($option['link'])) {
                echo '<a href="' . $option['link'] . '" class="button" id="link-downloads" target="_blank">Скачать</a>';
//                echo $option['link'];
                $links = $option['link'];
            }
        }

//        $allDownloads = \Core_statistic::setAllDownloads();
    }

    public static function lastPages($limit = '')
    {
        if (!isset($limit) || $limit == '') {
            $limit = 10;
        }

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder->select()
            ->from('infosys')
            ->where('published', 'enable')
            ->where('type', 'release')
            ->orderBy('id', 'DESC')
            ->limit($limit)
            ->sql();

        $db = new Connection();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function findes($search)
    {
        $sql = "SELECT * FROM  infosys WHERE MATCH(description) AGAINST('$search') 
                OR MATCH(title) AGAINST('$search') 
                AND type = 'release'
                AND published = 'enable'";
        $db = new Connection();

        return $db->query($sql);
    }
}