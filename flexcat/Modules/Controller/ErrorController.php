<?php

namespace FlexCat\Modules\Controller;

class ErrorController extends CmsController
{
    public function page404()
    {
        header("HTTP/1.0 404 Not Found");
        $this->view->render('index');
    }

    public static function pageError404()
    {

    }
}