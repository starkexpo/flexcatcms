<?php

namespace FlexCat\Modules\Controller;

use FlexCat\Database\Connection;
use FlexCat\Database\QueryBuilder;
use FlexCat\Functions\UploadFile;
use FlexCat\Helper\TestEmail;

class UsersSiteController extends CmsController
{
    public function register()
    {
        sleep(1);
        $params = $this->request->post;
        $usersBase = $this->getAllUsers();

        $role = "user";
        $login = htmlspecialchars(trim($params['login']));
        $login = stripcslashes($login);

        $email = htmlspecialchars(trim($params['email']));
        $testMail = TestEmail::verificationEmail($email);

        $password = htmlspecialchars(trim($params['password']));
        $password = stripcslashes($password);

        $password2 = htmlspecialchars(trim($params['password2']));
        $password2 = stripcslashes($password2);

        $name = htmlspecialchars(trim($params['name']));
        $name = stripcslashes($name);

        $lastName = htmlspecialchars(trim($params['lastName']));
        $lastName = stripcslashes($lastName);

        $dadname = htmlspecialchars(trim($params['dadname']));
        $dadname = stripcslashes($dadname);

        $sex = htmlspecialchars(trim($params['sex']));
        $sex = stripcslashes($sex);

        $db = new Connection();

        if ($testMail === false) {
            $error[] = 'mail';
        }

        if (strlen($login) <= 3) {
            $error[] = 'login';
        }

        if ($password <> $password2 || strlen($password) <= 5) {
            $error[] = 'password';
            $error[] = 'password2';
        }

        if ($sex != 'man' && $sex != 'woman') {
            $error[] = 'sex';
        }

        foreach ($usersBase as $item) {
            if ($item['email'] == $params['email']) {
                $error[] = 'email';
            }
            if ($item['login'] == $params['login']) {
                $error[] = 'login';
            }
        }





        if (!empty($error)) {
            echo json_encode($error);
            exit();
        }

        $password = md5($password);

        $sql = "INSERT INTO users (login, email, password, role, firstname, lastname, dadname, sex) 
        VALUES ('$login', '$email', '$password', '$role', '$name', '$lastName', '$dadname', '$sex')";


        $db->query($sql);

        $ok[] = "goodRegistration";
        echo json_encode($ok);
    }

    protected function getAllUsers()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder->select()
            ->from('users')
            ->where('role', 'user')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }


    protected function getAllListUsers()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder->select()
            ->from('users')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public function validEmail()
    {
        $params = $this->request->post;


        /*$valid = TestEmail::verificationEmail($id);

        if ($valid === true) {
            echo "OK";
        } else {
            echo "ErrorEmail";
        }*/
    }

    public function authorize()
    {
        sleep(1);
        $params = $this->request->post;

        $email = htmlspecialchars(trim($params['email']));
        $testMail = TestEmail::verificationEmail($email);

        $password = htmlspecialchars(trim($params['password']));
        $password = stripcslashes($password);

        if ($testMail === false || strlen($password) <= 4) {
            $error[] = 'mail';
            $error[] = 'password';

            echo json_encode($error);
            exit();
        }

        @session_start();
        $queryBuilder = new QueryBuilder();
        $db = new Connection();
        $password = md5($password);

        $sql = $queryBuilder->select()
            ->from('users')
            ->where('email', $email)
            ->where('password', $password)
            ->where('activate', '1')
            ->sql();

        $user = $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);

//        var_dump($user);

        if (!empty($user)) {
            $_SESSION['email'] = $email;
            $_SESSION['password'] = $password;
            $_SESSION['id'] = $user[0]['id'];

            $reload[] = 'reload';
            echo json_encode($reload);
        } else {
            $error[] = 'email';
            $error[] = 'password';

            echo json_encode($error);
            exit();
        }
    }

    public static function getUser($id)
    {
        if (!isset($id)) {
            return false;
        }
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder->select()
            ->from('users')
            ->where('id', $id)
            ->where('role', 'user')
            ->sql();

        return $db->query($sql, $queryBuilder->values);
    }

    public static function getAvatar()
    {
        if (isset($_SESSION['id'])) {
            $user = self::getUser($_SESSION['id']);
            $user = $user[0];

            if (strlen($user->photo) <= 0) {
                $array['avatar'] = '<i class="icofont-ui-user block-user-avatar-ico avatar_icon"></i>';
                $array['link'] = '<a href="#LoadAvatar" class="show-button-loadAvatar avatar_links"><i class="icofont-ui-edit"></i></a>';
                $array['form'] = '<div id="LoadAvatar" class="hide"><label for="formTitle" class="label_avatar">аватарка</label><br>
            <input name="file" type="file" accept="image/*">
            <div class="ajax-reply hide"></div></div><br>';


            } else {
                $array['avatar'] = '<img src="/upload/img/avatar/' .
                    $user->photo . '" alt="' . $user->login .
                    '" class="block-user-avatar">';
                $array['link'] = '<a href="/delete_avatar" class="block-user-delAvatar"><i class="icofont-close-circled"></i></a>';
                $array['form'] = '';
            }

            $array['id'] = $user->id;
            $array['login'] = $user->login;
            $array['email'] = $user->email;
            $array['img'] = $user->photo;

            return $array;
        } else {

            return false;
        }
    }

    public static function testAuthorization()
    {
        @session_start();
        if (isset($_SESSION['email']) || isset($_SESSION['password']) ||
            isset($_SESSION['id'])) {
            return true;
        } else {
            return false;
        }
    }

    public function avatar()
    {
        if (isset($_POST['my_file_upload'])) {
            $files = $_FILES[0]; // полученные файлы

            $upload = new UploadFile();
            $filename = $upload->uploadImage($files, "avatar");
            $data['photo'] = $filename;

            $queryBuilder = new QueryBuilder();
            $db = new Connection();

            $sql = $queryBuilder->update('users')
                ->set($data)
                ->where('id', $_SESSION['id'])
                ->sql();

            $db->query($sql, $queryBuilder->values);

            echo json_encode($filename);
        }
    }

    public function deleteAvatar()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();
        $data['photo'] = '';

        $user = self::getUser($_SESSION['id']);
        $user = $user[0];

        unlink($_SERVER['DOCUMENT_ROOT'] . '/upload/img/avatar/' . $user->photo);

        $sql = $queryBuilder->update('users')
            ->set($data)
            ->where('id', $user->id)
            ->sql();

        $db->query($sql, $queryBuilder->values);

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function logout()
    {
        @session_start();
        unset($_SESSION['email']);
        unset($_SESSION['password']);
        unset($_SESSION['id']);

        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public function updateLogin()
    {
        sleep(1);
        $params = $this->request->post;

        foreach ($params as $key => $param) {
            $newName = $key;
        }
//        echo $newName;
        $listUsers = $this->getAllListUsers();
        $count = 0;

        foreach ($listUsers as $listUser) {
            if ($listUser['login'] == $newName) {
                $count++;
            }
        }

        if ($count > 0) {
            echo "error";
        } else {
            $queryBuilder = new QueryBuilder();
            $db = new Connection();

            $data['login'] = $newName;

            $sql = $queryBuilder->update('users')
                ->set($data)
                ->where('id', $_SESSION['id'])
                ->sql();

            $db->query($sql, $queryBuilder->values);

            echo "save";
        }
    }
}