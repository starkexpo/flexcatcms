<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 03/03/2019
 * Time: 01:35
 */

namespace FlexCat\Modules\Controller;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class CallRequestController extends CmsController
{
    public function addCall()
    {
        sleep(1);

        $queryBuilder = new QueryBuilder();
        $db = new Connection();
        $error = array();
        $errorSum = 0;
        $ajax = 0;

        if (!isset($_POST['name']) || strlen($_POST['name']) <= 0) {
            $error[] = 'name';
            $errorSum++;
        }

        if (!isset($_POST['phone']) || strlen($_POST['phone']) <= 6) {
            $error[] = 'phone';
            $errorSum++;
        }

        if ($errorSum <= 0) {
            $phone = trim(htmlspecialchars(stripslashes($_POST['phone'])));
            $name = trim(htmlspecialchars(stripslashes($_POST['name'])));

            $db->query("INSERT INTO call_request (name, phone) VALUES('$name', '$phone')");

            $error[] = 'ok';
            echo json_encode($error);
            exit();
        } else {
            echo json_encode($error);
            exit();
        }
    }
}