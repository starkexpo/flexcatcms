<?php

namespace FlexCat\Modules\Controller;

use FlexCat\Template\Controller;

class CmsController extends Controller
{
    /**
     * HomeController constructor.
     * @param $di
     */
    public function __construct($di)
    {
        parent::__construct($di);
    }

    public $data = [];


    //Подключаем шаблон
    public function header()
    {
        $this->view->render('header');
    }

    public function footer()
    {
        $this->view->render('footer');
    }
}