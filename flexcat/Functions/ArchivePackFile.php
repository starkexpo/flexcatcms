<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 04/02/2019
 * Time: 23:50
 */

namespace FlexCat\Functions;

use function Couchbase\fastlzDecompress;

class ArchivePackFile
{
    /**
     * @param $source
     * @param $destination
     * @return bool
     */
    public function ArchiveZip($source, $destination){
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new \ZipArchive();
        if (!$zip->open($destination, \ZipArchive::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true){
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file){
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);
                $file = str_replace('\\', '/', $file);

                if (is_dir($file) === true){
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                }else if (is_file($file) === true){
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        }else if (is_file($source) === true){
            $zip->addFromString(basename($source), file_get_contents($source));
        }
        return $zip->close();
    }



    public function unArchive($pathFile, $extract)
    {
//        $file = 'flexcat.zip';
//        $path = $_SERVER['DOCUMENT_ROOT'] . "/upload/";

        try {
            $zip = new \ZipArchive();
            $zip->open($pathFile);
            $zip->extractTo($extract);
            $zip->close();
        } catch (\Exception $e) {
            echo "Невозможно извлеч архив (или архив не найден) ", $e;
        }


    }
}