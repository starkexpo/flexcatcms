<?php

namespace FlexCat\Functions;

class UploadFile
{
    /**
     * @param $filename
     * @param $dir
     * @param string $path
     * @return bool|string
     */
    public function uploadImage($filename, $dir, $path = '')
    {
        if ($path == '') {
            $pathDefault = require $_SERVER['DOCUMENT_ROOT'] . '/config/main.php';
            $path = $_SERVER['DOCUMENT_ROOT'] . "/" . $pathDefault['upload_folder'] . "/" . "img/";
        }

        $filePath = $path . $dir . "/";

        if (@chdir($filePath) === false) {
            @chdir($path);
            @mkdir($dir, 0777);
        }

        $tmp = $filename['tmp_name'];

        if (is_uploaded_file($filename['tmp_name'])) {

            if (preg_match('{image/(.*)}is', $filename['type'], $p)) {

                switch ($p[1]) {
                    case "jpeg":
                        $typeImage = 'jpg';
                        break;
                    case "svg+xml":
                        $typeImage = 'svg';
                        break;
                    case "png":
                        $typeImage = 'png';
                        break;
                    case "gif":
                        $typeImage = 'gif';
                        break;
                    case "x-icon":
                        $typeImage = 'ico';
                        break;
                    default:
                        $typeImage = $p[1];
                }

                $newName = "$dir" . time() . rand(1111, 9999) . "." . $typeImage;
                $upload = $filePath . $newName;

                move_uploaded_file($tmp, $upload);

                return $newName;
            } else {

                return "This file format " . $filename['type'] . " is invalid !" . false;
            }
        }

        return false;
    }
}