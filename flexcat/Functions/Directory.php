<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 05/02/2019
 * Time: 00:19
 */

namespace FlexCat\Functions;

class Directory
{
    public function remove($dir)
    {
        if ($objs = glob($dir."/*")) {
            foreach($objs as $obj) {
                is_dir($obj) ? $this->remove($obj) : unlink($obj);
            }
        }
        rmdir($dir);
    }
}