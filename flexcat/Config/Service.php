<?php

return [
    FlexCat\Service\Database\Provider::class,
    FlexCat\Service\Router\Provider::class,
    FlexCat\Service\View\Provider::class,
    FlexCat\Service\Config\Provider::class,
    FlexCat\Service\Request\Provider::class,
    FlexCat\Service\Load\Provider::class
];