<?php

namespace FlexCat\Auth;

use FlexCat\Helper\Cookie;
use FlexCat\Helper\Session;

class Auth implements AuthInterface
{
    /**
     * @var bool
     */
    protected $authorized = false;
    protected $hash_user;
    protected $id;

    protected $user;

    /**
     * @return bool
     * возвращает статус авторизации (авторизовани или нет)
     */
    public function authorized()
    {
        return $this->authorized;
    }

    public function hashUser()
    {

        return Session::get('auth_user');
    }


    /**
     * @param $user
     * авторизовывает пользователя
     */
    public function authorize($user, $login, $id)
    {
        Session::set('auth_authorized', true);
        Session::set('auth_user', $user);
        Session::set('id', $id);

//        $this->authorized = true;
//        $this->hash_user = $user;
    }


    public function unAuthorize()
    {
        Session::delete('auth_authorized');
        Session::delete('auth_user');
        Session::delete('id');

//        $this->authorized = false;
//        $this->user = null;
    }

    /**
     * @return string
     */
    public static function salt()
    {
        return (string)rand(10000000, 99999999);
    }

    /**
     * @param $password
     * @param string $salt
     * @return string
     */
    public static function encryptPassword($password, $salt = '')
    {
        return hash('sha256', $password . $salt);
    }
}