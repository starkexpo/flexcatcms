<?php

namespace FlexCat\Template;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class DefaultTheme
{
    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var Connection
     */
    protected $db;

    /**
     * DefaultTheme constructor.
     */
    public function __construct()
    {
        $this->queryBuilder = new QueryBuilder();

        $this->db = new Connection();
    }


    /**
     * @return string
     */
    public function getDefault()
    {
        $sql = $this->queryBuilder
            ->select()
            ->from('sites')
            ->where("defaults", 'enable')
            ->limit(1)
            ->sql();

        $sites = $this->db->query($sql, $this->queryBuilder->values, \PDO::FETCH_ASSOC);

        $defaultTheme = "default";



        if (!empty($sites)) {

            if ($sites[0]['template'] > 0) {
                $sql = $this->queryBuilder
                    ->select()
                    ->from('sites_templates')
                    ->where("id", $sites[0]['template'])
                    ->limit(1)
                    ->sql();

                $defaulTemplate = $this->db->query($sql,
                    $this->queryBuilder->values, \PDO::FETCH_ASSOC);

                $defaultTheme = $defaulTemplate[0]['folder'];
            }

            return $defaultTheme;
        }
    }
}