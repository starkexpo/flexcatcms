<?php

namespace FlexCat\Template;

use FlexCat\Http\Router\DispatchedRoute;
use FlexCat\Helper\Common;

class Engine
{
    /**
     * @var
     */
    private $di;
    private $dispatcher;
    public $router;

    /**
     * Engine constructor.
     * @param $di
     */
    public function __construct($di)
    {
        $this->di = $di;
        $this->router = $this->di->get('router');
    }

    /**
     * Run CMS
     */
    public function run()
    {
        try {
            switch (mb_strtolower(ENV)) {
                case "flexcat":
                    require_once ROOT_DIR . '/flexcat/Routing/site/Route.php';
                    $env = '';
                    break;

                default:
                    require_once  '../Routing/admin/Route.php';
                    $env = 'FlexCat';
            }

            $routerDispatch = $this->router->dispatch(Common::getMethod(), Common::getPathUrl());


            if ($routerDispatch == null) {
                $routerDispatch = new DispatchedRoute('ErrorController:page404');
            }

            list($class, $action) = explode(':', $routerDispatch->getController(), 2);

            $controller = $env . '\\' . ENV . '\\Modules\\Controller\\' . $class;
            $parameters = $routerDispatch->getParameters();

            call_user_func_array([new $controller($this->di), $action], $parameters);
        } catch (\Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}


