<?php

namespace FlexCat\Template;

class Define
{
    const NAME    = 'FlexCatCMS';
    const VERSION = '1.0.2';
    const EXEC    = true;
    const PHP_MIN = '7.0.0';

    const DEFAULT_MODULE = [
        'Backend' => 'Backend',
        'Frontend' => 'Frontend'
    ];
}