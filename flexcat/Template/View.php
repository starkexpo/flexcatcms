<?php

namespace FlexCat\Template;


class View
{
    /**
     * @var \FlexCat\Template\Theme
     */
    protected $theme;

    protected static $defaulUrl;

    /**
     * Template constructor.
     */
    public function __construct()
    {
        $theme = new Theme();
        $this->theme = $theme;

        self::$defaulUrl = $theme::getUrl();
    }

    /**
     * @param $template
     * @param array $vars
     * @throws \Exception
     */
    public function render($template, $vars = [])
    {
        $templatePath = $this->getTemplatePath($template, ENV);

        if (!is_file($templatePath)) {

            throw new \InvalidArgumentException(
                sprintf('Template "%s" not found in "%s"', $template, $templatePath)
            );
        }


        $this->theme->setData($vars);
        extract($vars);

        ob_start();
        ob_implicit_flush(0);

        try {
            require $templatePath;


        } catch (\Exception $e) {
            ob_end_clean();
            throw $e;
        }

        echo ob_get_clean();
    }



    /**
     * @param $template
     * @param null $env
     * @return string
     */
    private function getTemplatePath($template, $env = null)
    {

        if ($env == 'FlexCat') {

            return  ROOT_DIR .   self::$defaulUrl . "/" . $template . '.php';
        }

        return ROOT_DIR . '/Template/' . $template . '.php';
    }
}