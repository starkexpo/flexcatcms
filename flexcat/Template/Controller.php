<?php

namespace FlexCat\Template;

use FlexCat\DI\DI;

abstract class Controller
{
    /**
     * Controller constructor.
     * @param $di
     * @var \FlexCat\DI\DI
     */
    protected $di;
    protected $db;
    protected $view;
    protected $config;
    protected $request;
    protected $load;


    /**
     * Controller constructor.
     * @param DI $di
     */
    public function __construct(DI $di)
    {
        $this->di = $di;


        $this->initVars();
    }

    /**
     * @param $key
     * @return bool
     */
    public function __get($key)
    {
        return $this->di->get($key);
    }

    /**
     * @return $this
     */
    public function initVars()
    {
        $vars = array_keys(get_object_vars($this));

        foreach ($vars as $var) {
            if ($this->di->has($var)) {
                $this->{$var} = $this->di->get($var);
            }
        }

        return $this;
    }

}