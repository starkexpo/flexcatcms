<?php
/**
 * Created by PhpStorm.
 * User: sergejerov
 * Date: 14/01/2019
 * Time: 16:04
 */

namespace FlexCat\Template;

use FlexCat\Database\QueryBuilder;
use FlexCat\Database\Connection;

class Builder
{

    public static function getDefaultSite()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder
            ->select()
            ->from('sites')
            ->where('defaults', 'enable')
            ->orderBy('id', 'ASC')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function getListMenu()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $site = self::getDefaultSite();

        $sql = $queryBuilder
            ->select()
            ->from('menu')
            ->where('site_id', $site[0]['id'])
            ->orderBy('id', 'ASC')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function getListItem()
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder
            ->select()
            ->from('menu_item')
            ->where('published', 'enable')
            ->orderBy('id', 'ASC')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function getPage($id)
    {
        $queryBuilder = new QueryBuilder();
        $db = new Connection();

        $sql = $queryBuilder
            ->select()
            ->from('pages')
            ->where('published', 'enable')
            ->where('id', $id)
            ->orderBy('id', 'ASC')
            ->sql();

        return $db->query($sql, $queryBuilder->values, \PDO::FETCH_ASSOC);
    }

    public static function showPage($page_id)
    {
            $itemPage = self::getPage($page_id);
            foreach ($itemPage as $page) {
                echo $page['content'];
            }
    }



}