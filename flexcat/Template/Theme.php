<?php


namespace FlexCat\Template;

class Theme
{
    const RULES_NAME_FILE = [
        'header' => 'header-%s',
        'footer' => 'footer-%s',
        'sidebar' => 'sidebar-%s'
    ];

    const URL_THEME_MASK = '/Template/Theme/%s';

    /**
     * @var string
     */
    protected static $url = '';

    /**
     * @var array
     */
    protected $data = [];

    protected static $execute;


    /**
     * @return string
     */
    public static function getUrl()
    {
        $defaults = new DefaultTheme();

        return sprintf(self::URL_THEME_MASK, $defaults->getDefault());
    }


   public static function getNowItem()
    {
        $layouts = null;
        $nowItem = array();

        $itemMenu = \FlexCat\Template\Builder::getListItem();
        $uri = $_SERVER['REQUEST_URI'];
        $menuList = \FlexCat\Template\Builder::getListMenu();


//        $replace = @preg_match('/\/[a-zA-Z]*-*[a-zA-Z]*\//', $uri, $matches);
//        if (empty($matches)) {
            $replace = @preg_match('/\/[a-zA-Z-_]*/', $uri, $matches);
//        }


        $replace = @preg_replace('/\//', '', $matches[0]);
        if (strlen($replace) <= 0 || $replace == '') {
            $replace = @preg_replace('/\//', '', $uri);
        }

        $j = 0;

        foreach ($itemMenu as $item) {
            /*if (preg_match("!" . $item['path'] . "!si", $uri) && $menuList[0]['id'] == $item['menu']) {
                $layouts = $item['layouts'];
                $nowItem = $item;
            }*/

            $pattern = @preg_replace('/\//', '', $item['path']);

            if ($pattern == $replace   && strlen($replace) > 0) {
                $layouts = $item['layouts'];
                $nowItem = $item;
                $j++;
            }

            $i = 0;

            foreach ($menuList as $menu) {
                if ($item['menu'] == $menu['id']) {
                    $siteID = $menu['site_id'];
                    $i++;
                }
            }

            if ($item['path'] == $uri && $i > 0) {
                $layouts = $item['layouts'];
                $nowItem = $item;
            }
        }

        if ($j <= 0 && $uri !== '/') {
            $layouts = null;
            $nowItem = array();
        }


        if (strlen($layouts) <= 0 or $layouts == null) {
            $sitePageError = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getSiteNow($siteID);

            foreach ($itemMenu as $item) {
                if ($item['id'] == $sitePageError->page404) {

                    $layouts = $item['layouts'];
                    $nowItem = $item;
                }
            }
        }
//        var_dump($nowItem);
//        var_dump($layouts);

        return json_encode($nowItem, $layouts);
    }

    public static function execute()
    {
        $nowItem = json_decode(self::getNowItem());

        $layoutsLoad = @require_once $_SERVER['DOCUMENT_ROOT'] . self::getUrl() . "/layouts" . $nowItem->layouts . '.php';

        if ($layoutsLoad === true) {

            if ($nowItem->typeItem == "page") {
                $pageList = \FlexCat\Template\Builder::showPage($nowItem->page_id);
            }

            if ($nowItem->typeItem == "dynamic") {
                $optionList = json_decode(\FlexCat\Admin\Modules\Model\Widget\WidgetRepository::getListOption($nowItem->dynamic_page));
                if (!empty($optionList)) {

                    foreach ($optionList as $option) {
                        if (strlen($option->value) <= 0) {
                            $value = $option->defaults;
                        } else {
                            $value = $option->value;
                        }
                        $variable = '$' . $option->variable . ' = "' . $value . '";';
                        eval($variable);
                    }
                }
                require $_SERVER['DOCUMENT_ROOT'] . '/Template/Dynamic/template' . $nowItem->dynamic_page . '/template.php';
            }
        }
    }

    public static function showTitle()
    {
        $nowItem = json_decode(self::getNowItem());

        if (strlen($nowItem->meta_title) > 0) {
            echo $nowItem->meta_title;
        } else {
            @$infosys = \Core_infosys::getTitle();
            if (!empty($infosys)) {
                if (strlen($infosys['seo_title']) > 0) {
                    echo $infosys['seo_title'];
                } else {
                    echo $infosys['title'];
                }
            } else {
                echo $nowItem->meta_title;
            }
        }
    }

    public static function showKeywords()
    {
        $nowItem = json_decode(self::getNowItem());
        if (strlen($nowItem->keywords) > 0) {
            echo $nowItem->keywords;
        } else {
            @$infosys = \Core_infosys::getTitle();
            if (!empty($infosys)) {
                if (strlen($infosys['seo_keywords']) > 0) {
                    echo $infosys['seo_keywords'];
                }
            } else {
                echo $nowItem->seo_keywords;
            }
        }
    }

    public static function showDescription()
    {
        $nowItem = json_decode(self::getNowItem());

        if (strlen($nowItem->description) > 0) {
            echo $nowItem->description;
        } else {
            @$infosys = \Core_infosys::getTitle();
            if (!empty($infosys)) {
                if (strlen($infosys['seo_description']) > 0) {
                    echo $infosys['seo_description'];
                }
            } else {
                echo $nowItem->seo_description;
            }
        }
    }

    public static function showFavicon()
    {
        $nowItem = json_decode(self::getNowItem());
        $nowSite = \FlexCat\Admin\Modules\Model\Sites\SitesRepository::getSiteNow($nowItem->site);

        echo $nowSite->favicon;
    }

    /**
     * @param null $name
     * @throws \Exception
     */
    public function header($name = null)
    {
        $name = (string)$name;
        $file = 'header';

        if ($name !== '') {

            $file = sprintf(self::RULES_NAME_FILE['header'], $name);
        }

        $this->loadTemplateFile($file);
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function footer($name = '')
    {
        $name = (string)$name;
        $file = 'footer';

        if ($name !== '') {

            $file = sprintf(self::RULES_NAME_FILE['footer'], $name);
        }

        $this->loadTemplateFile($file);
    }

    /**
     * @param string $name
     * @throws \Exception
     */
    public function sidebar($name = '')
    {
        $name = (string)$name;
        $file = 'sidebar';

        if ($name !== '') {

            $file = sprintf(self::RULES_NAME_FILE['sidebar'], $name);
        }

        $this->loadTemplateFile($file);
    }

    /**
     * @param string $name
     * @param array $data
     * @throws \Exception
     */
    public function block($name = '', $data = [])
    {
        $name = (string)$name;

        if ($name !== '') {

            $this->loadTemplateFile($name, $data);
        }
    }

    /**
     * @param $name
     * @param array $data
     */
    public static function load($name, $data = [])
    {
        require $_SERVER['DOCUMENT_ROOT'] . self::getUrl() . "/" . $name . ".php";
    }

    /**
     * @param $nameFile
     * @param array $data
     * @throws \Exception
     */
    private function loadTemplateFile($nameFile, $data = [])
    {
        $templateFile = ROOT_DIR . $this::getUrl() . "/" . $nameFile . '.php';

        if (ENV == 'Admin') {
            $templateFile = ROOT_DIR . '/Template/' . $nameFile . '.php';
        }

        if (is_file($templateFile)) {
            extract(array_merge($data, $this->data));
            require_once $templateFile;
        } else {

            throw  new \Exception(
                sprintf('Template file %s does not exist!', $templateFile)
            );
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }
}