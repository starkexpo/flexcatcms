<?php

namespace FlexCat\Template;

use FlexCat\DI\DI;

class Load
{

    const MASK_MODEL_REPOSITORY = '\FlexCat\%s\Modules\Model\%s\%sRepository';

    public $di;

    public function __construct(DI $di)
    {
        $this->di = $di;
    }


    public function model($modelName, $modelDir = false)
    {
        $modelName  = ucfirst($modelName);

        $modelDir   = $modelDir ? $modelDir : $modelName;

        $namespaceModel = sprintf(
            self::MASK_MODEL_REPOSITORY,
            ENV, $modelDir, $modelName
        );

        $isClassModel = class_exists($namespaceModel);

        if ($isClassModel) {
            $modelRegistry = $this->di->get('model') ?: new \stdClass();
            $modelRegistry->{lcfirst($modelName)} = new $namespaceModel($this->di);

            $this->di->set('model', $modelRegistry);
        }

        return $isClassModel;
    }
}